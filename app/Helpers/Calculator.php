<?php
namespace App\Helpers;

final class Calculator {
    
    public static function omise_percent($total){
        $omise_fee  = number_format($total * (3.65/100), 2);
        $vat        = number_format($omise_fee * (7/100), 2);
        $total_fee  = number_format($omise_fee + $vat, 2);
        $balance    = $total - $total_fee;
        
        $result     = array(
            "total"     => $total,
            "omise_fee" => $omise_fee,
            "vat"       => $vat,
            "total_fee" => $total_fee,
            "balance_bat" => $balance
        );
        return $result;
    }

}