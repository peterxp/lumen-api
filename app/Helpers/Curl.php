<?php
namespace App\Helpers;

final class CURL {

    public static function RestCall($method="",$url="",$parameter) {
        $service_url = (string) $url;
        switch ($method) :
            case 'GET':
                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $curl_response = curl_exec($curl);
                if ($curl_response === false) :
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    return var_export($info);
                else:
                    curl_close($curl);
                    return json_decode($curl_response);
                endif;
                break;
            case 'POST':
                $curl = curl_init($service_url);
                $curl_post_data = $parameter;
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
                $curl_response = curl_exec($curl);
                if ($curl_response === false) :
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    return var_export($info);
                else:
                    curl_close($curl);
                    return json_decode($curl_response);
                endif;
                break;
            case 'PUT':
                $ch = curl_init($service_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                $data = $parameter;
                curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
                $response = curl_exec($ch);
                if ($response === false) :
                    $info = curl_getinfo($ch);
                    curl_close($ch);
                    return var_export($info);
                else:
                    curl_close($ch);
                    return json_decode($response);
                endif;
                break;
            case 'DELETE':
                $ch = curl_init($service_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                $curl_post_data = $parameter;
                curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
                $response = curl_exec($ch);
                if ($curl_response === false) :
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    return var_export($info);
                else:
                    curl_close($curl);
                    return json_decode($curl_response);
                endif;
                break;
            default:
                return false;
                break;
        endswitch;
    }



}