<?php
namespace App\Helpers;

final class Utility {

    // Generate token
    public static function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public static function generateToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[Utility::crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    // End generate token

    function checkBadWord($message){
        $restricted_message = ["line","lineid","line id","id","ไลน์","ไลน์ไอดี","add","แอด","shoppee"];
        $result = count(array_filter($restricted_message, create_function('$e','return strstr("'.strtolower($message).'", $e);')));
        return $result;
    }


}