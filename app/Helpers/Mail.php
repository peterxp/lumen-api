<?php
namespace App\Helpers;
use PHPMailer;

final class Mail {

    #ref https://github.com/PHPMailer/PHPMailer

    public static function sendMail($vendor_email = "", $vendor_name = "valued vendor", $type = "", $token) {

        $mail = new \PHPMailer(true);

        $mail->SMTPDebug = false;
        $mail->isSMTP();
        $mail->Host = env('MAIL_HOST');
        $mail->SMTPAuth = true;
        $mail->Username = env('MAIL_USERNAME');
        $mail->Password = env('MAIL_PASSWORD');
        $mail->SMTPSecure = 'tls';
        $mail->Port = env('MAIL_PORT');
        $mail->From = env('MAIL_FROM_ADDRESS');
        $mail->FromName = env('MAIL_FROM_NAME');
        $mail->isHTML(true);
        if (empty($vendor_name)) {
            if($type == "user_reset_password"){
                $vendor_name = "valued user";
            }else{
                $vendor_name = "valued vendor";
            }
        } else {
            $vendor_name = $vendor_name;
        }

        $body_header = '<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head><body style="font-family: Gotham, Helvetica, Arial, sans-serif;color:#666;"><div style="width:100%; max-width:690px; margin:0 auto; position:relative;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="float:left;width:100%;"><tbody><tr><td valign="middle" style="border-bottom:1px solid #ccc; height:2em;"> <img style="float:right; display:inline-block; margin:1em 0; margin-bottom:1em; width:auto; height:2.5em; position:absolute; right:0; top:-0.5em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/header-logo.gif" /></td></tr><tr><td align="left" valign="middle" style="line-height:1.5em;font-size:0.8em;">';
        $body_footer = '</td></tr><tr><td align="center" valign="middle"> <img style="float:left; width:100%; margin:2em 0;margin-bottom:0.5em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/footer.jpg" /><br> <a style="font-size:x-small; display:inline-block;text-decoration:none; margin-bottom:0; color:#333;" href="https://www.onner.com/">www.onner.com</span></td></tr><tr><td align="center" valign="middle"><div style="float:left; width:100%; text-align:center; font-size:small; padding:1em 0; margin:1em 0; margin-bottom:2em; border-top:1px solid #ccc;border-bottom:1px solid #ccc;"><div style="float:left;display: inline-block;width:50%; border-right:1px solid #666; box-sizing:border-box;"> Follow us : <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/facebook.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/instagram.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/twitter.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/youtube.gif" /></a></div><div style="float:left;display: inline-block;width:50%; box-sizing:border-box;"> <a style="text-decoration:none; color:inherit;" href="https://www.onner.com/"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/getapp.gif" />Download app</a></div></div></td></tr><tr><td align="center" valign="middle" style="font-weight:bold;"><a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/contact-us">CONTACT US</a> <a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/about-us">ABOUT US</a> <a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/faq">FAQ</a></td></tr><tr><td align="center" valign="middle"><span style="display:inline-block; margin:1em 0;font-size: x-small; font-style: normal; color: #666666;">Not interested in e-mail updates? <a style="color:#666;" href="https://app.onner.com/mail/unsubscribe">Unsubscribe</a></span></td></tr></tbody></table></div></body></html>';
        switch ($type) {
            case 'signup':
                $subject = "Your Onner Log In Information";
                $body = $body_header.'<h1 style="font-weight:normal; font-size:1.3em; margin-top:1.5em;">Dear '.$vendor_name.'</h1><p>Welcome to Onner! Thank you for signing up for the Onner Vendor Application.<br>Your log in information is below:</p><p> E-Mail: '.$vendor_email.'<br> Password: *******</p><p> Remember, your shop must be approved by Onner before it can open and be shown to customers on the Onner Vendor Application. Please fill out your shop and product information then send us a shop approval request.</p><p>Thank you.<br>Onner Support</p>'.$body_footer;
                break;
            case 'signin':
                $subject = "Your Onner Log In Information Was Used";
                $body = $body_header.'<h1 style="font-weight:normal; font-size:1.3em; margin-top:1.5em;">Dear '.$vendor_name.'</h1><p>Your Onner log in information was used to sign in to the Onner Vendor Application.</p><p> If you have not recently signed in to Onner and believe someone may have accessed your account, change your password as soon as possible or contact Onner Support.</p><p>Thank you.<br>Onner Support</p>'.$body_footer;
                break;
            case 'user_reset_password':
                $expires = date("Y-m-d H:i:s", strtotime("+30 minutes"));
                $subject = "Reset Password Request";
                $body = $body_header.'<h1 style="font-weight:normal; font-size:1.3em; margin-top:1.5em;">Dear '.$vendor_name.'</h1><p>You can reset your Onner password by clicking the link below:</p><p> Date of request: '.date("Y-m-d H:i:s").'<br>Link: <a href="http://api-naja.onner.com/v2/user/renewPassword/'.$token.'">Click</a><br>Link expires: '.$expires.' </p><p> If you did not make this request, contact Onner Support. Or if you choose to ignore this email, your password will remain the same.</p><p>Thank you.<br>Onner Support</p>'.$body_footer;
                break;
            case 'vendor_reset_password':
                $expires = date("Y-m-d H:i:s", strtotime("+30 minutes"));
                $subject = "Reset Password Request";
                $body = $body_header.'<h1 style="font-weight:normal; font-size:1.3em; margin-top:1.5em;">Dear '.$vendor_name.'</h1><p>You can reset your Onner password by clicking the link below:</p><p> Date of request: '.date("Y-m-d H:i:s").'<br>Link: <a href="http://api-naja.onner.com/v2/vendor/renewPassword/'.$token.'">Click</a><br>Link expires: '.$expires.' </p><p> If you did not make this request, contact Onner Support. Or if you choose to ignore this email, your password will remain the same.</p><p>Thank you.<br>Onner Support</p>'.$body_footer;
                break;
            default:
                $subject="";
                $body="";
                break;
        }
        try {
            $mail->addAddress($vendor_email, $vendor_name);
            $mail->Subject = $subject;
            $mail->Body = $body;
            if (! $mail->send()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public static function sendMailRequestTransfer($message) {

        $recipients = array(
            'xyz' => 'Onphiphak Tipchai',
            'xyz' => 'Frank Xia'
        );
        $recipient_name = "Onner Financial Team";
        $subject = "Vendors Request Transfer Money [".$message['shop_name']."]";

        $body_header = '<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head><body style="font-family: Gotham, Helvetica, Arial, sans-serif;color:#666;"><div style="width:100%; max-width:690px; margin:0 auto; position:relative;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="float:left;width:100%;"><tbody><tr><td valign="middle" style="border-bottom:1px solid #ccc; height:2em;"> <img style="float:right; display:inline-block; margin:1em 0; margin-bottom:1em; width:auto; height:2.5em; position:absolute; right:0; top:-0.5em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/header-logo.gif" /></td></tr><tr><td align="left" valign="middle" style="line-height:1.5em;font-size:0.8em;">';
        $body_footer = '</td></tr><tr><td align="center" valign="middle"> <img style="float:left; width:100%; margin:2em 0;margin-bottom:0.5em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/footer.jpg" /><br> <a style="font-size:x-small; display:inline-block;text-decoration:none; margin-bottom:0; color:#333;" href="https://www.onner.com/">www.onner.com</span></td></tr><tr><td align="center" valign="middle"><div style="float:left; width:100%; text-align:center; font-size:small; padding:1em 0; margin:1em 0; margin-bottom:2em; border-top:1px solid #ccc;border-bottom:1px solid #ccc;"><div style="float:left;display: inline-block;width:50%; border-right:1px solid #666; box-sizing:border-box;"> Follow us : <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/facebook.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/instagram.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/twitter.gif" /></a> <a href="#"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/youtube.gif" /></a></div><div style="float:left;display: inline-block;width:50%; box-sizing:border-box;"> <a style="text-decoration:none; color:inherit;" href="https://www.onner.com/"><img style="width:1.2em; height:1.2em;" src="https://s3-ap-southeast-1.amazonaws.com/onnercloud/mailsystem/icons/getapp.gif" />Download app</a></div></div></td></tr><tr><td align="center" valign="middle" style="font-weight:bold;"><a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/contact-us">CONTACT US</a> <a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/about-us">ABOUT US</a> <a style=" font-size:x-small;color:#333; display:inline-block; margin:0 0.5em; text-decoration:none;" href="https://www.onner.com/faq">FAQ</a></td></tr><tr><td align="center" valign="middle"><span style="display:inline-block; margin:1em 0;font-size: x-small; font-style: normal; color: #666666;">Not interested in e-mail updates? <a style="color:#666;" href="https://app.onner.com/mail/unsubscribe">Unsubscribe</a></span></td></tr></tbody></table></div></body></html>';

        $body = $body_header.'<h1 style="font-weight:normal; font-size:1.3em; margin-top:1.5em;">Dear '.$recipient_name.',</h1>
        	<p>Our system have vendors request transfer money as below</p>
        	<p>
        		<b/>Shop ID:</b> '.$message['shop_id'].'<br/>
        		<b/>Shop Name:</b> '.$message['shop_name'].'<br/>
        		<b/>Start Date:</b> '.$message['start_date'].'<br/>
        		<b/>End Date:</b> '.$message['end_date'].'<br/>
        		<b/>TAB:</b> '.$message['tab_value'].'<br/>
        		<b/>BAT:</b> '.$message['bat_value'].'<br/>
        		<b/>Omise Fee:</b> '.$message['omise_fee'].'<br/>
        		<b/>Vat:</b> '.$message['vat'].'<br/>
        		<b/>Total Deductins(Omise Fee + Vat) :</b> '.$message['total_fee'].'<br/>
        	</p>
        	<p>Thank you.<br>Onner Support</p>'.$body_footer;


        try {
            if (Mail::sendEmail($recipients, $subject, $body)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }

    }

    /*
    $recipients = array(
        'abc@hotmail.com' => 'Onner Member Name 2',
        'efg@hotmail.com' => 'Onner Member Name 2'
    );
    */
    public static function sendEmail($recipients, $subject, $body){
        $mail = new \PHPMailer;
        $mail->SMTPDebug = false;
        // $mail->SMTPDebug = 3;
        $mail->CharSet = 'utf-8';
        $mail->isSMTP();
        $mail->Host = env('MAIL_HOST');
        $mail->SMTPAuth = true;
        $mail->Username = env('MAIL_USERNAME');
        $mail->Password = env('MAIL_PASSWORD');
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->From = env('MAIL_FROM_ADDRESS');
        $mail->FromName = env('MAIL_FROM_NAME');
        $mail->isHTML(true);

        try {
            if(is_array($recipients)){
                foreach ($recipients as $email => $name) {
                    $mail->addAddress($email, $name);
                }
            }else{
                return false;
            }
            $mail->AddBCC('chai.pr@onner.com', 'Supachai Pradabsri');
            $mail->Subject = $subject;
            $mail->Body = $body;
            if (! $mail->send()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }


    //Check real email in the real world
    public static function is_real_email($email){
        if(!preg_match ("/^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/", $email))
            return false;
        list($prefix, $domain) = explode("@",$email);
        if(function_exists("getmxrr") && getmxrr($domain, $mxhosts))
            return true;
        elseif (@fsockopen($domain, 25, $errno, $errstr, 5))
            return true;
        else
            return false;
    }

}