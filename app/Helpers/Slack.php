<?php
namespace App\Helpers;
use App\Helpers\CURL;

final class Slack {

    const SLACK_HOOK_URL = "https://hooks.slack.com/services/T0JGJG6AZ/B0JGS394Z/xyz";

    public static function hookNewOrder($order_id, $shop_name, $customer_name, $created_at, $grand_total){
        #prepare value
        $username = "xyz";
        //$channel  = "#onner-order";
        $channel  = "#test-slack-hook";
        $fallback = "We got a new order! Click here to <https://xyz.com/order/'.$order_id.'|View order>";
        $pretext  = "We got a new order! Click here to <https://xyz/order/'.$order_id.'|View order>.";
        $color    = "#00B16A";
        $fields_title = "Order ID: ".$order_id;
        $fields_value = "";
        $fields_value .= "Customer: ".$customer_name."\n";
        $fields_value .= "Shop: ".$shop_name."\n";
        $fields_value .= "Date: ".$created_at."\n";
        $fields_value .= "Total: ".$grand_total." THB";
        $fields_short  = false;

        #arrange value to slack array format
        $data = [
            "username"      => $username,
            "channel"       => $channel,
            "attachments"   => [
                [
                    "fallback"  => $fallback,
                    "pretext"   => $pretext,
                    "color"     => $color,
                    "fields"    => [
                        [
                            "title" => $fields_title,
                            "value" => $fields_value,
                            "short" => $fields_short
                        ]
                    ]
                ]
            ]
        ];

        return CURL::RestCall("POST", slack::SLACK_HOOK_URL, json_encode($data));

    }


    public static function hookRequestTransfer($transfer_id, $shop_name, $tab_value, $bat_value, $start, $end, $created_at){

        #prepare value
        $username = "Onner Backend";
        //$channel  = "#onner-transaction";
        $channel  = "#test-slack-hook";
        $fallback = $shop_name." want to withdraw money! Click here to <https://abc.com/finance/viewtransfer/".$transfer_id."|View transaction>.";
        $pretext  = $shop_name." want to withdraw money! Click here to <https://abc.com/finance/viewtransfer/".$transfer_id."|View transaction>.";
        $color    = "#f39c12";
        $fields_title = "Transaction ID: ".$transfer_id;
        $fields_value = "";
        $fields_value .= "Shop: ".$shop_name."\n";
        $fields_value .= "TAB: ".number_format($tab_value,2)." THB\n";
        $fields_value .= "BAT: ".number_format($bat_value,2)." THB(".$start." - ".$end.")\n";
        $fields_value .= "Request Date: ".$created_at;
        $fields_short  = false;

        #arrange value to slack array format
        $data = [
            "username"      => $username,
            "channel"       => $channel,
            "attachments"   => [
                [
                    "fallback"  => $fallback,
                    "pretext"   => $pretext,
                    "color"     => $color,
                    "fields"    => [
                        [
                            "title" => $fields_title,
                            "value" => $fields_value,
                            "short" => $fields_short
                        ]
                    ]
                ]
            ]
        ];

        return CURL::RestCall("POST", slack::SLACK_HOOK_URL, json_encode($data));
    }


    public static function hookRequestOpenShop($shop_id, $shop_name, $category_name, $created_at){

        #prepare value
        $username = "Onner Backend";
        $channel  = "#onner-shop";
        $fallback = $shop_name." request to open shop! Click here to <https://abc.com/shop/view/".$shop_id."|Review shop detail>.";
        $pretext  = $shop_name." request to open shop! Click here to <https://abc.com/shop/view/".$shop_id."|Review shop detail>.";
        $color    = "#f39c12";
        $fields_title = "Shop ID: ".$shop_id;
        $fields_value = "";
        $fields_value .= "Shop: ".$shop_name."\n";
        $fields_value .= "Category: ".$category_name."\n";
        $fields_value .= "Request date: ".$created_at;
        $fields_short  = false;

        #arrange value to slack array format
        $data = [
            "username"      => $username,
            "channel"       => $channel,
            "attachments"   => [
                [
                    "fallback"  => $fallback,
                    "pretext"   => $pretext,
                    "color"     => $color,
                    "fields"    => [
                        [
                            "title" => $fields_title,
                            "value" => $fields_value,
                            "short" => $fields_short
                        ]
                    ]
                ]
            ]
        ];

        #CURL POST to hook to slack
        CURL::RestCall("POST", SLACK_HOOK_URL, json_encode($data));
    }


    public static function hookApproveShop($shop_id, $shop_name, $category_name, $user_logs_datetime, $user_full_name, $created_at){

        #prepare value
        $username = "Onner Backend";
        $channel  = "#onner-shop";
        $fallback = $shop_name." has been approved! Click here to <https://abc.com/shop/view/".$shop_id."|View shop detail>.";
        $pretext  = $shop_name." has been approved! Click here to <https://abc.com/shop/view/'.$shop_id'].'|View shop detail>.";
        $color    = "#27ae60";
        $fields_title = "Shop ID: ".$shop_id;
        $fields_value = "";
        $fields_value .= "Shop: ".$shop_name."\n";
        $fields_value .= "Category: ".$category_name."\n";
        $fields_value .= "Request date: ".$created_at."\n";
        $fields_value .= "Approved date:  ".$user_logs_datetime."\n";
        $fields_value .= "Approved by: ".$user_full_name;
        $fields_short  = false;

        #arrange value to slack array format
        $data = [
            "username"      => $username,
            "channel"       => $channel,
            "attachments"   => [
                [
                    "fallback"  => $fallback,
                    "pretext"   => $pretext,
                    "color"     => $color,
                    "fields"    => [
                        [
                            "title" => $fields_title,
                            "value" => $fields_value,
                            "short" => $fields_short
                        ]
                    ]
                ]
            ]
        ];

        #CURL POST to hook to slack
        CURL::RestCall("POST", SLACK_HOOK_URL, json_encode($data));
    }


}