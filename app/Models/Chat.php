<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

final class Chat extends Model
{
    protected $table = 'chat';

    protected $guarded = [];

    public static function deleteChat($shop_id, $user_id, $owner_type, $last_chat_id){

        if ($owner_type == 'user') {
            $status_key = "status_user";
        }elseif ($owner_type == 'shop') {
            $status_key = "status_vendor";
        }

        $chat = Chat::where('user_id', '=', $user_id)
                ->where('shop_id', '=', $shop_id)
                ->where('id', '<=', $last_chat_id)
                ->update([$status_key => 0]);

        if ($chat) {
            return "success";
        }else{
            return "fail";
        }
    }

    public static function createChat($shop_id, $user_id, $owner_type){

        if (Chat::checkChatShopAndUser($shop_id, $user_id, $owner_type)) {
            return "chat already";
        }else{

            $insertChat = [];
            if ($owner_type == 'shop'){
                $insertChat = [
                    'shop_id' 		=> $shop_id,
                    'user_id' 		=> $user_id,
                    'message' 		=> 'chat start',
                    'status_user'	=> 0,
                    'status_vendor' => 1,
                    'owner_type'	=> $owner_type,
                ];
            }
            elseif ($owner_type == 'user'){
                $insertChat = [
                    'shop_id' 		=> $shop_id,
                    'user_id' 		=> $user_id,
                    'message' 		=> 'chat start',
                    'status_user'	=> 1,
                    'status_vendor' => 0,
                    'owner_type'	=> $owner_type,
                ];
            }

            $chat = Chat::create($insertChat);

            if ($chat){
                return "create chat success";
            }
        }

    }

    public static function addChat($user_id, $shop_id, $message, $owner_type){

        $insertChat = [
            'shop_id' 		=> $shop_id,
            'user_id' 		=> $user_id,
            'message' 		=> $message,
            'owner_type'	=> $owner_type,
            'status_user'	=> 1,
            'status_vendor' => 1,
        ];
        //insert and get id
        $chat = Chat::create($insertChat);
        $last_insert_id = $chat->id;

        if ($last_insert_id > 0){

            // owner type = 'vendor' => send message to user
            if ($owner_type == 'shop'){

                $chatObject = Chat::queryMessage($last_insert_id);
                $chatObject->shop_info = Shop::where('shop_id', '=', $shop_id)->get();

                $user_aws_tokens = UserAuthentication::where('user', '=', $user_id)->where('status', '=', 1)->get();
                foreach ($user_aws_tokens as $user) {
                    $shop_name = $chatObject->shop_info->name ? $chatObject->shop_info->name : 'Shop';
                    #TODO send push here
                    #Pushnotification::sendPushChat($user->apns_key, $shop_name.': '.$message, 1, 'Message.mp3', $chatObject);
                }
            }
            // owner type = 'user' => send message to vendor
            else if ($owner_type == 'user'){

                $chatObject = Chat::queryMessage($last_insert_id);
                $chatObject->user_info = User::getUser($user_id);

                $vendor_aws_tokens = VendorAuthentication::where('vendor_id', '=', $shop_id)->where('status', '=', 1)->get();
                foreach ($vendor_aws_tokens as $vendor) {
                    $user_name = $chatObject->user_info->name ? $chatObject->user_info->name : 'User';
                    //Pushnotification::sendPushChat($vendor->apns_key, $user_name.': '.$message, 1, 'Message.mp3', $chatObject);
                }
            }
            return "success";
        }else{
            return "fail";
        }
    }

    public static function queryLastMessageForUser($user_id){
        $sql ="SELECT * FROM (SELECT chat.*
									        FROM 		chat
									        WHERE 		(chat.user_id = $user_id OR chat.user_id = 0 )
									        AND 		chat.status_user = 1
									        ORDER BY    chat.created_at DESC
									)AS c1
									GROUP BY c1.shop_id
									ORDER BY c1.created_at DESC
								";
        $chats_with_this_shop = DB::select($sql);
        $returnData = [];
        foreach ($chats_with_this_shop as $chat) {
            $shop_info = Shop::where('id', '=', $chat->shop_id)->get();
            if (count($shop_info) > 0) {
                if ($chat->user_id == 0) $chat->user_id =  $user_id;
                $chat->shop_info = $shop_info;
                array_push($returnData,	$chat);
            }
        }
        return $returnData;
    }


    public static function queryLastMessageForShop($shop_id){
        $sql = "SELECT * FROM (SELECT 		chat.*
									        FROM 		chat
									        WHERE 		(chat.shop_id = $shop_id OR chat.shop_id = 0 )
									        AND 		chat.status_vendor = 1
									        ORDER BY    chat.created_at DESC
									)AS c1
									GROUP BY c1.user_id
									ORDER BY c1.created_at DESC
              ";
        $chats_with_this_shop = DB::select($sql);
        $returnData = [];
        foreach ($chats_with_this_shop as $chat) {

            $user = User::getUser($chat->user_id);

            if (count($user) > 0) {
                if ($chat->shop_id == 0) $chat->shop_id =  $shop_id;
                $chat->user_info = $user;
                array_push($returnData, $chat);
            }
        }
        return $returnData;
    }

    public static function getMessage($shop_id, $user_id, $chat_id, $owner_type){
        if ($chat_id == '0') {
            $result = Chat::queryMessageAll($shop_id, $user_id, $owner_type);
        }else{
            $result = Chat::queryNewMessage($shop_id, $user_id, $chat_id, $owner_type);
        }
        return $result;
    }

    public static function queryNewMessage($shop_id, $user_id, $chat_id, $owner_type){
        $returnData = [];
        if ($owner_type == 'user') {
            $sql ="SELECT 	chat.*
                        FROM 	chat
                        WHERE 	chat.shop_id = $shop_id
                        AND 	(chat.user_id = $user_id OR chat.user_id = 0 )
                        AND     chat.status_user = 1
                        AND 	chat.id >  $chat_id
                        ORDER BY chat.created_at ASC
 								";
            $chats  = DB::select($sql);

            foreach ($chats as $chat) {
                if ($chat->user_id == 0) $chat->user_id = $user_id;
                array_push($returnData, $chat);
            }
            return $returnData;

        }elseif ($owner_type == 'shop'){
            $sql = "SELECT 	chat.*
                        FROM 	chat
                        WHERE 	(chat.shop_id = $shop_id OR chat.shop_id = 0 )
                        AND 	chat.user_id = $user_id
                        AND  	chat.status_vendor = 1
                        AND 	chat.id >  $chat_id
                        ORDER BY chat.created_at ASC
 								";
            $chats  = DB::select($sql);

            foreach ($chats as $chat) {
                if ($chat->shop_id == 0) $chat->shop_id = $shop_id;
                array_push($returnData, $chat);
            }
            return $returnData;
        }


    }

public static function queryMessageAll($shop_id, $user_id, $owner_type){
        $result = NULL;
        if ($owner_type == 'user') {
            $sql = "SELECT 	     chat.*
                        FROM 	 chat
                        WHERE 	 chat.shop_id = $shop_id
                        AND 	 (chat.user_id = $user_id OR chat.user_id = 0 )
                        AND      chat.status_user = 1
                        ORDER BY chat.created_at ASC
 								";
            $result  = DB::select($sql);

        }elseif ($owner_type == 'shop'){
            $sql = "SELECT 	     chat.*
                        FROM 	 chat
                        WHERE 	(chat.shop_id = $shop_id OR chat.shop_id = 0 )
                        AND 	 chat.user_id = $user_id
                        AND      chat.status_vendor = 1
                        ORDER BY chat.created_at ASC
 								";
            $result  = DB::select($sql);
        }

        return $result;
    }

    public static function queryMessage($chat_id){
        return Chat::where('id', '=', $chat_id)->get();
    }

    public static function checkChatShopAndUser($shop_id, $user_id, $owner_type){

        $count = 0;
        if ($owner_type == 'user') {

            $count = Chat::where('shop_id', '=', $shop_id)
                         ->where('user_id', '=', $user_id)
                         ->where('status_user', '=', 1)
                         ->count();

        }elseif($owner_type == 'shop'){

            $count = Chat::where('shop_id', '=', $shop_id)
                         ->where('user_id', '=', $user_id)
                         ->where('status_vendor', '=', 1)
                         ->count();
        }

        if($count > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }


    /// 8 Feb 2016
    public static function countMessage($shop_id, $user_id, $owner_type){

        if ($owner_type == 'shop') {
            $count = Chat::where('shop_id', '=', $shop_id)
                        ->where('vendor_see', '=', 0)
                        ->count();
        }

        elseif ($owner_type == 'user') {
            $count = Chat::where('user_id', '=', $user_id)
                        ->where('user_see', '=', 0)
                        ->count();
        }

        return $count;
    }

}