<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


final class Address extends Model
{
    protected $table = 'address';

    public $timestamps = false;

    protected $guarded = array();

}