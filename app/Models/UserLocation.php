<?php
namespace App\Models;

use DB;


use Illuminate\Database\Eloquent\Model;


final class UserLocation extends Model
{
    protected $table = 'user_location';

    protected $fillable = array('');


    public static function getByUserId($user_id){

        $address = UserLocation::select(['id', 'data'])
                    ->where('user_id', '=', $user_id)
                    ->where('status', '=', 1)->get();

        if(count($address) > 0){
            foreach($address as $key){

                if($key->data == null) {
                    $key->data = "{}";
                }
                $data[] = ['id' => $key->id, 'data'=> json_decode($key->data)];
            }
        }else{
            $data[] = "";
        }

        return $data;
    }
}