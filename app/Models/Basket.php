<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
use App\Models\Shop;
//use App\Models\DeliveryRule;
use App\Models\MenuImage;
use App\Models\Tier;

use Illuminate\Database\Eloquent\Model;


final class Basket extends Model
{
    protected $table = 'basket';

	protected $fillable = [''];


	public static function deleteByShop($shop_id, $user_id){
		if (Basket::queryDeleteByShop($shop_id, $user_id)){
			return Basket::getBasket($user_id);
		}
	}

	public static function deleteByBasket($basket_id,$user_id){
		if (Basket::queryDeleteByBasket($basket_id)){
			return Basket::getBasket($user_id);
		}
	}


	public static function getBasketCount($user_id){
		$basket = Basket::queryCountMenuByUser($user_id);
		return $basket;
	}


	public static function getBasket($user_id){
		$returnData = array();

		foreach(Basket::queryBasketGroupByShop($user_id) as $shop_id){

			$shop = Shop::queryShopWithMaxDistanceByShop($shop_id->shop_id);
			$shop = $shop[0];

			$baskets = Basket::queryBasket($user_id, $shop_id->shop_id);

			$tmp_basket = [];
			$total_price = 0;
			$charge_price = 0;
			foreach ($baskets as $basket) {

				$sum_price = ( (int)$basket->qty * (int)$basket->price );
				$basket->sum_price = (string)$sum_price;
				$basket->sum_price_show = number_format($sum_price);
				$charge_price = $basket->delivery_charge;
				array_push($tmp_basket, $basket);
				$total_price = $total_price + $sum_price;
				$menuinfo = null;

				$menu_count = Menu::queryMenuWithStock($basket->menu_id);

				if (count($menu_count) > 0){
					$menuinfo = Menu::queryMenuWithStock($basket->menu_id);
					$tier = Menu::queryTier($basket->tier_id);
					$tier->price_show = number_format((int)$tier['price']);
					$menuinfo->tier = $tier;
				}else{
					$menuinfo = [''=>'']; //empty array
				}

				$basket->menuinfo = $menuinfo;
				$basket->location = Basket::queryLocationBasket($basket->basket_location_id);
			}

			$shop->total_food 		= (string)($total_price);
			$shop->total_food_show 	= number_format($total_price);
			$shop->total 		= (string)($total_price+$charge_price);
			$shop->total_show 	= number_format($total_price+$charge_price);
			$shop->baskets = $tmp_basket;
			array_push($returnData, $shop);
		}
		return $returnData;
	}

	public static function queryBasket($user_id,$shop_id){

		$result = DB::select("SELECT basket.*, tier.price
			FROM basket INNER JOIN tier ON basket.tier_id = tier.id
			WHERE (basket.user_id = $user_id) AND (basket.shop_id = $shop_id) AND (basket.status = '1')
		");

		return $result;
	}

	public static function queryBasketGroupByShop($user_id){
		$result = DB::select("
			SELECT 	basket.shop_id
			FROM 	basket
			WHERE 	basket.user_id = $user_id
			AND     basket.status  = 1
			GROUP BY basket.shop_id");
		return $result;
	}

	public static function queryLocationBasket($basket_location_id){
		$result = DB::select("SELECT address.*,
			basket_location.latitude,
			basket_location.longitude,
			basket_location.status
			FROM address,basket_location
			WHERE basket_location.id = $basket_location_id
			AND basket_location.address_id = address.id");
		return $result;
	}

	public function queryCountMenuByUser($user_id){
		Basket::where('user_id', '=', $user_id)
			->where('status', '=', 1)
			->select('count(id) as menu_count');
	}

	public static function leaveNote($basket_id, $note){

		$basket = Basket::find($basket_id);
		$basket->note = $note;
		$result = $basket->save();
		return ($result)? true : false;

	}

	public static function queryBasketWithID($basket_id){
		return Basket::where('id', '=', $basket_id)->get();
	}

	//update by shop
	public static function queryDeleteByShop($shop_id, $user_id){

		$chk = Basket::where('user_id', '=', $user_id)
			->where('shop_id', '=', $shop_id)
			->update(['status' => 3]);
		return $result = ($chk > 0)? true : false;

	}

	//user delete basket
	public static function userDeleteBasket($basket_id, $user_id){

		$chk = Basket::where('id', '=', $basket_id)
			->update(['status' => 3]);
		return $result = ($chk > 0)? true : false;

	}

	//return count basket
	public static function countMenuByUser($user_id){
		return Basket::where('user_id', '=', $user_id)
			->where('status', '=', '1')
			->count();
	}

}