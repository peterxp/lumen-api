<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


final class UserAuthentication extends Model
{
    protected $table = 'user_authentication';
    protected $fillable = array('');

    public static function authenticateOnlyAuthKey($auth_key){
        $user  = UserAuthentication::where('auth_key', '=', $auth_key)->get();
        $result = (count($user)> 0)? true : false;
        return $result;
    }

    //Check user authenticate
    public static function authenticate($auth_key, $id){
        $user = UserAuthentication::where('user_id', '=',$id)
            ->where('auth_key', '=',$auth_key)
            ->get();
        $result = (count($user)> 0)? true : false;
        return $result;
    }

    public static function get_endpoint_key($auth_key){
        $auth = UserAuthentication::where('auth_key', $auth_key)->first();
        $apns_key = ($auth->apns_key)? $apns_key : "0";
        return $apns_key;
    }



}