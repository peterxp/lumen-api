<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Message{

    //-----------------------------------------------
    //	Success			    				|
    //-----------------------------------------------
    public  static function response($data){
        $info = array(
            "data"      => $data,
            "status"    => "true",
            "message"   => "success"
        );
        return $info;
    }


    //-----------------------------------------------
    //	Fail			    				|
    //-----------------------------------------------
    public  static function responseFalse($data){
        $info = array(
            "data"      => $data,
            "status"    => "false",
            "message"   => "fail"
        );
        return $info;
    }

    //-----------------------------------------------
    //	Wrong email format			    				|
    //-----------------------------------------------

    public  static function wrongEmailFormat(){
        $info = array(
            "status" => "false",
            "message"=> "Wrong email format"
        );
        return $info;
    }



    //-----------------------------------------------
    //	Wrong parameter			    				|
    //-----------------------------------------------

    public  static function wrongParameter(){
        $info = array(
            "status" => "false",
            "message"=> "Wrong parameter"
        );
        return $info;
    }

    //-----------------------------------------------
    //	Reset Passwrod			    				|
    //-----------------------------------------------

    public  static function resetpassSuccess(){
        $info = array(
            "status" => "true",
            "message" => "Please check your mail to reset password."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Sign In False 			    				|
    //-----------------------------------------------
    public  static function signInFalse(){
        $info = array(
            "status" => "false",
            "message" => "Your email address or password is incorrect."
        );
        return $info;
    }

    public static function signInFalseParam(){
        $info = array(
            "status" => "false",
            "message" => "Please sign in with your email or sign in with Facebook."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Sign Out False 			    				|
    //-----------------------------------------------
    public  static function signOutFalse(){
        $info = array(
            "status" => "false",
            "message" => "Can't sign out."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Sign Up False 			    				|
    //-----------------------------------------------
    public  static function signUpRepasswordFalse(){
        $info = array(
            "status" => "false",
            "message" => "Password dose not match."
        );
        return $info;
    }

    public  static function signUpInsertFalse(){
        $info = array(
            "status" => "false",
            "message" => "Can't sign up."
        );
        return $info;
    }

    function signUpDuplicateFalse(){
        $info = array(
            "status" => "false",
            "message" => "Email address is already exist."
        );
        return $info;
    }

    public  static function signUpEmailWrong(){
        $info = array(
            "status" => "false",
            "message" => "Invalid email address."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Reset Password			    				|
    //-----------------------------------------------
    public  static function resetPassFalseParam(){
        $info = array(
            "status" => "false",
            "message" => "Please enter your email."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Check fake email		    				|
    //-----------------------------------------------
    public  static function resetPassFalse(){
        $info = array(
            "status" => "false",
            "message" => "Your email not exist."
        );
        return $info;
    }

    //-----------------------------------------------
    //	Promotion False		    				|
    //-----------------------------------------------

    public  static function checkPromotionFalse(){
        $info = array(
            "status" => "false",
            "message" => "Invalid code."
        );
        return $info;
    }

    public static function checkDuplicateEmail(){
        $info = array(
            "status" => "false",
            "message" => "Email address is already exist."
        );
        return $info;
    }

    public static function authenticateFail(){
        $info = array(
            "status"    => "false",
            "message"   => "Authentication fail"
        );
        return $info;
    }

    public  static function invalidEmail(){
        $info = array(
            "status" => "false",
            "message" => "Invalid email address."
        );
        return $info;
    }

    public static function passwordNotMatch(){
        $info = array(
            "status" => "false",
            "message" => "Password does not match the  re-password"
        );
        return $info;
    }

    public static function incompleteOperation(){
        $info = array(
            "status" => "false",
            "message" => "Incomplete operation"
        );
        return $info;
    }

}