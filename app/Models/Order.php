<?php
namespace App\Models;

use App\Helpers\Curl;
use DB;
use App\Models\UserAuthication;
use App\Models\Menu;
use App\Models\Message;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Request;


final class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [];

	public static function vendorCompleteOrder($shop_id, $order_id){
		$order = ['status_for_vendor'	=> 4];
		$chk = Order::where('id', '=', $order_id)
					->where('shop_id', '=', $shop_id)
					->update($order);

		if($chk){
			$result = "Completed Order";
		}else{
			$result = "Fail";
		}
		return $result;
	}


	//use this function instead of updateOrder();
	public static function updateStatus($order_id, $order_status, $user_id){

		$order_data = [
			'status_for_vendor' => $order_status,
			'status_for_user' 	=> $order_status,
			'createdate'	 	=> date('Y-m-d H:i:s')
		];
		Order::where('id', '=', $order_id)->update($order_data);

		if($order_status == 2){
			$msg = 'Vendor accepted your order';
		}else{
			$msg = 'Vendor declined your order';
		}

		$user_aws_tokens = UserAuthentication::where('status', '=', 1)->where('user_id', '=', $user_id)
			->select('apns_key');

		foreach ($user_aws_tokens as $user) {
			//TODO push notification here
			//return	$this->Pushnotification->sendPushChat($user->apns_key, $returnStatusMsg, 1, 'Order', '');
		}

	}

	public static function getOrderForUser($user_id){
//		$order = array();
//		foreach ($this->queryOrderUser($user_id) as $order) {
//			$order->shop_info = $this->Shop_model->queryShopByShopID($order->shop_id)[0];
//			array_push($order, $order);
//		}
//		return $this->Resmsg->returnResponse($order);


	}

	public static function getOrderForVendor($vendor_id, $shop_id){

		$inqueue_orders	= [];
		$completed_orders = [];

		foreach (Order::queryOrderVendor($vendor_id, $shop_id) as $order) {
			$order->user_info = User::getUser($order->user_id);

			// Upload to production tomorrow
			if ($order->is_use_pcode == '1') { // 1 is use promotion but chagne to 2 until ios fontend change logic
				$order->is_use_pcode = '2';
				$basket = json_decode($order->basket_log, true);
				$order->total_price = $basket['total'];
				$order->food_price 	= $basket['total_food'];
			}
			///////////////////////////////////////////////////////

			if ($order->status_for_vendor == 2){
				array_push($inqueue_orders, $order);
			}elseif ($order->status_for_vendor == 4) {
				array_push($completed_orders, $order);
			}

		}
		$orders = ['inqueue_orders' => $inqueue_orders, 'completed_orders' => $completed_orders];
		return $orders;

	}

	public static function queryOrderUser($user_id){
		$result = DB::select("SELECT 	orders.*,
											pcode.code AS promotion_code,
											pcode.value AS promotion_value,
											pcode.ptype AS promotion_type,
											order_status.description_user AS status_show
									FROM 	orders, order_status, pcode
									WHERE 	orders.user_id = $user_id
									AND 	orders.status_for_user = order_status.id
									AND     orders.pcode_id = pcode.id
									ORDER BY orders.id DESC
										");
		return $result;
	}

	public static function queryOrderVendor($vendor_id, $shop_id){
		$result = DB::select("SELECT 	orders.*,
											pcode.code AS promotion_code,
											pcode.value AS promotion_value,
											pcode.ptype AS promotion_type,
											order_status.description_vendor AS status_show
									FROM 	orders, order_status, pcode
									WHERE 	orders.vendor_id = $vendor_id
									AND     orders.shop_id	= $shop_id
									AND 	orders.status_for_vendor = order_status.id
									AND     orders.pcode_id = pcode.id
									ORDER BY orders.updated_at DESC
										");
		return $result;
	}

	public static function updateBasket($basket_id){
//		$query = $this->db->query("UPDATE 	basket
//									SET 	basket.status = 2
//									WHERE 	basket.id = $basket_id
//								");
//		if ($query) {
//			return true;
//		}
//		return false;
	}

	public static function getFirstOrderDateByShop($shop_id){
//
//		$query = $this->db->query("SELECT  SUBSTRING(oders.createdate, 1, 10) as createdate
//									FROM    oders
//									WHERE   oders.shop_id = $shop_id
//									AND (oders.is_transfer = 1)
//									ORDER BY oders.createdate ASC
//									LIMIT 0, 1
//									");
//		$reusult = $query->result();
//
//		if ($query->num_rows() > 0){
//			$row = $query->row_array();
//			$row['createdate'];
//		}else{
//			$row['createdate'] = 0;
//		}
//		return $row['createdate'];
	}


	public static function updateOrderIsTransfer($shop_id, $status){

		#for caluculate
		$current_date = date('Y-m-d H:i:s');
		$back_date = date("Y-m-d 23:59:59",strtotime("-8 days",strtotime($current_date)));

//		$sql = "UPDATE oders
//        		SET 	oders.is_transfer = 2
//				WHERE   oders.shop_id = $shop_id
//				AND 	oders.createdate <= '$back_date'
//				AND     (oders.status_for_vendor = 2 OR oders.status_for_vendor = 4)
//				AND     oderss.is_transfer = 1 ";
//		$query = $this->db->query($sql);

		Order::where('shop_id', '=', $shop_id)
			->where('created_at', '<=', $back_date)
			->where('is_transfer', '=', 1)
			->where(function ($query) {
				$query->orWhere('status_for_vendor', '=', 2)
					  ->orWhere('status_for_vendor', '=', 4);
			})
			->update(['is_transfer' =>  2]);
	}


	public static function getCountNewOrder($shop_id){
//		$sql = "SELECT COUNT(order_log.id) as new_order_count
//									FROM order_log
//									WHERE order_log.shop_id = $shop_id
//									AND order_log.status = 0
//								";
//		$result = DB::select($sql);

		$result = OrderLog::where('shop_id', '=', $shop_id)
					->where('status', '=', 0)
					->count();


		return $result;
	}


}