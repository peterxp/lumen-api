<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


final class BasketLocation extends Model
{
    protected $table = 'basket_location';

    protected $guarded = array();

    public $timestamps = false;


}