<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


final class UserVIP extends Model
{
    protected $table = 'user_vip';

    public $timestamps = false;

    protected $guarded = array();

}