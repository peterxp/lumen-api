<?php
namespace App\Models;

use DB;

use Illuminate\Database\Eloquent\Model;


final class UserSkip extends Model
{
    protected $table = 'user_skip';

    protected $fillable = array('');

    public static function getUserSkipProfile($user_id){

        $user_skip = UserSkip::where('id', '=', $user_id)->first();

        $result['profile'] = $user_skip;
        $result['address'] = [
            'data' => [],
            'status' => 'true',
            'message'=>'success'
        ];
        $result['auth_key'] = $user_skip['auth_key'];
        $result['skip'] = true;
        return $result;
    }


}