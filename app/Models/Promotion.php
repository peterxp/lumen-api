<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;


final class Promotion extends Model
{
    protected $table = 'pcode';

    protected $fillable = [];

    public static function getPromotion($code){

        $promotion = Promotion::where('code', '=', $code)->first();
        if(count($promotion) > 0){
            $result = $promotion;
        }else{
            $result = false;
        }
        return $result;
    }

    public static function validateCode($shop_id=0, $user_id, $code, $amount, $total_amount=0){

        $promotion = Promotion::getPromotion($code);

        if ($promotion){
            if ($promotion->ptype == 1) {
                $result = Promotion::checkPromotionType1($shop_id, $user_id, $code, $amount, $total_amount, $promotion);
            }elseif ($promotion->ptype == 2) {
                $result = Promotion::checkPromotionType2($promotion);
            }elseif ($promotion->ptype == 3) {
                $result = Promotion::checkPromotionType4($shop_id, $user_id, $code, $amount, $total_amount, $promotion);
            }elseif ($promotion->ptype == 4) {
                $result = Promotion::checkPromotionType4($shop_id, $user_id, $code, $amount, $total_amount, $promotion);
            }
        }else{
            return Message::checkPromotionFalse();
        }
        return $result;
    }

    public static function checkPromotionType1($shop_id, $user_id, $code, $amount, $total_amount, $promotion){

        //promotion discount percent type
        $discount_value = (int)$amount * ((int)$promotion->value / 100);
        $promotion->vip_info = '';
        $promotion->value_show = '-'.(int)$discount_value.' THB';

        return $promotion;
    }

    public static function checkPromotionType2($promotion){
        //Promotion::checkPromotionRule($user_id, $amount, $pcode, $promotion);
        return false;
    }

    public static function checkPromotionType3($promotion){
        // VIP
        $promotion->vip_info = Promotion::queryVIPUSER($promotion->user_vip_id);
        $promotion->value_show = '';

        return $promotion;
    }

    public static function checkPromotionType4($shop_id, $user_id, $pcode, $amount, $total_amount, $promotion){

        if ($promotion->discount_type == 1) { // Discount type 1 is a discount from food price
            if (Promotion::checkPromotionRule4($user_id, $amount, $pcode, $promotion)){
                $promotion->vip_info = '';
                if ((int)$amount <= $promotion->value){
                    $promotion->value = $amount;
                }
                $promotion->value_show = '-'.$promotion->value.' THB';
                $promotion->used_count = Promotion::checkPromotionHistoryCount($promotion->id);
                return $promotion;
            }
        }

        elseif ($promotion->discount_type == 2){ // Discount type 1 is a discount from total price

            if (Promotion::checkPromotionRule4($user_id, $total_amount, $pcode, $promotion)){
                $promotion->vip_info = '';
                if ((int)$total_amount <= $promotion->value){
                    $promotion->value = $total_amount;
                }
                $promotion->value_show = '-'.$promotion->value.' THB';
                $promotion->used_count = Promotion::checkPromotionHistoryCount($promotion->id);

                return $promotion;
            }
        }

    }

    public static function checkPromotionRule4($user_id, $amount, $pcode, $promotion){
        if (Promotion::checkPromotionHistoryCount($promotion->id) < $promotion->limit_count) {
            $nowDate = date('Y-m-d H:i:s');

            if (Promotion::checkInRange($promotion->startdate, $promotion->enddate, $nowDate) && $promotion->min_amount <= $amount) {
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public static function checkPromotionHistoryCount($pcode_id){

        $sql = "SELECT COUNT(promotion_logs.pcode_id) AS count_used_code
                FROM  promotion_logs
				WHERE promotion_logs.pcode_id = $pcode_id";
        $row = DB::select($sql);
        $result = $row->count_used_code;

        return $result;
    }

    public static function checkInRange($start_date, $end_date, $date_from_user){
        // Convert to timestamp
        $start_ts   = strtotime($start_date);
        $end_ts 	= strtotime($end_date);
        $user_ts 	= strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    public static function updateVIPBalance($user_id, $user_vip_id, $balance){
        $vip = UserVIP::where('id', '=', $user_vip_id)
                      ->update(['balance'=>$balance]);
        return $vip;
    }

    public static function queryVIPUSER($user_vip_id){
        return UserVIP::where('id', '=', $user_vip_id)->get();
    }


    ############### This function rule use only ONNERXMAS promotion #################

    public static function checkPromotionRule($user_id, $amount, $pcode, $promotion){
        if (Promotion::checkOrdersHistory($user_id) && Promotion::checkOrderHistoryCount(6, $promotion->limit_count)) {
                $nowDate = date('Y-m-d H:i:s');
                if (Promotion::check_in_range($promotion->startdate, $promotion->enddate, $nowDate) && $promotion->min_amount <= $amount) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
    }

    #Check Order History use code
    public static function checkOrdersHistory($user_id){
        $sql = "SELECT 	*
                FROM 	orders
                WHERE 	orders.is_use_pcode = 1
                AND		orders.user_id = $user_id
                AND 	orders.pcode_id = 6  # ONNERXMAS CODE
										";
        $row = DB::select($sql);
        if (count($row) > 0){
            return false;
        }
        return true;
    }

    public static function checkOrderHistoryCount($pcode_id, $limit_count){
        $sql = "SELECT COUNT(orders.pcode_id) AS count_used_code
                FROM  orders
                WHERE orders.pcode_id = $pcode_id
              ";

        $query_count = DB::select($sql);
        if ($query_count->count_used_code < $limit_count) {
            return true;
        }
        return false;
    }


}