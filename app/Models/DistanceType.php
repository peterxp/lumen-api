<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


final class DistanceType extends Model
{
    protected $table = 'distance_type';

    protected $guarded = array('');

    public $timestamps = false;


}