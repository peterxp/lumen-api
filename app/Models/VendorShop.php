<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
//use App\Models\DeliveryRule;
use App\Models\Menu;
use Illuminate\Database\Eloquent\Model;


final class VendorShop extends Model
{
    protected $table = 'shop';

    protected $fillable = array('');


    /**
     * Get All shop with delivery rule and max distance
     */
    public static function getshopAllWithMaxDistance(){

        $returnData = array();
        $shop_list = Shop::queryShopWithMaxDistance();

        foreach($shop_list as $shop){

            $shop->delivery_ranges = Shop::queryDeliveryRuleTypeRange($shop->id);
            if(Shop::queryCheckAllPlace($shop->id)){
                $shop->delivery_places = Shop::queryDeliveryRuleTypePlaceAllThai($shop->id);
            }else{
                $shop->delivery_places = Shop::queryDeliveryRuleTypePlace($shop->id);
            }

            $shop->open_rules = Shop::queryOpenShopRule($shop->id);

			#check shop that have menus only to show
			/*
            if (count(Menu::queryMenusByShop($shop->id)) > 0){
                array_push($returnData, $shop);
            }*/

			#check shop that have menus only to show
			if (count(Menu::checkActiveMenuByShop($shop->id)) > 0){
				array_push($returnData, $shop);
			}
        }

        # find shop open by date time
        $shopOpenArray = Shop::findShopOpen();

        # check condition to collect only open shop by shop.id
        $openShop = array();
        $closeShop = array();
        foreach($returnData as $key => $val){
            if(in_array($val->id, $shopOpenArray)){
                $openShop[] = $val;
            }else{
                $closeShop[] = $val;
            }
        }

		// Add it to the laravel log

		$returnData = array(
            'shops_open'  => $openShop,
            'shops_close' => $closeShop,
            'version'     => '1.0.3'
        );

        return $returnData;
    }



    #It concern with shop open rule
    public static function queryShopWithMaxDistance(){

		$result = DB::select("
				SELECT  shop.*,
			    category.name as category_name,
			    category.iconurl AS category_image,
			    shop_location.latitude,
			    shop_location.longitude,
			    shop_location.address_id,
			    shop_status.description as shops_status,

			    (SELECT     MAX(distance_type.end)
			        FROM    distance_type
					INNER JOIN delivery_rule ON delivery_rule.typeid = distance_type.id
			        WHERE   (delivery_rule.shop_id = shop.id) AND (delivery_rule.typeinout = 1)
			    )  as max_distance

			    FROM    shop
				INNER JOIN category ON shop.category_id = category.id
				INNER JOIN shop_location ON shop.id = shop_location.shop_id
				INNER JOIN shop_status ON shop.status = shop_status.id

				WHERE shop.status = 1
			    GROUP BY shop.id
			    ORDER BY shop.sort_number ASC
			    ");

        return $result;
    }

	public static function queryShopWithMaxDistanceByShop($shop_id){
		$result = DB::select("SELECT 	shop.*,
									        category.name as category_name,
									        category.iconurl AS category_image,
									        shop_location.latitude,
									        shop_location.longitude,
									        shop_location.address_id,
									        shop_status.description as shops_status,
									        (SELECT 	MAX(distance_type.end)
												FROM 	delivery_rule, distance_type
												WHERE 	delivery_rule.shop_id = shop.id
												AND 	delivery_rule.typeinout = 1
												AND 	delivery_rule.typeid = distance_type.id
									        )  as max_distance

									FROM 	shop, category, shop_location, shop_status,delivery_rule, distance_type
									WHERE 	shop.category_id = category.id
									AND 	shop.id = shop_location.shop_id
									AND 	shop.status = shop_status.id
									AND 	shop.status = 1
									AND 	shop.id = $shop_id
									GROUP BY shop.id

								");
		return $result;
	}


    public static function queryDeliveryRuleTypeRange($shop_id){
        $rules = DB::select("SELECT  	delivery_rule.id,
											delivery_rule.dayspend,
									        delivery_rule.timespend,
									        delivery_rule.price,
									        distance_type.*
									FROM shop, delivery_rule, distance_type
									WHERE shop.id = $shop_id
									AND shop.id = delivery_rule.shop_id
									AND delivery_rule.typeinout = 1
									AND distance_type.id = delivery_rule.typeid
								");
        return $rules;
    }


//    public static function queryCheckAllPlace($shop_id){
//        $data = DB::select("SELECT	*
//									FROM 	delivery_rule
//									WHERE 	delivery_rule.shop_id = $shop_id
//									AND		delivery_rule.typeid = 999
//								");
//        if (count($data) > 0){
//            return true;
//        }else{
//            return false;
//        }
//    }

	public static function queryCheckAllPlace($shop_id){
		$data  = DeliveryRule::where('shops_id', $shop_id)
								->where('typeid', '999');

		$result = ($data)? true : false;
		return $result;
	}

    public static function queryDeliveryRuleTypePlace($shop_id){
        $data = DB::select("SELECT  	delivery_rule.id,
											delivery_rule.dayspend,
									        delivery_rule.timespend,
									        delivery_rule.price,
									        place.*
									FROM shop, delivery_rule, place
									WHERE shop.id = $shop_id
									AND shop.id = delivery_rule.shop_id
									AND delivery_rule.typeinout = 2
									AND place.id = delivery_rule.typeid
								");
        return $data;
    }

    public static function queryDeliveryRuleTypePlaceAllThai($shop_id){
        $data = DB::select("SELECT	#p1.delivery_rule_id,
									       	IFNULL(p2.dayspend, p1.dayspend) AS dayspend,
									        IFNULL(p2.timespend, p1.timespend) AS timespend,
									        IFNULL(p2.price, p1.price) AS price,
									        p1.place_id,
									        p1.name_th,
									        p1.name_en
									FROM   (	SELECT	#delivery_rule.id AS delivery_rule_id,
									       				delivery_rule.dayspend   AS dayspend,
									       				delivery_rule.timespend	AS timespend,
									       				delivery_rule.price		AS price,
									    				place.id 				AS place_id,
									       				place.name_th 			AS name_th,
									    				place.name_en 			AS name_en
									       		FROM 	shop, delivery_rule, place, shop_location
									       		WHERE 	shop.id = $shop_id
									            AND 	shop.id = delivery_rule.shop_id
									            AND 	delivery_rule.typeinout = 2
									            AND 	shop_location.shop_id = shop.id

									        	AND     CASE WHEN (delivery_rule.is_flat = 0) THEN place.id != shop_location.place_id
									        			ELSE place.id = shop_location.place_id END

									        	#AND     delivery_rule.is_flat = 0
									           # AND 	place.id != shop_location.place_id
									            AND 	place.id != 0 AND place.id != 999

									    ) p1

									LEFT JOIN
									   	(
									       		SELECT	#delivery_rule.id AS delivery_rule_id,
									       				delivery_rule.dayspend   AS dayspend,
									       				delivery_rule.timespend	AS timespend,
									       				delivery_rule.price		AS price,
									    				place.id 				AS place_id,
									       				place.name_th 			AS name_th,
									    				place.name_en 			AS name_en
									            FROM 	shop, delivery_rule, place
									            #WHERE 	shop.id = 296
									            WHERE 	shop.id = $shop_id
									            AND 	shop.id = delivery_rule.shop_id
									            AND 	delivery_rule.typeinout = 2
									        	#AND     delivery_rule.is_flat = 0
									            AND 	place.id = delivery_rule.typeid
									   ) p2

									ON p2.place_id = p1.place_id
									GROUP BY p1.place_id


								");
        return $data;
    }


    public static function queryOpenShopRule($shop_id){
        $data = DB::select("SELECT shop_open_rule.*,
										(SELECT 	day.symbol_en
								         FROM 		day
								         WHERE 		day.id = shop_open_rule.start_day) AS start_day_name,
								         (SELECT 	day.symbol_en
								         FROM 		day
								         WHERE 		day.id = shop_open_rule.close_day) AS close_day_name,
								         DATE_FORMAT(shop_open_rule.start_time,'%H:%i') AS start_time_short,
       	 								 DATE_FORMAT(shop_open_rule.close_time,'%H:%i') AS close_time_short
									FROM 	shop_open_rule
									WHERE 	shop_open_rule.shop_id = $shop_id
								");
        return $data;
    }



    public static function calculate_hours($dayspend = 0, $timespend){
        $arr = explode(":", $timespend);

        if($dayspend > 0){
            $minute_from_day = $dayspend * 24;
        }else{
            $minute_from_day = 0;
        }

        $h = $arr['0'];
        $m = $arr['1'];
        $s = $arr['2'];

        $min_from_h = (int)$h;
        $min_from_m	= (int)$m;

        $hours = $minute_from_day + $min_from_h;
        $minutes = $min_from_m;
        $seconds = $s;

        return $hours.":".$minutes.":".$seconds;
    }



    public static function findShopOpen(){
        //get day of week 1=Mon, 2=Tue, 3=Wed, 4=Thu, 5=Fri, 6=Sat, 7=Sun
        $day_of_week = date('N');
        //get current time value by UTC+7
        $current_time = date('H:i:s');

        //start_day, close_day, start_time, close_time
        $data = DB::select("
        	SELECT shop_id
			FROM shop_open_rule
			INNER JOIN shop ON shop.id = shop_open_rule.shop_id
			WHERE (shop.status = 1)
			AND ('$day_of_week' BETWEEN start_day  AND close_day )
			AND ('$current_time' BETWEEN start_time AND close_time )
			ORDER BY shop.updated_at DESC
		");

        $shop_id_array = array();
        foreach ($data as $key => $val) {
            array_push($shop_id_array, $val->shop_id);
        }
        return $shop_id_array;
    }




}