<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


final class Bank extends Model
{
    protected $table = 'bank';

    public $timestamps = false;

    protected $guarded = array();

}