<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


final class VendorAuthentication extends Model
{
    protected $table = 'vendor_authentication';
//    protected $fillable = array('');

    public static function authenticateOnlyAuthKey($auth_key){
        $user  = VendorAuthentication::where('auth_key', '=', $auth_key)->get();
        $result = (count($user)> 0)? true : false;
        return $result;
    }

    //Check user authenticate
    public static function authenticate($auth_key, $id){
        $user = VendorAuthentication::where('vendor_id', '=',$id)
            ->where('auth_key', '=', $auth_key)
            ->get();
        $result = (count($user)> 0)? true : false;
        return $user;
    }

    public static function get_endpoint_key($auth_key){
        $auth = VendorAuthentication::where('auth_key', $auth_key)->first();
        $apns_key = ($auth->apns_key)? $apns_key : "0";
        return $apns_key;
    }

    public static function createAuthentication($vendor_id, $system_info, $device_token){

        if($device_token == "not allow token id"){
            $sns_endpoint = "user_not_allow_notification";
        }else if ($device_token == "simulator"){
            $sns_endpoint = "user_simulator";
        }else{
            #TODO enable here
            $user_data = "vendor,".$vendor_id;
            //$sns_endpoint = PushNotification::getSNSEndPoint('vendor', $device_token, $user_data);
            $sns_endpoint = "TEST";
        }

        #create vendor_authentication
        $auth = new VendorAuthentication();
        $auth->vendor_id    = $vendor_id;
        $auth->status       = 1;
        $auth->system_info  = $system_info;
        $auth->auth_key     = sha1($system_info);
        $auth->apns_key     = $sns_endpoint;

        if($auth->save()) {
            $result = $sns_endpoint;
        }else {
            $result = "";
        }
        return $result;
    }


    public static function getAWSKey($vendor_id){
        $result = VendorAuthentication::where('vendor_id', '=', $vendor_id)
            ->where('status', '=', 1)
            ->select(['apns_key'])
            ->get();
        return $result;
    }
}