<?php
namespace App\Models;

use DB;
use App\Models\UploadFile;

use Illuminate\Database\Eloquent\Model;


final class MenuImage extends Model
{
    protected $table = 'menu_image';

	protected $guarded = [''];

	public $timestamps = false;

	public static function getImageByMenu($menu_id){
		$data = MenuImage::where('menu_id', '=', $menu_id)
			->where('status', '=', 1)
			->select('id','image_url')->get();
		return $data;
	}

	public static function getImageByMenuDraft($menu_id){

		$data = MenuImage::where('menu_id', '=', $menu_id)
						->where('status', '=', 2)
						->select('id','image_url')->get();
		return $data;
	}

	public static function insertImageMenu($vendor_id, $shop_id, $menu_id, $status, $menu_image_1, $menu_image_2, $menu_image_3){

		$upload = new UploadFile();

		if ($menu_image_1 != null){
			$img1_path  = '';
			if ($status == 1){
				$img1_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products/'.$menu_id.'/1.jpg';
			}else{
				$img1_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products_draft/'.$menu_id.'/1.jpg';
			}
			$image_url_1  = $upload->upload_image($menu_image_1, $img1_path);
			$insertImage = [
				'image_url' => $image_url_1,
				'menu_id'	=> $menu_id,
				'status'	=> $status
			];
			MenuImage::create($insertImage);

		}

		if ($menu_image_2 != null){
			$img2_path  = '';
			if ($status == 1){
				$img2_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products/'.$menu_id.'/2.jpg';
			}else{
				$img2_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products_draft/'.$menu_id.'/2.jpg';
			}
			$image_url_2  = $upload->upload_image($menu_image_2, $img2_path);
			$insertImage = array(
				'image_url' => $image_url_2,
				'menu_id'	=> $menu_id,
				'status'	=> $status
			);
			MenuImage::create($insertImage);
		}

		if ($menu_image_3 != null){
			$img3_path  = '';
			if ($status == 1){
				$img3_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products/'.$menu_id.'/3.jpg';
			}else{
				$img3_path  = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products_draft/'.$menu_id.'/3.jpg';
			}
			$image_url_3  = $upload->upload_image($menu_image_3, $img3_path);
			$insertImage = array(
				'image_url' => $image_url_3,
				'menu_id'	=> $menu_id,
				'status'	=> $status
			);
			MenuImage::create($insertImage);
		}
	}

	public static function deleteMenuImage($menu_id, $status){

		MenuImage::where('menu_id', '=', $menu_id)
			     ->where('status', '=', $status)
				 ->delete();
	}

	public static function deleteFolderImageByMenu($vendor_id, $shop_id, $menu_id){

		$path_arr = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products/'.$menu_id;
		$S3 = new UploadFile();
		return $S3->AwsS3DeleteFolder($path_arr);

	}


	public static function deleteFolderImageByMenuDraft($vendor_id, $shop_id, $menu_id){

		$path_arr = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products_draft/'.$menu_id;
		$S3 = new UploadFile();
		return $S3->AwsS3DeleteFolder($path_arr);

	}

}