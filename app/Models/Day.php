<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


final class Day extends Model
{
    protected $table = 'day';

    protected $guarded = array('');

    public $timestamps = false;


}