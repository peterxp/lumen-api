<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


final class ShopLocation extends Model
{
    protected $table = 'shop_location';

    protected $guarded = array();

    public $timestamps = false;


    public static function getLocation($shop_id){

        $data = ShopLocation::select(
                'shop_location.id',
                'shop_location.latitude',
                'shop_location.longitude',
                'place.name_th',
                'place.name_en')
            ->where('shop_location.shop_id', '=', $shop_id)
            ->join('place', 'shop_location.place_id', '=', 'place.id')
            ->first();

        return $data;

    }
}