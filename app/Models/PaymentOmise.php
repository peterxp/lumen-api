<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class PaymentOmise extends Model {

    function __construct(){

        if( (array_shift((explode(".",$_SERVER['HTTP_HOST'])))) == "api-naja")
        {
            define('OMISE_API_VERSION', '2015-11-17');
            define('OMISE_PUBLIC_KEY', 'pkey_test_525faet623827q4zlu4'); //Public key
            define('OMISE_SECRET_KEY', 'skey_test_525faet622n9lj69ftu'); //Secret key

        } else {

            define('OMISE_API_VERSION', '2015-11-17');
            define('OMISE_PUBLIC_KEY', 'pkey_test_525faet623827q4zlu4'); //Public key
            define('OMISE_SECRET_KEY', 'skey_test_525faet622n9lj69ftu'); //Secret key

        }

    }

    public function init(){

        $customer = \OmiseCustomer::retrieve();
        return (array)$customer;
    }


    /**
     * @param $recipient_id
     * @param $amount
     * @return string
     */
    public function transfer($recipient_id, $amount){

        $transfer = \OmiseTransfer::create([
            "amount"    => $amount,
            "recipient" => $recipient_id
        ]);

        return $omise_log = (array)$transfer;
    }

}