<?php
namespace App\Models;

use Aws\Common\Aws;
use Aws\Sns\SnsClient as SnsClient;


use Illuminate\Database\Eloquent\Model;


class APNS extends Model {

    //ref http://docs.aws.amazon.com/aws-sdk-php/v2/api/class-Aws.Sns.SnsClient.html#_publish
    //ref http://docs.aws.amazon.com/sns/latest/dg/mobile-push-send-custommessage.html
    # AWS.SNS.MOBILE.APNS.TTL
    function __construct(){

        $this->sns_client = new SnsClient([
            'version'     => env('AWS_VERSION'),
            'region'      => env('AWS_REGION'),
            'debug'       => env('AWS_DEBUG'),
            'credentials' => [
                'key'       => env('AWS_KEY'),
                'secret'    => env('AWS_SECRET')
            ]
        ]);

        if( (array_shift((explode(".",$_SERVER['HTTP_HOST'])))) == "api-naja")
        {
            $this->SERVICE_ENVIRONMENT = "APNS";
            $this->ARN_KEY  = [
                "user"  => "arn:aws:sns:ap-southeast-1:xyz:app/APNS/onner_customer_ios_distribution",
                "vendor"=> "arn:aws:sns:ap-southeast-1:xyz:app/APNS/onner_vendor_ios_distribution"
            ];
        }
        else
        {
            $this->SERVICE_ENVIRONMENT = "APNS_SANDBOX";
            $this->ARN_KEY  = [
                "user"  => "arn:aws:sns:ap-southeast-1:xyz:app/APNS_SANDBOX/xyz.dev.com",
                "vendor"=> "arn:aws:sns:ap-southeast-1:xyz:app/APNS_SANDBOX/vendor.xyz.com"
            ];
        }
    }

    public function createSNSEndPoint($ARN_TYPE, $device_token, $custom_data){

        try {

            $result =  $this->sns_client->createPlatformEndpoint([
                'PlatformApplicationArn' 	=> $this->ARN_KEY[$ARN_TYPE],
                'Token' 					=> $device_token,
                'CustomUserData' 			=> $custom_data
            ]);
            return $result['EndpointArn'];
        } catch (Exception $e) {
            return $e;
        }
    }

    function deleteEndpoint($endpoint){
        try {
            $result = $this ->sns_client->deleteEndpoint([
                'EndpointArn' => $endpoint
            ]);
            return $result;
        } catch (Exception $e) {
            return $e;
        }
    }

    function sendPush($target="", $message="", $badge=0, $action="", $value="", $sound="default"){

        $result = null;
        try {
            $result = $this ->sns_client->publish([
                "TargetArn" => $target,
                "Message"   => json_encode([
                    $this->SERVICE_ENVIRONMENT	=> json_encode([
                        "aps" => [
                            "alert" => $message,
                            "sound" =>"Message.mp3",
                            "action"=> "push_message",
                            "content-available"=>1
                        ]
                    ])
                ]),
                "MessageAttributes" => [
                    "AWS.SNS.MOBILE.APNS.TTL" => [
                        "DataType"      => "Number",
                        "StringValue"   => "3600"
                    ]
                ],
                "MessageStructure" => "json"
            ]);
        } catch (Exception $e) {
            return $e;
        }
        return $result["MessageId"];
    }


    function sendPushOrder($target="", $message="", $badge=0, $sound="default", $order_object, $count=1){

        $result = null;
        try{
            $result = $this ->sns_client->publish([
                "TargetArn" => $target,
                "MessageAttributes" => [
                    "AWS.SNS.MOBILE.APNS.TTL" => ["DataType" => "Number","StringValue" => "3600"]
                ],
                "MessageStructure" => "json",
                "Message" => json_encode([
                    "default" => $message,
                    $this->SERVICE_ENVIRONMENT => json_encode([
                        "aps" => [
                            "alert"  => $message,
                            "sound"  => $sound,
                            "badge"  => (int)$count,
                            "action" => "push_order",
                            "content-available" => 1

                        ],
                        "order_object" => $order_object
                    ])

                ])
            ]);
        }
        catch (Exception $e){
            return $e->getMessage();
        }

        return $result["MessageId"];
    }

    function sendPushChat($target="",$message="",$badge=0,$sound="default",$chat_object , $count=0){

        $result = null;
        try{
            $this ->sns_client->publish([
                "TargetArn" => $target,
                "MessageAttributes" => [
                    "AWS.SNS.MOBILE.APNS.TTL" => ["DataType" => "Number","StringValue" => "3600"]
                ],
                "MessageStructure" => "json",
                "Message" => json_encode([
                    "default" => $message,
                    $this->SERVICE_ENVIRONMENT => json_encode([
                        "aps" => [
                            "alert" => $message,
                            "sound" => $sound,
                            "badge" => (int)$count,
                            "content-available" => 1,
                            "action" => "push_message"
                        ],
                        "chat_object" => $chat_object
                    ])
                ])
            ]);
        }
        catch (Exception $e){
            return $e->getMessage();
        }
        return $result["MessageId"];
    }


}