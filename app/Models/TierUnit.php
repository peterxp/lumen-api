<?php
namespace App\Models;

use DB;


use Illuminate\Database\Eloquent\Model;


final class TierUnit extends Model
{
    protected $table = 'tierunit';

    protected $fillable = array('');

	public $timestamps = false;

}