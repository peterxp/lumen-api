<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
use App\Models\MenuImage;
use App\Models\Tier;
use App\Models\UploadFile;

use Illuminate\Database\Eloquent\Model;


final class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = array('');

	public static function checkExistMenu($menu_id){
		$menu = Menu::where('id', '=', $menu_id)->get()->first();
		$result = (count($menu)> 0)? true : false;
		return $result;
	}


//	public static function checkMenuInBasket($shop_id, $user_id){
//		$poOrder = $this->Basket_model->queryBasket($user_id,$shop_id);
//		foreach ($poOrder as $po) {
//			if (!$this->checkMenu($po->menu_id)){
//				return $this->Resmsg->returnResponse('unavaliable');
//			}
//		}
//		return $this->Resmsg->returnResponse('avaliable');
//	}

	public static function checkMenu($menu_id){
		if (count(Menu::queryMenu($menu_id)) > 0) {
			$menu = Menu::queryMenu($menu_id)[0];
			if ($menu->mstatus_id == 1)
				return true;
			else
				return false;
		}else{
			return false;
		}
	}

	public static function getByShop($shop_id){
		$result = [];
		$menu_list = Menu::getMenuByShop($shop_id);
		foreach($menu_list as $menu){
			$menu->tiers 	= Tier::getTierByMenu($menu->id, 1);
			$menu->images 	= MenuImage::getImageByMenu($menu->id, 1);
			array_push($result, $menu);
		}
		return $result;
	}

	public static function getMenusOutStock($shop_id){
		$result = array();
		$menu_out_of_stock = Menu::queryMenuOutStockByShop($shop_id);
		foreach( $menu_out_of_stock as $menu){
			$menu->tiers 	= Tier::getTierByMenu($menu->id, 1);
			$menu->images 	= MenuImage::getImageByMenu($menu->id);
			array_push($result, $menu);
		}
		return $menu_out_of_stock;
	}

	public static function queryMenu($menu_id){
		$data = Menu::where('id', '=', $menu_id)->get();
		return $data;
	}

	public static function queryMenuWithStock($menu_id){
//		$data = DB::select("SELECT menu.*
//									FROM  menu
//									WHERE menu.id = $menu_id
//									AND   menu.mstatus_id = 1
//								");

		$data = Menu::where('id', '=', $menu_id)
			->where('status', '=', 1)->first();
		return $data;
	}

	public static function queryTier($tier_id){
//		$data = DB::select("SELECT tier.*,
//										  tierunit.description AS unit
//									FROM  tier,tierunit
//									WHERE tier.id = $tier_id
//									AND   tier.status = 1
//									AND   tierunit.id = tier.tierunit_id
//								");
		$data = Tier::select('tier.*', 'tierunit.description AS unit')
			->where('tier.id', '=', $tier_id)
			->where('tier.status', '=', 1)
			->join('tierunit', 'tier.tierunit_id', '=', 'tierunit.id')
			->first();

		return $data;
	}

	public static function getMenuByShop($shop_id){

		$sql= "SELECT menu.*, menu_status.description as status
				FROM menu
				INNER JOIN menu_status
				ON menu.status = menu_status.id
				WHERE menu.shop_id = $shop_id
				AND menu_status.id = 1";

		$data = DB::select($sql);
		return $data;
	}

	//count to check existing menu
	public static function checkActiveMenuByShop($shop_id){
		return Menu::where('menu.shop_id', $shop_id)
				->where('menu.status', 1)
				->count();
	}

	public static function queryMenuOutStockByShop($shop_id){

		$sql = "SELECT 	menu.*, menu_status.description AS status
				FROM 	menu
				INNER JOIN menu_status ON menu.status = menu_status.id
				WHERE 	menu.shop_id = $shop_id
				AND     menu_status.id = 2
				GROUP BY menu.id";
		$data = DB::select($sql);

		return $data;
	}

	public static function stockControl($menu_id, $status){
		$chk = Menu::where('id', '=', $menu_id)
			->update(['status' => $status]);

		return ($chk)? true : false;
	}

    //Vendor create menu
	public static function createMenu($vendor_id, $shop_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers, $menu_id_ref = ''){

		$menu = new Menu();
		$menu->id = $menu_id_ref;
		$menu->name = $name;
		$menu->description = $description;
		$menu->main_ingredient = $main_ingredient;
		$menu->shop_id 	= $shop_id;
		$menu->status 	= 1;
		$menu->save();

		$menu_id = $menu->id;

		if($menu_id > 0){
			//update menu cover image
			Menu::updateMenuCover($vendor_id, $shop_id, $menu_id, $cover);

			//insert image menu
			MenuImage::insertImageMenu($vendor_id, $shop_id, $menu_id, $status=1, $image1, $image2, $image3);

			//insert tier
			Tier::insertTierMenu($menu_id, $tiers, 1);

			$result = "createmenu";
		}else{
			$result = "can_not_create_menu";
		}

		return $result;
	}

	public static function updateMenuCover($vendor_id, $shop_id, $menu_id, $cover_image){

		$upload = new UploadFile();
		$path = "images/vendors/".$vendor_id."/shops/".$shop_id."/products/".$menu_id."/cover.jpg";
		$image_url = $upload->upload_image($cover_image, $path);

		$chk_error = strpos($image_url, "error");
		# if not error
		if($chk_error == false ){
			# update image cover url to database
			Menu::where('id', '=', $menu_id)->update(['cover'=>$image_url]);
		}
	}

	public static function updateMenu($vendor_id, $shop_id, $menu_id, $cover, $image1, $image2, $image3, $name='', $main_ingredient='', $description='', $tiers){

		//update menu cover
		if($cover != null){
			Menu::updateMenuCover($vendor_id, $shop_id, $menu_id, $cover);
		}

		//update menu data
		$menuArr = [
			'name' 				=> $name,
			'description' 		=> $description,
			'main_ingredient' 	=> $main_ingredient
		];
		$chk = Menu::where('id', '=', $menu_id)->update($menuArr);

		//delete old menu image
		MenuImage::deleteMenuImage($menu_id, 1);

		//insert new menu image
		MenuImage::insertImageMenu($vendor_id, $shop_id, $menu_id, 1, $image1, $image2, $image3);

		//delete tier menu
		Tier::deleteTierMenu($menu_id, 1);
		//insert or update or delete tier
		//Tier::insertTierMenuWithBackup($menu_id, $tiers, $backup_tier_ids, 1);
		Tier::insertTierMenu($menu_id, $tiers, $status = 1);

		return "updatemenu";

	}

	public static function deleteMenu($vendor_id, $shop_id, $menu_id){

		$status = 1; //publish menu only

		$chk = Menu::where('id', '=', $menu_id)
			->where('shop_id', '=', $shop_id)
			->delete();

		if($chk){

			#TODO change to set flag show/not show
			#delete image in s3
			Tier::deleteTierMenu($menu_id, $status);
			MenuImage::where('menu_id', '=', $menu_id)
				->where('status', '=', $status)
				->delete();

			return Message::response("deleteMenu");

		}else{
			return Message::responseFalse("FailDeleteMenu");
		}
	}

    public static function getShopMenu($vendor_id, $shop_id){

		$menuArray = [];
		$menus = Menu::getMenuByShop($shop_id);
		foreach($menus as $menu){
			$menu->tiers = Tier::getTierByMenu($menu->id, 1); // 1 = real tier
			$menu->images = MenuImage::getImageByMenu($menu->id);
			array_push($menuArray, $menu);
		}

		$menu_draft = MenuDraft::getMenuByShop($shop_id);
		$draftMenuArray = [];
		foreach($menu_draft as $menu){
			$menu->tiers = Tier::getTierByMenu($menu->id, 2); // 2 = draft tier
			$menu->images = MenuImage::getImageByMenuDraft($menu->id);
			array_push($draftMenuArray, $menu);
		}

		$returnData = [
			'menus_instock' 	=> $menuArray,
			'menus_outstock' 	=> Menu::getMenusOutStock($shop_id),
			'menus_draft' 		=> $draftMenuArray
		];

		return $returnData;
	}


}