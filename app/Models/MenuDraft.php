<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
//use App\Models\DeliveryRule;
use App\Models\MenuImage;
use App\Models\Tier;

use Illuminate\Database\Eloquent\Model;


final class MenuDraft extends Model
{
    protected $table = 'menu_draft';

    protected $fillable = array('');

	public static function deleteDraftMenu($menu_draft_id){

		$chk = MenuDraft::where('id', '=', $menu_draft_id)->delete();

		return ($chk)? true : false;

	}

//	function queryMenusDraftByShop($shop_id){
//		$query = $this->db->query("SELECT 	menu_draft.*,
//									        mstatus.description AS status
//									FROM 	menu_draft, mstatus
//									WHERE 	menu_draft.shop_id = $shop_id
//									AND 	mstatus.id = menu_draft.mstatus_id
//									GROUP BY menu_draft.id
//								");
//		return $query->result();
//	}

	public static function getMenuByShop($shop_id){

		$sql= "SELECT menu_draft.*, menu_status.description as status
				FROM menu_draft
				INNER JOIN menu_status
				ON menu_draft.status = menu_status.id
				WHERE menu_draft.shop_id = $shop_id
				AND menu_status.id = 1";

		$data = DB::select($sql);
		return $data;
	}


	//Vendor create draft menu
	public static function createDraft($vendor_id, $shop_id, $menu_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers){

		$menu = new MenuDraft();
		//$menu->id = $menu_id_ref;
		$menu->name = $name;
		$menu->description = $description;
		$menu->main_ingredient = $main_ingredient;
		$menu->shop_id 	= $shop_id;
		$menu->status 	= 1;
		$menu->menu_id_ref = $menu_id;
		$menu->save();

		$menu_draft_id = $menu->id;

		if($menu_draft_id > 0){

			//update menu cover image
			MenuDraft::updateMenuDraftCover($vendor_id, $shop_id, $menu_draft_id, $cover);

			//insert tier
			Tier::insertTierMenu($menu_draft_id, $tiers, 2);

			MenuImage::insertImageMenu($vendor_id, $shop_id, $menu_draft_id, $status=2, $image1, $image2, $image3);

			$result = "updatemenudraft";
		}else{
			$result = "can_not_create_menu_draft";
		}

		return $result;
	}


	public static function updateMenuDraftCover($vendor_id, $shop_id, $menu_draft_id, $cover_image){

		$upload = new UploadFile();
		$path = "images/vendors/".$vendor_id."/shops/".$shop_id."/products_draft/".$menu_draft_id."/cover.jpg";
		$image_url = $upload->upload_image($cover_image, $path);

		$chk_error = strpos($image_url, "error");
		# if not error
		if($chk_error == false ){
			# update image cover url to database
			MenuDraft::where('id', '=', $menu_draft_id)->update(['cover'=>$image_url]);
		}
	}

	public static function updateDraft($vendor_id, $shop_id, $menu_id, $menu_draft_id, $cover, $image1, $image2, $image3, $name='', $main_ingredient='', $description='', $tiers){

		if($cover != null){
			MenuDraft::updateMenuCover($vendor_id, $shop_id, $menu_id, $cover);
		}

		//update menu data
		$menuArr = [
			'name' 				=> $name,
			'description' 		=> $description,
			'main_ingredient' 	=> $main_ingredient
		];
		MenuDraft::where('id', '=', $menu_draft_id)->update($menuArr);

		MenuImage::deleteMenuImage($menu_id, 2);
		MenuImage::insertImageMenu($vendor_id, $shop_id, $menu_draft_id, 2, $image1, $image2, $image3);

		Tier::deleteTierMenu($menu_id, 2);
		Tier::insertTierMenu($menu_id, $tiers, $status = 2);

		return "updatemenudraft";
	}


	public static function updateMenuCover($vendor_id, $shop_id, $menu_id, $cover_image){

		$upload = new UploadFile();
		$path = "images/vendors/".$vendor_id."/shops/".$shop_id."/products_draft/".$menu_id."/cover.jpg";
		$image_url = $upload->upload_image($cover_image, $path);

		$chk_error = strpos($image_url, "error");
		# if not error
		if($chk_error == false ){
			# update image cover url to database
			Menu::where('id', '=', $menu_id)->update(['cover'=>$image_url]);
		}
	}


	public static function deleteMenu($vendor_id, $shop_id, $menu_id){

		$status = 2; //draft menu only

		$chk =MenuDraft::where('id', '=', $menu_id)
				->where('shop_id', '=', $shop_id)
				->delete();

		if($chk){

			Tier::deleteTierMenu($menu_id, $status);
			MenuImage::where('menu_id', '=', $menu_id)
				->where('status', '=', $status)
				->delete();

			return Message::response("deleteMenu");

		}else{
			return Message::responseFalse("FailDeleteDraftMenu");
		}

	}


}