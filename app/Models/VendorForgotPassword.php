<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

final class VendorForgotPassword extends Model
{
    protected $table = 'vendor_forgot_password';
    protected $fillable = array('');

    //disable updated_at
    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public static function checkToken($token){

        $vendor = DB::table('vendor_forgot_password')
            ->select('vendor.id', 'vendor.email', 'vendor_forgot_password.token')
            ->join('vendor', 'vendor_forgot_password.vendor_id', '=', 'vendor.id')
            ->where('vendor_forgot_password.token', $token)
            ->take(1)
            ->get();

        $result = (count($vendor))? $vendor[0] : NULL;
        return $vendor;
    }




}