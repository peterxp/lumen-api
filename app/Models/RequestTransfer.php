<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
use App\Models\Shop;
//use App\Models\DeliveryRule;
use App\Models\MenuImage;
use App\Models\Tier;

use Illuminate\Database\Eloquent\Model;


final class RequestTransfer extends Model
{
    protected $table = 'request_transfer';

	protected $guarded = array();

    public static function checkExisting($vendor_id, $shop_id){
        $today = date('Y-m-d');
        $sum = DB::select(" SELECT COALESCE(sum(request_transfer.id),0) as sum FROM `request_transfer`
                                WHERE `shop_id` = '$shop_id'
                                AND   `vendor_id` = '$vendor_id'
                                AND   SUBSTRING(created_at, 1, 10) = '$today' ");
        return $sum[0]->sum;
    }
}