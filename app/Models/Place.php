<?php
namespace App\Models;

use DB;

use Illuminate\Database\Eloquent\Model;

final class Place extends Model
{
    protected $table = 'place';

    protected $fillable = array('');

	public $timestamps = false;

}