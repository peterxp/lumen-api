<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

final class UserForgotPassword extends Model
{
    protected $table = 'user_forgot_password';

    //disable updated_at
    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public static function checkToken($token){

        $user = DB::table('user_forgot_password')
            ->select('user.id', 'user.email', 'user_forgot_password.token')
            ->join('user', 'user_forgot_password.user_id', '=', 'user.id')
            ->where('user_forgot_password.token', $token)
            ->take(1)
            ->get();

        $result = (count($user))? $user[0] : NULL;
        return $user;
    }




}