<?php
namespace App\Models;

use App\Http\Controllers\UserLocationController;
use DB;
use App\Models\Authentication;
use App\Models\VendorAuthication;
use App\Models\UserLocation;


use Illuminate\Database\Eloquent\Model;


final class User extends Model
{
    protected $table = 'user';

    protected $fillable = array('');


    public static function checkExistingByEmail($email){
        $user  = User::where('email', $email);
        $result = ($user)? true : false;
        return $result;


    }

    public static function getProfile($user_id, $auth_key){

        $user['profile'] = DB::table('user')
            ->where('user.id', '=', $user_id)
            ->join('gender', 'user.gender_id', '=', 'gender.id')
            ->select(
                'user.id',
                'user.facebook_id',
                'user.email',
                'user.device_count',
                'user.name',
                'user.last_name',
                'user.image_url',
                'user.phone',
                'gender.description as gender'
            )->first();

        //$user['profile'] =
        //check new user or old user
        $user['profile']->is_new_user = User::isNewUser($user_id);  // true or false

        $user['address'] = UserLocation::getByUserId($user_id);
        $user['auth_key']= sha1($auth_key);

        return $user;
    }

    public static function getUser($user_id){
        $result = $user['profile'] = DB::table('user')
            ->where('user.id', '=', $user_id)
            ->join('gender', 'user.gender_id', '=', 'gender.id')
            ->select(
                'user.id',
                'user.facebook_id',
                'user.email',
                'user.device_count',
                'user.name',
                'user.last_name',
                'user.image_url',
                'user.phone',
                'gender.description as gender'
            )->first();

        return $result;
    }

//    public static function getAddress($user_id){
//
//        $address = DB::select("
//            SELECT
//            user_locations.id,
//            user_locations.data,
//            user_locations.longitude,
//            address.house,
//            address.floor,
//            address.placename,
//            address.soi,
//            address.street,
//            address.subdistrict,
//            address.district,
//            address.province,
//            address.country,
//            address.postcode,
//            (SELECT name_en FROM place WHERE place.id = user_locations.place_id) AS name_en,
//            (SELECT name_th FROM place WHERE place.id = user_locations.place_id) AS name_th
//            FROM user_locations
//            INNER JOIN address ON user_locations.address_id = address.id
//            WHERE user_locations.user_id = '$user_id'");
//
//        return $address;
//    }


    public static function checkDuplicateByEmail($email){
        $user = User::where('email', $email)->get()->first();
        $result = (count($user)> 0)? true : false;
        return $result;
    }


    public static function resetPassword($email, $password){
        $chk = User::where('email', '=', $email)->update(['password' => sha1($password)]);
        if($chk){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    //is new user
    //compare if created_at > 1 minute it's mean new user
    public static function isNewUser($user_id){
        $current_date = date('Y-m-d H:i:s');
        $chk = DB::select(" SELECT * FROM user
									INNER JOIN uinfo ON (user.id = uinfo.id)
									WHERE user.id = '$user_id' AND created_at > '$current_date' - INTERVAL 1 MINUTE
								");
        if (count($chk) > 0) {
            $result = "1";
        }else{
            $result = "0";
        }
        return $result;
    }
}