<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


final class ShopOpenRule extends Model
{
    protected $table = 'shop_open_rule';

    public $timestamps = false;

    protected $fillable = array('shop_id', 'start_day', 'start_time', 'close_data', 'close_time');


    public static function getOpenRule($shop_id)
    {
        $data = DB::select("SELECT  shop_open_rule.id,
                                    shop_open_rule.start_day AS start_value,
                                    shop_open_rule.close_day AS close_value,
                                    (SELECT day.symbol_en FROM day WHERE shop_open_rule.start_day = day.id) AS start_day,
                                    (SELECT day.symbol_en FROM day WHERE shop_open_rule.close_day = day.id) AS close_day,
                                    shop_open_rule.start_time,
                                    shop_open_rule.close_time,
                                    DATE_FORMAT(shop_open_rule.start_time,'%H:%i') AS start_time_short,
                                    DATE_FORMAT(shop_open_rule.close_time,'%H:%i') AS close_time_short
                                FROM shop_open_rule
                                WHERE shop_open_rule.shop_id = $shop_id
                                ORDER BY shop_open_rule.start_day ASC
                            ");
        return $data;
    }

        //Improve sql performance by insert batch records
    public static function createBatch($arr, $shop_id){

        ShopOpenRule::deleteByShopID($shop_id);
        $rule_all = [];
        foreach($arr as $rule){
            $rule_data['shop_id']    = $rule['shop_id'];
            $rule_data['start_day']  = $rule['start_day'];
            $rule_data['start_time'] = (string)$rule['start_time'];
            $rule_data['close_day']  = $rule['close_day'];
            $rule_data['close_time'] = (string)($rule['close_time'] == "00:00:00")? "23:59:59":$rule['close_time'];

            $rule_all[] = $rule_data;
        }

        //Insert batch here (one time with multiple record)
        if(DB::table('shop_open_rule')->insert($rule_all)){
            return "openRules_updated";
        }else{
            return "Not complete update shop open rule";
        }
    }

    public static function deleteByShopID($shop_id){
        return ShopOpenRule::where('shop_id', '=', $shop_id)
                            ->delete();
    }

}