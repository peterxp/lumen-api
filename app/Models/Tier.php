<?php
namespace App\Models;

use DB;


use Illuminate\Database\Eloquent\Model;


final class Tier extends Model
{
    protected $table = 'tier';

    protected $fillable = array('');

	public $timestamps = false;

	//status (1=real menu, 2=draft menu)
	public static function getTierByMenu($menu_id, $status = 1){

		$sql = "SELECT tier.id, tier.price,
						tier.qty, tierunit.id AS tierunit_id,
						tierunit.description AS unit,
						tierunit.unit_type
				FROM 	tier
				INNER JOIN tierunit ON tierunit.id = tier.tierunit_id
				WHERE 	(tier.menu_id = $menu_id) AND (tier.status = $status) ";
		$data = DB::select($sql);

		return $data;
	}

	public static function deleteTierMenu($menu_id, $status){

		$chk = Tier::where('menu_id', '=', $menu_id)
			->where('status', '=', $status)
			->delete();

		$result = ($chk)? true : false;
		return $result;
	}

	public static function insertTierMenu($menu_id, $tiers, $status = 1){

		$insertData = []; //use for new tier

		foreach ($tiers as $tier) {

				$id = ($tier['id'] > 0 )? $tier['id'] : 0;

				$insertData[] = [
					'id'			=> $id,
					'price' 		=> $tier['price'],
					'qty'			=> $tier['qty'],
					'tierunit_id'	=> $tier['tierunit_id'],
					'menu_id' 		=> $menu_id,
					'status'		=> $status
				];
		}

		DB::table('tier')->insert($insertData);

	}



}