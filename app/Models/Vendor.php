<?php
namespace App\Models;

use DB;
use App\Models\VendorAuthication;
use App\Models\BankAccount;
use Illuminate\Database\Eloquent\Model;


final class Vendor extends Model
{
    protected $table = 'vendor';

    protected $fillable = array('');


    public static function checkExistingByEmail($email){
        $vendor  = Vendor::where('email', $email);
        $result = ($vendor)? true : false;
        return $result;
    }

    public static function getProfile($vendor_id, $auth_key){

        $vendor['profile'] = DB::table('vendor')
            ->where('vendor.id', '=', $vendor_id)
            ->join('gender', 'vendor.gender_id', '=', 'gender.id')
            ->select(
                'vendor.id',
                'vendor.facebook_id',
                'vendor.email',
                'vendor.name',
                'vendor.last_name',
                'vendor.phone',
                'gender.description as gender'
            )->first();

        $vendor['address'] = [];
        $vendor['auth_key']= sha1($auth_key);

        return $vendor;
    }

    public static function getVendor($vendor_id){
        $result = $vendor['profile'] = DB::table('vendor')
            ->where('vendor.id', '=', $vendor_id)
            ->join('gender', 'vendor.gender_id', '=', 'gender.id')
            ->select(
                'vendor.id',
                'vendor.facebook_id',
                'vendor.email',
                'vendor.name',
                'vendor.last_name',
                'vendor.address',
                'vendor.phone',
                'gender.description as gender'
            )->first();

        return $result;
    }

    public static function getAddress($vendor_id){

        $address = DB::select("SELECT
            user_location.id,
            user_location.latitude,
            user_location.longitude,
            address.house,
            address.floor,
            address.placename,
            address.soi,
            address.street,
            address.subdistrict,
            address.district,
            address.province,
            address.country,
            address.postcode,
            (SELECT name_en FROM place WHERE place.id = user_location.place_id) AS name_en,
            (SELECT name_th FROM place WHERE place.id = user_location.place_id) AS name_th
            FROM user_location
            INNER JOIN address ON user_location.address_id = address.id
            WHERE user_location.user_id = '$vendor_id'");

        return $address;
    }

    public static function checkDuplicateByEmail($email){
        $vendor = Vendor::where('email', $email)->get()->first();
        $result = (count($vendor)> 0)? true : false;
        return $result;
    }

    public static function resetPassword($email, $password){
        $chk = Vendor::where('email', '=', $email)->update(['password' => sha1($password)]);
        if($chk){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

//    public static function getVendor($vendor_id){
//        $vendor = DB::select("SELECT 	    vendor.id,
//											vendor.email,
//											vendor.facebook_id,
//											vendor.vstatus_id,
//											vstatus.description AS vendor_status,
//											vinfo.name,
//											vinfo.lastname,
//											vinfo.phone,
//											vinfo.address
//									FROM 	vendor, vinfo, vstatus
//									WHERE 	vendor.id = vinfo.id
//									AND 	vendor.id = $vendor_id
//									AND  	vendor.vstatus_id = vstatus.id
//								");
//        return $vendor;
//    }


}