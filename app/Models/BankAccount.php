<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


final class BankAccount extends Model
{
    protected $table = 'bank_account';

    protected $fillable = array('');


    public static function getBankAccount($vendor_id){

        $data = BankAccount::select(
                    'bank.id as bank_id',
                    'bank.name as bankname_en',
                    'bank.symbol',
                    'bank_account.account as acc_number',
                    'bank_account.name as acc_name'
                )
            ->where('bank_account.vendor_id', '=', $vendor_id)
            ->join('bank', 'bank_account.bank_id', '=', 'bank.id')
            ->first();

        return $data;

    }


}