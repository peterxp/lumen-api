<?php
namespace App\Models;

use DB;

use Illuminate\Database\Eloquent\Model;


final class Information extends Model
{
    protected $table = 'information';

    protected $fillable = [];

	public $timestamps = false;

	public static function getVendorInfo(){
		$result = [
			'categories' 	=> Information::getAllShopCategory(),
			'tier_units' 	=> Information::getAllTierUnit(),
			'places_type' 	=> Information::getAllPlace(),
			'distance_type'	=> Information::getAllDistanceType(),
			'banks'			=> Information::getAllBank(),
			'days'			=> Information::getAllDay()
		];
		return $result;
	}

	public static function getAllShopCategory(){
		return ShopCategory::all();
	}

	public static function getAllTierUnit(){
		return TierUnit::all();
	}

	public static function getAllPlace(){
		return Place::all();
	}

	public static function getAllDistanceType(){
		return DistanceType::all();
	}

	public static function getAllBank(){
		return Bank::all();
	}

	public static function getAllDay(){
		return Day::all();
	}


}