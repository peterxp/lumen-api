<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

final class ShopCategory extends Model
{
    protected $table = 'shop_category';

    protected $guarded = array('');

    public $timestamps = false;


}