<?php
namespace App\Models;

use App\Helpers\Calculator;
use App\Helpers\Curl;
use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;


use Illuminate\Database\Eloquent\Model;


final class Dashboard extends Model
{
    // protected $table = 'delivery_rule';
    // not have dashboard any more
    // protected $fillable = array('');

    public static function getAll($vendor_id, $shop_id, $report_type=''){

        $orderinfo 		= array();
        $forGraphdata 	= array();
        $topMenus		= array();
        $topCustomers  	= array();
        $total_sale = 0;
        $avg_value  = 0;
        $new_customer = 0;

        if ($report_type == 'day'){
            $forGraphdata 	= Dashboard::querySaleDataCurrentDay($shop_id);
            $dayResult = array();
            for ($i=0 ; $i<24; $i++) {
                $timex = sprintf("%02d:00",$i);
                $temp = array(
                    'purchase_date' => $timex,
                    'amount'		=> '0.1'
                );
                foreach ($forGraphdata as $hourx) {
                    if ($hourx->hour == $i){
                        $temp = array( 'purchase_date' 	=> $timex,
                            'amount'		=> $hourx->total
                        );
                    }
                }
                array_push($dayResult, $temp);
            }

            $forGraphdata = $dayResult;

            $orderinfo 		= Dashboard::querySaleDataForDayInfo($shop_id);
            foreach ($orderinfo as $order) {
                $total_sale = $total_sale + $order->grand_total;
            }
            if (count($orderinfo) > 0){
                $avg_value = (float)$total_sale / count($orderinfo);
                $topMenus = Dashboard::findTopMenu($orderinfo);
            }
            $topCustomers = Dashboard::findTopUsersToday($shop_id);
            $new_customer = Dashboard::findNewCustomerToday($shop_id);
        }

        elseif ($report_type == 'week') {
            $forGraphdata 	= Dashboard::querySaleDataWithRange($shop_id, 7);

            ### duplicate date 28-02-2016 (2 times)
            $dayResult = array();
            foreach ($forGraphdata as $key) {
                if ($key->purchase_date == "2016-02-29"){

                    $temp = array(
                        'purchase_date' => "2016-02-28" ,
                        'amount'		=> $key->amount
                    );

                }else{

                    $temp = array(
                        'purchase_date' => $key->purchase_date ,
                        'amount'		=> $key->amount
                    );
                }
                array_push($dayResult, $temp);
            }
            $forGraphdata = $dayResult;
            ###

            $orderinfo 		= Dashboard::querySaleDataAllInfo($shop_id, 7);
            foreach ($orderinfo as $order) {
                $total_sale = $total_sale + $order->grand_total;
            }
            if (count($orderinfo) > 0){
                $avg_value = (float)$total_sale / count($orderinfo);
                $topMenus = Dashboard::findTopMenu($orderinfo);
            }

            $topCustomers = Dashboard::findTopUsers($shop_id, 7);
            $new_customer = Dashboard::findNewCustomer($shop_id, 7);
        }

        elseif ($report_type == 'month') {
            $forGraphdata 	= Dashboard::querySaleDataWithRange($shop_id, 30);

            ### duplicate date 28-02-2016 (2 times)
            $dayResult = array();
            foreach ($forGraphdata as $key) {
                if ($key->purchase_date == "2016-02-29"){

                    $temp = array(
                        'purchase_date' => "2016-02-28" ,
                        'amount'		=> $key->amount
                    );

                }else{

                    $temp = array(
                        'purchase_date' => $key->purchase_date ,
                        'amount'		=> $key->amount
                    );
                }
                array_push($dayResult, $temp);
            }
            $forGraphdata = $dayResult;
            ###

            $orderinfo 		= Dashboard::querySaleDataAllInfo($shop_id, 30);
            foreach ($orderinfo as $order) {
                $total_sale = $total_sale + $order->grand_total;
            }
            if (count($orderinfo) > 0){
                $avg_value = (float)$total_sale / count($orderinfo);
                $topMenus = Dashboard::findTopMenu($orderinfo);
            }
            // $avg_value = (float)$total_sale / count($orderinfo);
            // $topMenus = $this->findTopMenu($orderinfo);
            $topCustomers = Dashboard::findTopUsers($shop_id, 30);
            $new_customer = Dashboard::findNewCustomer($shop_id, 30);
        }

        #total lifetime sales
        $lifetime_sales = number_format(Dashboard::lifetime_sales($shop_id), 2);

        #for show on dashboard
        $net_sales_from = Order::getFirstOrderDateByShop($shop_id);

        $current_date = date("Y-m-d");
        $net_sales_to = date("Y-m-d",strtotime("-8 days", strtotime($current_date)));

        if($net_sales_from == null || $net_sales_from == 0){
            // $net_sales_from = $net_sales_to;
            $net_sales_from = date("Y-m-d",strtotime("-17 days", strtotime($current_date)));
        }

        $balance_tab = Dashboard::queryTAB($shop_id);
        $balance_bat_total = Dashboard::queryBAT($shop_id);

        //Calculate and subtract fee,vat
        $balance_bat_tmp = Calculator::omise_percent($balance_bat_total);

        //Remove Comma
        $balance_bat = $balance_bat_tmp['balance_bat'];

        $balance_bat_show = number_format($balance_bat_tmp['balance_bat'], 2, '.', ',');

        # temporaly solution
        if($balance_bat < 1){
            $net_sales_from = "";
            $net_sales_to = "";
        }

        $transfer_history = Dashboard::findTransferHistory($shop_id);

        $returnInfo = array(
            'data_for_graph' 	=> $forGraphdata,
            'top_menus'			=> $topMenus,
            'top_customers'		=> $topCustomers,
            'new_customer'		=> $new_customer,
            'total_sale'		=> $total_sale,
            'total_sale_show'	=> number_format($total_sale,2),
            'avg_value'			=> $avg_value,
            'avg_value_show'	=> number_format($avg_value,2),
            'balance_tab'		=> $balance_tab,
            'balance_bat'		=> $balance_bat,
            'balance_tab_show'	=> number_format($balance_tab,2),
            'balance_bat_show'	=> $balance_bat_show,
            'net_sales'			=> ['from' => $net_sales_from,'to' => $net_sales_to],
            'history'			=> $transfer_history,
            'lifetime_sales'	=> $lifetime_sales
        );
        return $returnInfo;
    }

    public static function querySaleDataAllInfo($shop_id, $range){

        $current_date   = date('Y-m-d H:i:s');
        $range_date     = date("Y-m-d 23:59:59",strtotime("-".$range." days",strtotime($current_date)));

        $sql = "SELECT 	orders.*
									FROM 	orders
									WHERE 	orders.shop_id = $shop_id
									AND 	(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
									AND 	orders.created_at > '$range_date'
								";
        $data = DB::select($sql);
        return $data;
    }

    public static function querySaleDataForDayInfo($shop_id){
        $start_date = date('Y-m-d 00:00:00');
        $end_date 	= date('Y-m-d 23:59:59');
        $sql = "SELECT 	orders.*
									FROM 	orders
									WHERE 	orders.shop_id = $shop_id
									AND 	(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
									AND 	orders.created_at BETWEEN '$start_date' AND '$end_date'
								";
        $data = DB::select($sql);
        return $data;
    }

    public static function querySaleDataCurrentDay($shop_id){

        $start_date = date('Y-m-d 00:00:00');
        $end_date 	= date('Y-m-d 23:59:59');
        $sql = "SELECT 	HOUR(orders.created_at) AS hour,
                    sum(orders.grand_total) AS total
                    FROM 	orders
                    WHERE 	orders.shop_id = $shop_id
                    AND 	(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                    AND 	orders.created_at BETWEEN '$start_date' AND '$end_date'
                    GROUP BY HOUR(orders.created_at) ";

        $data = DB::select($sql);
        return $data;
    }

    public static function querySaleDataWithRange($shop_id, $range){

        $current_date   = date('Y-m-d H:i:s');
        $range_date     = date("Y-m-d 23:59:59", strtotime("-".$range." days",strtotime($current_date)));

        $sql = " SELECT
				t1.purchase_date,
				coalesce(SUM(t1.amount+t2.amount), 0.1) AS amount
				from
				(
				  select DATE_FORMAT(a.Date,'%Y-%m-%d') as purchase_date,
				  '0' as  amount
				  from (
				    select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
				    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
				    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
				    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
				  ) a
				  where a.Date > '$range_date'
				)t1
				left join
				(
				  SELECT DATE_FORMAT(orders.created_at, '%Y-%m-%d') as purchase_date,
				  coalesce(SUM(orders.grand_total), 0) AS amount
				  FROM orders
				  WHERE orders.created_at > '$range_date'
				  AND (orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
				  AND orders.shop_id = $shop_id
				  GROUP BY orders.created_at
				)t2
				on t2.purchase_date = t1.purchase_date
				group by t1.purchase_date
				order by t1.purchase_date asc
		";

        $data = DB::select($sql);
        return $data;
    }


    public static function findTopMenu($orderInfo){
        $topArray = array();
        $tmpMenuIDs = array();
        $resultArray = array();
        foreach ($orderInfo as $order) {
            $basket_log = json_decode($order->basket_log);
            foreach ($basket_log->baskets as $basket) {
                array_push($tmpMenuIDs, $basket->menu_id);
                $tmp['menu_id'] 	= $basket->menu_id;
                $tmp['count']		= 0;
                $tmp['menu_name']	= $basket->menuinfo->name;
                $tmp['menu_cover']	= $basket->menuinfo->cover;
                $tmp['menu_price']	= $basket->menuinfo->tier->price;
                $tmp['menu_price_show'] = number_format($basket->menuinfo->tier->price);
                if (!in_array($tmp, $topArray)){
                    array_push($topArray, $tmp);
                }
            }
        }
        asort($topArray);
        foreach ($topArray as $top) {
            foreach ($tmpMenuIDs as $menu_id) {
                if ($top['menu_id'] == $menu_id){
                    $top['count'] = $top['count'] + 1;
                }
            }
            array_push($resultArray, $top);
        }

        return $resultArray;
}

    public static function findTopUsers($shop_id, $range){

        $current_date   = date('Y-m-d H:i:s');
        $range_date     = date("Y-m-d 23:59:59", strtotime("-".$range." days", strtotime($current_date)));

        $sql = "SELECT 	orders.user_id,
                    COUNT(orders.user_id) AS user_count,
                    SUM(orders.grand_total) AS amount,
                    uinfo.name,
                    user.email,
                    uinfo.lastname,
                    uinfo.imageurl
                FROM  orders
                INNER JOIN user ON (orders.user_id = user.id)
                INNER JOIN uinfo ON (user.id = uinfo.id)
                WHERE 	orders.shop_id = $shop_id
                AND 	(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                AND 	orders.created_at > '$range_date'
                AND		orders.user_id = uinfo.id
                GROUP BY orders.user_id
                ORDER BY amount DESC
								";
        $data = DB::select($sql);
        return $data;
    }


    public static function findCustomerAllExceptRange($shop_id, $range){

        $current_date = date('Y-m-d H:i:s');
        $range_date = date("Y-m-d 23:59:59", strtotime("-".$range." days", strtotime($current_date)));

        $sql = "SELECT orders.user_id
                    FROM 		orders
                    WHERE 		orders.shop_id = $shop_id
                    AND 		(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                    AND 		orders.created_at > '$current_date'
                    GROUP BY 	orders.user_id ";
        $data = DB::select($sql);
        return $data;
    }

    public static function findNewCustomer($shop_id, $range){

        $current_date   = date('Y-m-d H:i:s');
        $range_date     = date("Y-m-d 23:59:59",strtotime("-".$range." days",strtotime($current_date)));

        $all_customer   = Dashboard::findCustomerAllExceptRange($shop_id, $range);

        $sql = "SELECT 	orders.user_id
                        FROM 		orders
                        WHERE 		orders.shop_id = $shop_id
                        AND 		(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                        AND 		orders.created_at > '$range_date'
                        GROUP BY 	orders.user_id";

        $range_customer = DB::select($sql);

        $newCus = array();

        foreach ($range_customer as $r_cus) {
            if (!in_array($r_cus, $all_customer)){
                array_push($newCus, $r_cus);
            }
        }
        return count($newCus);

    }


    public static function findNewCustomerToday($shop_id){
        $start_date = date('Y-m-d 00:00:00');
        $end_date 	= date('Y-m-d 23:59:59');
        $all_customer = Dashboard::findCustomerAllExceptRange($shop_id, 0);

        $sql = "SELECT 	orders.user_id
                    FROM 		orders
                    WHERE 		orders.shop_id = $shop_id
                    AND 		(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                    AND 		orders.created_at BETWEEN '$start_date' AND '$end_date'
                    GROUP BY 	orders.user_id
                ";
        $range_customer = DB::select($sql);

        $newCus = array();

        foreach ($range_customer as $r_cus) {
            if (!in_array($r_cus, $all_customer)){
                array_push($newCus, $r_cus);
            }
        }
        return count($newCus);

    }

    public static function findTopUsersToday($shop_id){
        $start_date = date('Y-m-d 00:00:00');
        $end_date 	= date('Y-m-d 23:59:59');

        $sql = " SELECT 	orders.user_id,
                            COUNT(orders.user_id) AS user_count,
                            SUM(orders.grand_total) AS amount,
                            uinfo.name,
                            user.email,
                            uinfo.lastname,
                            uinfo.imageurl
                    FROM  orders
                    INNER JOIN user ON (orders.user_id = user.id)
                    INNER JOIN uinfo ON (user.id = uinfo.id)
                    WHERE 	orders.shop_id = $shop_id
                    AND 	(orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
                    AND 	orders.created_at BETWEEN '$start_date' AND '$end_date'
                    AND		orders.user_id = uinfo.id
                    GROUP BY orders.user_id
                    ORDER BY amount DESC
                    ";
        $data = DB::select($sql);
        return $data;
    }

    # Total acount balance
    # is_transfer = 1 is not transfer
    # is_transfer = 2 is transferred
    public static function queryTAB($shop_id){

        $sql = " SELECT  COALESCE(sum(orders.grand_total),0) as tab
									FROM    orders
									WHERE   orders.shop_id = $shop_id
									AND     (orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
									AND     orders.is_transfer = 1 ";

        $sum  = DB::select($sql);
        return (int)$sum['0']->tab;
    }

    # Total acount balance
    # is_transfer = 1 is not transfer
    # is_transfer = 2 is transferred
    public static function queryBAT($shop_id){

        #for calculate
        $current_date   = date('Y-m-d H:i:s');
        $back_date      = date("Y-m-d 23:59:59",strtotime("-8 days",strtotime($current_date)));

        $sql = "SELECT  COALESCE(SUM(orders.grand_total),0) as bat
									FROM    orders
									WHERE   orders.shop_id = $shop_id
									AND 	orders.created_at <= '$back_date'
									AND     (orders.status_for_vendor = 2 OR orders.status_for_vendor = 4)
									AND     orders.is_transfer = 1 ";

        $sum  = DB::select($sql);
        return (int)$sum['0']->bat;
    }


    public static function findTransferHistory($shop_id){

        $sql = " SELECT  r.id, r.tab_value, r.bat_value, r.start_date, r.end_date,
                    r.remark, r.status, s.status_info, SUBSTRING(r.created_at, 1, 10) as created_at
                    FROM    request_transfer AS r
                    INNER JOIN request_transfer_status AS s
                    ON (r.status  = s.id)
                    WHERE   r.shop_id = $shop_id";

        $row  = DB::select($sql);
        $data = (count($row) > 0)? $row : [];
        return $data;
    }


    public static function checkExistRequestTransfer($shop_id, $tab_value, $bat_value){
        $sql = " SELECT  *
					FROM    request_transfer
					WHERE   request_transfer.shop_id 	= $shop_id
					AND     request_transfer.tab_value 	= $tab_value
					AND		request_transfer.bat_value 	= $bat_value
					AND 	request_transfer.status = 1
				";

        $sum  = DB::select($sql);
        $chk = (count($sum > 0))? true : false;
        return $chk;

    }

    public static function lifetime_sales($shop_id){
        
        $sql = "SELECT  coalesce(SUM(orders.grand_total), 0) as sum FROM orders WHERE shop_id = $shop_id";
        $result  = DB::select($sql);
        $sum = $result['0']->sum;
        return (int)$sum;
    }

    public static function requestTransfer($vendor_id, $shop_id, $tab_value, $bat_value, $start_date, $end_date){

        //Calculate and subtract fee,vat
        $bat_value_from_database = Dashboard::queryBAT($shop_id);

        $bat_value_arr = Calculator::omise_percent($bat_value_from_database);

        $bat_value 	= $bat_value_arr['balance_bat'];
        $total 		= $bat_value_arr['total'];
        $omise_fee	= $bat_value_arr['omise_fee'];
        $vat 		= $bat_value_arr['vat'];
        $total_fee 	= $bat_value_arr['total_fee'];

        $requestTransferInfo = [
            'vendor_id'			=> $vendor_id,
            'shop_id' 			=> $shop_id,
            'tab_value' 		=> $tab_value,
            'bat_value' 		=> $bat_value,
            'omise_fee' 		=> $omise_fee,
            'vat' 				=> $vat,
            'total_fee' 		=> $total_fee,
            'total' 			=> $total,
            'start_date' 		=> $start_date,
            'end_date' 			=> $end_date,
            'remark'			=> '',
            'omise_log'			=> '',
            'status'			=> 1
        ];

        $rec = RequestTransfer::create($requestTransferInfo);
        $request_transfer_id = $rec->id;

        if ($request_transfer_id > 0) {

            #TODO enable when use
            #hookSlack and send mail
            //Curl::hookSlack("request_transfer", $request_transfer_id);

            return ["result"=>"Success"];
        }else{
            return ["result"=>"Fail"];
        }

    }



}