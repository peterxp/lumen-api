<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;
use App\Models\Shop;
//use App\Models\DeliveryRule;
use App\Models\MenuImage;
use App\Models\Tier;

use Illuminate\Database\Eloquent\Model;


final class BanWord extends Model
{
    protected $table = 'ban_word';

	protected $fillable = [''];

	public static function getAll(){
		$word = BanWord::all();
		$arr_word = [];
		foreach($word as $w){
			array_push($arr_word, $w->word);
		}
		return $arr_word;
	}


}