<?php
namespace App\Models;
use DB;
use App\Models\User;
use App\Models\UserAuthentication;

class Authentication {
    
    //Check user authenticate
    public static function auth_check_user($auth_key, $id){
        $user = UserAuthentication::where(array('user_id'=>$id, 'auth_key'=>$auth_key))->get()->first();
        $result = (count($user)> 0)? true : false;
        return $result;
    }

    function getEndpointKeyWithUserID($user_id){
        $query = $this->db->query("SELECT 	uauth.apns_key
									FROM 	uauth
									WHERE  	uauth.user_id = $user_id
								");
        return $query->result();
    }

    function delete_endPoint($endpoint){
        try {
            $result = $this ->sns_client->deleteEndpoint(array(
                'EndpointArn' => $endpoint
            ));
            return $result;
        } catch (Exception $e) {
            return $e;
        }
    }


}