<?php
namespace App\Models;

use Aws\S3\S3Client;

final class UploadFile {

    public $S3Client;
    public $bucket;

    function __construct(){

        $this->S3Client = S3Client::factory([
            'version' => env('AWS_VERSION'),
            'region'  => env('AWS_REGION'),
            'debug'   => env('AWS_DEBUG'),
            'credentials' => [
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET')
            ]
        ]);

        #TODO Change bucket name when upload to production

        if( (array_shift((explode(".", $_SERVER['HTTP_HOST'])))) == "api-naja"){
            #TODO change to production bucket when implement to production
            $this->bucket = "xyz";
        } else {
            $this->bucket = "abc";
        }

    }

    public function upload_image($file_name, $target_path){
        try{
            $upload = $this->S3Client->putObject(array(
                'Bucket'        => $this->bucket,
                'Key'           => $target_path,
                'ContentType'   => 'image/jpeg',
                'Body'          => fopen($file_name,'r+'),
                'ACL'           => 'public-read'
            ));
            $result = $upload->get('ObjectURL');

        } catch(Exception $e) {
            $result = $e;
        }
        return $result;
    }

    /*
     * Delete folder
     * Example delete myFolder
     * images/vendors/vendor_id/shops/shop_id/products_draft/myFolder
     */
    function AwsS3DeleteFolder($dir){

        $iterator = $this->S3Client->getIterator('ListObjects', array(
            'Bucket' => $this->bucket,
            'Prefix' => $dir
        ));

        foreach ($iterator as $object) {
            $this->S3Client->deleteObject(array(
                'Bucket' => $this->bucket,
                'Key' => $object['Key']
            ));
        }
        return "true";
    }

    /* Delete single image */
    public function AwsS3DeleteImage($full_path){

        try {
            $S3 = $this->S3Client->deleteObject(array(
                'Bucket'=> $this->bucket,
                'Key'   => $full_path
            ));
            $result = $S3->count();

        } catch(Exception $e) {
            $result = $e;
        }
        return $result;

    }



}