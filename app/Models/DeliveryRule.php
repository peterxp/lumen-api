<?php
namespace App\Models;

use DB;
use App\Models\Authentication;
use App\Models\UserAuthication;


use Illuminate\Database\Eloquent\Model;


final class DeliveryRule extends Model
{
    protected $table = 'delivery_rule';
    protected $fillable = array('');



    public static function getDeliveryRule($shop_id){

        $in_type = DB::select(" SELECT delivery_rule.id,
                                distance_type.start,
                                distance_type.end,
                                distance_type.description,
                                delivery_rule.dayspend,
                                delivery_rule.timespend,
                                delivery_rule.price,
                                delivery_rule.is_flat
                                FROM delivery_rule
                                INNER JOIN distance_type ON (distance_type.id = delivery_rule.typeid)
                                WHERE (delivery_rule.shop_id = $shop_id)
                                        AND (delivery_rule.typeinout = 1)
                                        AND (delivery_rule.is_flat = 0)
                                ORDER BY distance_type.id ASC ");


        $in_type_flat = DB::select(" SELECT delivery_rule.id,
                                    place.id AS placeid,
                                    place.name_th,
                                    place.name_en,
                                    delivery_rule.dayspend,
                                    delivery_rule.timespend,
                                    delivery_rule.price,
                                    delivery_rule.is_flat
                                  FROM 	delivery_rule
                                  INNER JOIN place ON (place.id = delivery_rule.typeid)
                                  WHERE delivery_rule.shop_id = $shop_id
                                  AND delivery_rule.typeinout = 2
                                  AND delivery_rule.is_flat = 1
                                  ORDER BY place.name_en ASC ");


        $out_type = DB::select(" SELECT delivery_rule.id,
                                    place.id AS placeid,
                                    place.name_th,
                                    place.name_en,
                                    delivery_rule.dayspend,
                                    delivery_rule.timespend,
                                    delivery_rule.price,
                                    delivery_rule.is_flat
                              FROM 	delivery_rule
                              INNER JOIN place ON (place.id = delivery_rule.typeid)
                              WHERE 	delivery_rule.shop_id = $shop_id
                              AND delivery_rule.typeinout = 2
                              AND delivery_rule.is_flat = 0
                              ORDER BY place.name_en ASC ");

        $delivery = array(
            "in_type" 	    => $in_type,
            "in_type_flat"  => $in_type_flat,
            "out_type" 	    => $out_type
        );
        return $delivery;
    }

    //Improve sql performance by insert batch records
    public static function createBatch($arr, $shop_id){

        /* example array */
//        $json_string = array(
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 1,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 0
//            ),
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 2,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 0
//            ),
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 3,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 1
//            )
//        );
//
//        echo json_encode($json_string);
//        exit();

        DeliveryRule::deleteByShopID($shop_id);

        $rule_all = $arr;

        $rule_all = [];
        foreach($arr as $rule){
            $rule_data['shop_id']       = $rule['shop_id'];
            $rule_data['dayspend']      = $rule['dayspend'];
            $rule_data['timespend']     = $rule['timespend'];
            $rule_data['price']         = $rule['price'];
            $rule_data['typeinout']     = $rule['typeinout'];
            $rule_data['typeid']        = $rule['typeid'];
            $rule_data['is_flat']       = $rule['is_flat'];

            //$rule_data['close_time'] = (string)($rule['close_time'] == "00:00:00")? "23:59:59":$rule['close_time'];

            $rule_all[] = $rule_data;
        }

        //Insert batch here (one time with multiple record)
        if(DB::table('delivery_rule')->insert($rule_all)){
            return "deliveryRules_updated";
        }else{
            return "Not complete update shop open rule";
        }
    }

    public static function deleteByShopID($shop_id){
        return DeliveryRule::where('shop_id', '=', $shop_id)
            ->delete();
    }

}