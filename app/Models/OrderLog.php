<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;

final class OrderLog extends Model
{
    protected $table = 'order_log';

    public $timestamps = false;

    protected $guarded = array();

    public static function orderMonitor(){

        #static function can not call outside variable
        #need to set factor inside function
        $TIME_FACTOR = 0.05;

        $sql = "SELECT 	order_log.*,
                        UNIX_TIMESTAMP(order_log.updated_at) as ts
                        FROM order_log
                        WHERE order_log.status = 0";

        $query = DB::select($sql);

        foreach ($query as $o) {
            $current = new DateTime();
            $datetime1 = new DateTime($o->alarm_date);
            $o->time_diffence = $datetime1->diff(new DateTime());
            $o->diff = $current->getTimestamp() - $datetime1->getTimestamp();

            $o->current = $current->getTimestamp();
            $shop_id = $o->shop_id;
            $vendor_id = $o->vendor_id;
            $user_id = $o->user_id;
            $o->shop_id;

            $rule = OrderLog::queryDeliverRule($o->shop_id);



            if ($o->diff > $rule->stamp * $TIME_FACTOR) {
                $o->resend = 'YES';

                //$vendor_aws_tokens = $this->Vendor_model->queryAWSKey($vendor_id);
                $vendor_aws_tokens =VendorAuthentication::getAWSKey($vendor_id);

                //$count_order = $this->getCountNewOrder($shop_id);
                $count_new_order = OrderLog::getCountNewOrder($shop_id);

                //$count_message = $this->Chat_model->countMessage($shop_id, 0, 'shop');
                $count_message = Chat::countMessage($shop_id, 0, 'shop');
                $sum = (int)$count_new_order + (int)$count_message;

                foreach ($vendor_aws_tokens as $vendor) {
                    $user = User::getUser($user_id);
                    $user_name = $user->name;
                    //$this->Pushnotification->sendPushOrder($vendor->apns_key,'Please check your order from '.$user_name,1,'Order.mp3' ,'user_id='.$user_id, $sum);
                }
                OrderLog::updateAlertUpdate($o);
            }
        }

        return $query;
    }

    public static function updateAlertUpdate($order_log){
        $update = date('Y-m-d H:i:s');
        $repeat_count = $order_log->repeat_count;
        $repeat_count = (int)$repeat_count + 1;
        $order_log_id = $order_log->id;
        $order_id     = $order_log->order_id;

        OrderLog::where('id', $order_log_id)
            ->update([
                'alarm_date' => $update,
                'repeat_count' => $repeat_count
            ]);

        #hook to slack via CURL
        if($repeat_count > 3 ){
            Curl::hookSlack("reminder_order", $order_id);
        }

    }

    public static function getCountNewOrder($shop_id){
        $result = OrderLog::where('shop_id', '=', $shop_id)
            ->where('status', '=', 0)
            ->count();

        return $result;
    }


    public static function queryDeliverRule($shop_id){

        $delivery_rules = DeliveryRule::where('shop_id', '=', $shop_id)->skip(0)->take(1)->get();

        $rule = $delivery_rules[0];
        $parsed = date_parse($rule->timespend);

        $seconds = ($parsed['day'] * 24 * 3600) + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
        $rule->stamp = $seconds;
        return $rule;

    }


}