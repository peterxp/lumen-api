<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

#Welcome Route
$app->get('/', function () use ($app) {
    $result = ['api'=>'V2', 'description'=>'Happy Coding'];
    return $result;
});


#API Route
$app->group(['prefix'=>'v2','namespace' => 'App\Http\Controllers\API'], function($app)
{
    #user
    $app->post('user/skip', 'UserSkipController@skip');
    $app->post('user/signUp', 'UserController@signUp');
    $app->post('user/update', 'UserController@update');
    $app->post('user/resetPassword','UserController@resetPassword');
    $app->get('user/renewPassword/{token}', ['uses'=>'UserController@renewPassword']);
    $app->post('user/updatePassword', 'UserController@updatePassword');
    $app->post('user/login', 'UserController@login');
    $app->post('user/facebookLogin','UserController@facebookLogin');
    $app->post('user/deleteSkip','UserController@deleteSkip');

    #shop
    $app->post('shop/getByUser', 'ShopController@getByUser');
    $app->post('shop/getByVendor', 'ShopController@getByVendor');
    $app->post('shop/updateShop', 'ShopController@updateShop');
    $app->post('shop/OpenOrClose', 'ShopController@OpenOrClose');
    $app->post('shop/createOpenRule', 'ShopController@createOpenRule');
    $app->post('shop/createDeliveryRule', 'ShopController@createDeliveryRule');
    $app->post('shop/updateLocation', 'ShopController@updateLocation');
    $app->post('shop/updateBankAccount', 'ShopController@updateBankAccount');
    $app->post('shop/RequestOpenShop', 'ShopController@RequestOpenShop');

    #menu
    $app->post('menu/getMenu', 'MenuController@getMenu');
    $app->post('menu/stockControl', 'MenuController@stockControl');
    $app->post('menu/createMenu', 'MenuController@createMenu');
    $app->post('menu/updateMenu', 'MenuController@createMenu');
    $app->post('menu/createDraftMenu', 'MenuController@createDraftMenu');
    $app->post('menu/updateDraftMenu', 'MenuController@createDraftMenu');
    $app->post('menu/delete', 'MenuController@delete');
    $app->post('menu/vendorGetMenu', 'MenuController@vendorGetMenu');

    #basket
    $app->post('basket/add', 'BasketController@add');
    $app->post('basket/countMenuByUser', 'BasketController@countMenuByUser');
    $app->post('basket/userDeleteBasket', 'BasketController@userDeleteBasket');
    $app->post('basket/getBasket', 'BasketController@getBasket');
    $app->post('basket/leaveNote', 'BasketController@leaveNote');

    #order
    $app->post('order/create', 'OrderController@create');
    $app->post('order/complete', 'OrderController@complete');
    $app->post('order/updateStatus', 'OrderController@updateStatus');
    $app->post('order/userGetOrder', 'OrderController@userGetOrder');
    $app->post('order/vendorGetOrder', 'OrderController@vendorGetOrder');
    $app->get('order/orderMonitor/{who}', 'OrderController@orderMonitor');

    #vendor
    $app->post('vendor/signUp', 'VendorController@signUp');
    $app->post('vendor/login', 'VendorController@login');
    $app->post('vendor/facebookLogin', 'VendorController@facebookLogin');
    $app->post('vendor/resetPassword','VendorController@resetPassword');
    $app->get('vendor/renewPassword/{token}', 'VendorController@renewPassword');
    $app->post('vendor/updatePassword', 'VendorController@updatePassword');

    #dashboard
    $app->post('dashboard/main', 'DashboardController@main');
    $app->post('dashboard/requestTransfer', 'DashboardController@requestTransfer');

    #information
    $app->post('information/getVendorInfo', 'InformationController@getVendorInfo');
    $app->post('information/getShopCategory', 'InformationController@getShopCategory');
    $app->post('information/getVendorBadge', 'ShopController@getVendorBadge');

    #chat
    $app->post('chat/createChat', 'ChatController@createChat');
    $app->post('chat/getMessage', 'ChatController@getMessage');
    $app->post('chat/getChatShop', 'ChatController@getChatShop');
    $app->post('chat/getChatUser', 'ChatController@getChatUser');
    $app->post('chat/userSendChat', 'ChatController@userSendChat');
    $app->post('chat/vendorSendChat', 'ChatController@vendorSendChat');
    $app->post('chat/deleteChat', 'ChatController@deleteChat');
    $app->post('chat/createChat', 'ChatController@createChat');

    $app->get('ban/all', 'BanWordController@all');

    #Promotion
    $app->post('promotion/validatePromotionCode', 'PromotionController@validatePromotionCode');
    $app->post('promotion/updateVIP', 'PromotionController@updateVIP');

    #Test
    $app->post('test/date', 'TestController@date');
    $app->post('test/createEndpoint', 'TestController@createEndpoint');
    $app->post('test/sendPush', 'TestController@sendPush');
    $app->post('test/upload', 'TestController@upload');
    $app->post('test/send_mail', 'TestController@send_mail');
    $app->get('test/checkExisting', 'TestController@checkExisting');
    $app->post('test/getTierByMenu', 'TestController@getTierByMenu');
    $app->get('test/carbon', 'TestController@carbon');

    #aws s3
    //$app->post('tests/deleteFolderFromAwsS3', 'TestController@deleteFolderFromAwsS3');
    $app->post('test/deleteImageFromAwsS3', 'TestController@deleteImageFromAwsS3');


    //================================


});

#Administrator API Route
$app->group(['prefix'=>'admin','namespace' => 'App\Http\Controllers\Admin'], function($app)
{
    $app->get('shopCategory/index', 'ShopCategoryController@index');
    $app->get('test/index', 'TestController@index');
    $app->get('test/carbon', 'TestController@carbon');
    $app->get('test/userExist', 'TestController@userExist');
    $app->get('test/rollbar', 'TestController@rollbar');

    $app->post('test/hook', 'TestController@hook');
    $app->get('test/banWord', 'TestController@banWord');
    $app->get('test/pagination', 'TestController@pagination');
    $app->get('test/getPromotion', 'TestController@getPromotion');
    $app->get('omise', 'TestController@omise');
    $app->get('omise/transfer', 'TestController@transfer');

});




