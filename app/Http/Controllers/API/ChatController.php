<?php
namespace App\Http\Controllers\API;

use App\Models\Chat;
use App\Models\VendorAuthentication;
use DB;
use App\Models\Authentication;
use App\Models\UserAuthentication;
use App\Models\PushNotification;
use App\Models\Message;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class ChatController extends Controller
{

    function createChat(Request $request){

        $auth_key       = $request->input('auth_key');
        $shop_id        = $request->input('shop_id');
        $user_id        = $request->input('user_id');
        $owner_type     = $request->input('owner_type');
        if ($shop_id && $user_id) {
            $info = Chat::createChat($shop_id, $user_id, $owner_type);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function getMessage(Request $request){

        $auth_key       = $request->input('auth_key');
        $shop_id        = $request->input('shop_id');
        $user_id        = $request->input('user_id');
        $chat_id        = $request->input('chat_id');
        $owner_type     = $request->input('owner_type');
        if ($shop_id && $user_id) {
            $info = Chat::getMessage($shop_id, $user_id, $chat_id, $owner_type);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function getChatShop(Request $request){

        $auth_key       = $request->input('auth_key');
        $shop_id        = $request->input('shop_id');
        if ($shop_id) {
            $info = Chat::queryLastMessageForShop($shop_id);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function getChatUser(Request $request){

        $auth_key   = $request->input('auth_key');
        $user_id    = $request->input('user_id');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($user_id) {
            $info = Chat::queryLastMessageForUser($user_id);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function userSendChat(Request $request){

        $auth_key   = $request->input('auth_key');
        $user_id    = $request->input('user_id');
        $shop_id	= $request->input('shop_id');
        $message	= $request->input('message');
        $owner_type = $request->input('owner_type');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($auth_key && $user_id && $shop_id  && $owner_type) {
            $info = Chat::addChat($user_id, $shop_id, $message, $owner_type);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function vendorSendChat(Request $request){

        $auth_key   = $request->input('auth_key');
        $user_id    = $request->input('user_id');
        $shop_id    = $request->input('shop_id');
        $message    = $request->input('message');
        $owner_type = $request->input('owner_type');

        ### check user authenticate ###
        $chk_auth = VendorAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($auth_key && $user_id && $shop_id  && $owner_type) {
            $info = Chat::addChat($user_id,$shop_id,$message,$owner_type);
            return Message::response($info);
        }else{
            return Message::wrongParameter();
        }
    }

    function deleteChat(Request $request){

        $auth_key       = $request->input('auth_key');
        $shop_id        = $request->input('shop_id');
        $user_id        = $request->input('user_id');
        $owner_type     = $request->input('owner_type');
        $last_chat_id   = $request->input('last_chat_id');
        if ($shop_id && $user_id) {
            $info = Chat::deleteChat($shop_id, $user_id, $owner_type, $last_chat_id);
            $result = Message::response($info);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

}


