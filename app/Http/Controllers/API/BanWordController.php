<?php
namespace App\Http\Controllers\API;


use App\Models\BanWord;
use App\Models\Information;
use App\Models\Message;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class BanWordController extends Controller
{
    public function all(Request $request){
        $result = BanWord::getAll();
        return Message::response($result);
    }

}


