<?php
namespace App\Http\Controllers\API;

use App\Helpers\Curl;
use App\Models\Order;
use App\Models\OrderLog;
use App\Models\Shop;
use DB;
use App\Models\Basket;
use App\Models\LocationBasket;
use App\Models\Message;
use App\Models\User;
use App\Models\Address;
use App\Helpers\Calculator;
use App\Models\VendorAuthentication;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class OrderController extends Controller
{

    public function create(Request $request){

        //prepare variable
        $auth_key 	        = $request->input('auth_key');
        $baskets_id         = $request->input('baskets_id');
        $status_for_user  	= 2;
        $status_for_vendor	= 2;
        $delivery_charge    = $request->input('delivery_charge');
        $delivery_address   = $request->input('delivery_address');
        $customer_phone     = $request->input('customer_phone');
        $food_price         = $request->input('food_price');
        $total_price        = $request->input('total_price');
        $estimate_time      = $request->input('estimate_time');
        $is_use_pcode       = $request->input('is_use_pcode');
        $pcode_id           = $request->input('pcode_id');
        $user_id  	        = $request->input('user_id');
        $vendor_id          = $request->input('vendor_id');
        $shop_id            = $request->input('shop_id');
        $basket_log         = $request->input('basket_log');

        $omise_transection_log = $request->input('omise_transection_log');
        $omise_transection_object = json_decode($omise_transection_log);
        $omise_charge_id	= ($omise_transection_object->id)? $omise_transection_object->id : NULL;

        $basket_log_object  = json_decode($basket_log);
        $grand_total        = $basket_log_object->total;


        #create order
        if ($auth_key && $user_id && $shop_id && $vendor_id && $baskets_id && $is_use_pcode && $basket_log) {
            //create order
            $order = new Order();
            $order->baskets_id = $baskets_id;
            $order->status_for_user  	= 2;
            $order->status_for_vendor	= 2;
            $order->delivery_charge     = $delivery_charge;
            $order->delivery_address    = $delivery_address;
            $order->customer_phone      = $customer_phone;
            $order->food_price          = $food_price;
            $order->total_price         = $total_price;
            $order->estimate_time       = $estimate_time;
            $order->is_use_pcode        = $is_use_pcode;
            $order->pcode_id            = $pcode_id;
            $order->user_id  	        = $user_id;
            $order->vendor_id           = $vendor_id;
            $order->shop_id             = $shop_id;
            $order->basket_log          = $basket_log;
            $order->omise_transection_log = $omise_transection_log;
            $order->omise_charge_id	    = $omise_charge_id;
            $order->grand_total         = $grand_total;

            if ($order->save()) {
                #get order_id to send mail via CURL
                $order_id = $order->id;
                //Curl::curlSendMail($order_id);

                $basket_id_array = explode(",", $baskets_id);
                foreach ($basket_id_array as $basket_id) {
                    Basket::where('id', '=', $basket_id)
                        ->update(['status'=>2]);
                }

                #TODO push notification to User and Vendor
                /*
                $user_aws_tokens = $this->User_model->queryAWSKey($user_id);
                foreach ($user_aws_tokens as $user) {
                    $this->Pushnotification->sendPush($user->apns_key,'',0,'push_order','user_id='.$user_id,'Order.mp3');
                }

                $vendor_aws_tokens = $this->Vendor_model->queryAWSKey($vendor_id);
                foreach ($vendor_aws_tokens as $vendor) {
                    $this->Pushnotification->sendPush($vendor->apns_key,'You have new order',0,'push_order','user_id='.$user_id,'Order.mp3');
                }
                */

                $result = Message::response('Order add');
            }else{
                $result = Message::responseFalse("Can not create order");
            }


        }else{
            $result = Message::wrongParameter();
        }

        return $result;
    }

    public function userGetOrder(Request $request){
        $auth_key       = $request->input('auth_key');
        $user_id        = $request->input('user_id');

        if ($auth_key && $user_id) {
//            $data = Order::getOrderForUser($user_id);
//            Message::response($data);

            $orders = array();
            $order_list = Order::queryOrderUser($user_id);

            foreach($order_list as $order){
                $order->shop_info = Shop::where('id', '=', $order->shop_id)->first();
                array_push($orders, $order);
            }
            $result = Message::response($orders);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

    public function vendorGetOrder(Request $request){
        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');

        if (!empty($auth_key) && !empty($vendor_id)) {
            $res = Order::getOrderForVendor($vendor_id, $shop_id);
            $result = Message::response($res);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

    public function updateStatus(Request $request){
        $auth_key       = $request->input('auth_key');
        $order_id       = $request->input('order_id');
        $order_status   = $request->input('order_status');
        $user_id        = $request->input('user_id');
        if ($auth_key && $order_id && $order_status) {
            $res = Order::updateStatus($order_id,$order_status,$user_id);
            $result = Message::response($res);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

    public function complete(Request $request){

        $auth_key   = $request->input('auth_key');
        $shop_id    = $request->input('shop_id');
        $order_id   = $request->input('order_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($auth_key && $order_id) {
            $res    = Order::vendorCompleteOrder($shop_id, $order_id);
            $result = Message::response($res);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

    // 24 Feb 2016 add repeat order push
    public function orderMonitor($who){

        if ($who == 'qoo') {
            $info = OrderLog::orderMonitor();
            $result = Message::response($info);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

}


