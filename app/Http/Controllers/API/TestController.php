<?php
namespace App\Http\Controllers\API;

use App\Models\APNS;
use App\Models\MenuImage;
use DB;
use App\Models\User;
use App\Models\Authentication;
use App\Models\UserAuthentication;
use App\Models\UploadFile;
use App\Models\Message;
use App\Helpers\Calculator;
use App\Models\PushNotification;
use App\Helpers\Utility;
use App\Models\DeliveryRule;

use Illuminate\Http\Request;
use App\Helpers\Mail;
use Aws\S3\S3Client;
use App\Models\Menu;
use App\Models\Tier;
use Carbon\Carbon;


use Laravel\Lumen\Routing\Controller as BaseController;

class TestController extends Controller
{

    public function send_mail(Request $request){

        Mail::sendEmail(array('chai.pr@onner.com'=>'Supachai Pradabsri'), "Test Class Mail", "Test Naja");
        #ref https://github.com/PHPMailer/PHPMailer

//        $mail = new \PHPMailer(true); // notice the \  you have to use root namespace here
//        try {
//            $mail->isSMTP(); // tell to use smtp
//            $mail->CharSet = "utf-8"; // set charset to utf8
//            $mail->SMTPAuth = true;  // use smpt auth
//            $mail->SMTPSecure = "tls"; // or ssl
//            $mail->Host = "email-smtp.us-east-1.amazonaws.com";
//            $mail->Port = 587; // most likely something different for you. This is the mailtrap.io port i use for testing.
//            $mail->Username = "AKIAJHVAUCZVMJKCJYZQ";
//            $mail->Password = "AtPfw8OonYZEtIhSReUf5VwizQOeyNXSY1iIYZsGTs00";
//            $mail->setFrom("do-not-reply@onner.com", "ONNER Tech Co., Ltd.");
//            $mail->Subject = "Test";
//            $mail->MsgHTML("This is a test #2");
//            $mail->addAddress("chai.pr@onner.com", "Recipient Name Chai.pr");
//            $mail->send();
//        } catch (phpmailerException $e) {
//            dd($e);
//        } catch (Exception $e) {
//            dd($e);
//        }
//        die('success');

    }

    /**
     * @return array
     */
    public function date(Request $request){

        $shop_id = 364;
         $rule = DeliveryRule::where('shop_id', '=', $shop_id)->skip(0)->take(1)->get();
        return $rule[0];

//        $json = [
//            [
//                'qty'			=> 1,
//                'price' 		=> 100,
//                'tierunit_id'	=> 1
//            ],
//            [
//                'qty'			=> 3,
//                'price' 		=> 250,
//                'tierunit_id'	=> 1
//            ],
//            [
//                'qty'			=> 5,
//                'price' 		=> 400,
//                'tierunit_id'	=> 1
//            ],
//
//        ];
//
//        return json_encode($json);

//        return Utility::generateToken(4);
        //return date('Y-m-d H:i:s');
    }


    public function createEndpoint(Request $request){
        $ARN_TYPE       = $request->input('arn_type');
        $device_token   = $request->input('device_token');
        $custom_data    = $request->input('custom_dat');

        $push = new PushNotification();
        $result = $push->createSNSEndPoint($ARN_TYPE, $device_token, $custom_data);

        return $result;
    }

    public function sendPush(Request $request){

        $target     = $request->input("target");
        $message    = $request->input("message");
        $sound      = "Message.mp3";
        $badge      = 1;
        $action     = "push_order";
        $value      = "1-2-1";

        $push = new APNS();
        $result = $push->sendPush($target, $message, $badge, $action, $value, $sound);

        return $result;
    }

    public function upload(Request $request){

        $upload = new UploadFile();
        $file_name = $request->file("cover");
        $rep = $upload->upload_image($file_name, "0.jpg");

        return $rep;
    }


    public function cal($money){
        return Calculator::omise_percent($money);
    }

    public function uploadx(Request $request){
        if ($request->hasFile('cover')) {
            echo "Y";
            $cover_path = 'images/vendors/99/shops/99/products/111/cover.jpg';
            $cover_image = $request->file('cover');

            //$fileName = $request->file('cover')->getClientOriginalName();
            //$request->file('cover')->move("upload", $fileName);
            $result = UploadFile::upload($cover_image, $cover_path);
        }else{
            $result = "N";
        }
        return $result;
    }

    public function auth_check(){
        $auth_key = "88f235c69e89f3059127ea8543589023bb95425axxx";
        $owner = "vauth";
        $chk = Authentication::auth_check($auth_key, $owner);
        $chk_result = ($chk)? ture : false;
        $result = array("result"=>$chk_result);
        return $result;
    }

//    public function index(){
//        return User::all();
//    }
    public function getUser($id){
        return User::find($id);
    }
    public function updateUser(){

        // $Book  = Book::find($id);
        // $Book->title = $request->input('title');
        // $Book->author = $request->input('author');
        // $Book->isbn = $request->input('isbn');
        // $Book->save();

        // return response()->json($Book);
    }

    public function checkExisting(){
        return $backup_tier_ids = Tier::where('menu_id', '=', 477)
            ->where('status', '=', 1)
            ->select('id')->get();

//        print_r($backup_tier_ids);

//        $chk = Menu::checkExistMenu(477);
//        if($chk){
//            echo "Yes:".$chk;
//        }else{
//            echo "No:".$chk;
//        }

    }

    public function getTierByMenu(Request $request){
        $menu_id    = $request->input('menu_id');
        $status     = $request->input('status');

        return Tier::where('menu_id', '=', $menu_id)
                    ->where('status', '=', $status)
                    ->select('id', 'qty', 'price', 'tierunit_id')
                    ->get();

    }

    //delete folder
    public function deleteFolderFromAwsS3(Request $request){
        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');
        $menu_id        = $request->input('menu_id');
        $menu_draft_id  = $request->input('menu_draft_id');

        if($menu_id != '0'){
            //delete images
            echo "hiya it will delete image in folder products/xyz/*_all_image.jpg";
            //return MenuImage::deleteImageFromAwsS3($vendor_id, $shop_id, $menu_id);

        }else{
            //delete draft image
            echo "hiya it will delete image in folder products_draft/xyz/*_all_image.jpg";
            //return MenuImage::deleteImageDraftFromAwsS3($vendor_id, $shop_id, $menu_draft_id);
        }

    }

    //delete single image
    public function deleteImageFromAwsS3(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');
        $menu_id        = $request->input('menu_id');
        $menu_draft_id  = $request->input('menu_draft_id');

        $full_path = 'images/vendors/'.$vendor_id.'/shops/'.$shop_id.'/products_draft/'.$menu_draft_id."/4.jpg";
        $S3 = new UploadFile();
        return $S3->AwsS3DeleteImage($full_path);
    }

    public function carbon(Request $request){

        $now = Carbon::now();
        $yesterday = Carbon::yesterday();

        $arr = [
            "yesterday" => $yesterday->toDateTimeString(),
            "today" => $now->toDateTimeString(),
            "tomorrow" =>  $now->addDay()->toDateTimeString(),
            "human_read" => Carbon::now()->diffForHumans()

        ];

        return $arr;

    }

}


