<?php
namespace App\Http\Controllers\API;

use DB;
use App\Models\UserSkip;
use App\Models\Message;

use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class UserSkipController extends Controller
{
    public function skip(Request $request){

        //change sysinfo, device to system_info
        $system_info	= $request->input('system_info');
        $device_token 	= $request->input('device_token');
        $auth_key       = sha1($request->input('system_info'));

        if (UserSkip::where('auth_key', '=', $auth_key)->exists()) {
            //delete skip user
            UserSkip::where('auth_key', '=', $auth_key)->delete();
            #TODO SNS end point
            //$this->Authentication->delete_endPoint($this->getEndpointKey($auth_key)[0]->apns_key);
        }

        $re = "new";
        $user_skip = new UserSkip();
        $user_skip->system_info = $system_info;
        $user_skip->auth_key    = $auth_key;
        $user_skip->status      = 1;
        $user_skip->save();
        $id = $user_skip->id;

        //TODO create SNS end point
        $apns_key = "test-at-onner-naja";
        $user_data = "user_id,".$id;
        //$sns_endpoint = $this->getSNSEndPoint('customer', $device_token, $id_type.",".$id);

        //update sns_endpoint
        UserSkip::where('id', '=', $id)
            ->update(['apns_key'=> $apns_key]);

        $res = UserSkip::getUserSkipProfile($id);
        $result = Message::response($res);

        return $result;
    }


    public function delete_skip(Request $request){

        $auth_key   = $request->input('auth_key');
        $user_id    = $request->input('user_id');

        if (empty($auth_key) OR empty($user_id) ) {
            return Message::wrongParameter();
        }

        if(UserAuthentication::authenticate($auth_key, $user_id)){

            //Delete User by id & Delete uauth by user_id
            User::where('id', $user_id)->delete();
            UserAuthentication::where('user_id', $user_id)->delete();

            $result = Message::response("deleted");

        }else{
            $result = Message::responseFalse("Unable to delete");
        }

        return $result;
    }


}


