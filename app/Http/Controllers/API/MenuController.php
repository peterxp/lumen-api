<?php
namespace App\Http\Controllers\API;

use App\Models\Menu;
use App\Models\MenuDraft;
use App\Models\MenuImage;
use App\Models\Message;
use App\Models\VendorAuthentication;
use DB;


use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class MenuController extends Controller
{

    public function getMenu(Request $request){
        $shop_id  = $request->input('shop_id');
        if (!empty($shop_id)) {
            $menu = Menu::getByShop($shop_id);
            $data = Message::response($menu);
        }else{
            $data = Message::responseFalse("false");
        }
        return $data;
    }

    public function countMenuByUser(Request $request){
        $user_id = $request->input('user_id');
        return Menu::countMenuByUser($user_id);
    }

    public function stockControl(Request $request){

        //change control to status
        $auth_key   = $request->input('auth_key');
        $menu_id    = $request->input('menu_id');
        $status     = $request->input('status');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $chk = Menu::stockControl($menu_id, $status);
        if($chk){
            return Message::response("menucontrol");
        }else{
            return Message::responseFalse("fail");
        }
    }

    public function createMenu(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id		= $request->input('vendor_id');
        $shop_id		= $request->input('shop_id');
        $menu_id 		= $request->input('menu_id'); // if menu id 0 create new nemu and other update menu where menu id
        $menu_draft_id	= $request->input('menu_draft_id'); // if menu_draft_id is 0 do not something and other must delete menu_draft by menu_draft_id

        $name			= $request->input('name');
        $main_ingredient= $request->input('main_ingredient');
        $description	= $request->input('description');
        $tiers		    = (array)json_decode($request->input('tiers'), true);

        $cover  = ($request->hasFile("cover"))? $request->file("cover") : null;
        $image1 = ($request->hasFile("image1"))? $request->file("image1") : null;
        $image2 = ($request->hasFile("image2"))? $request->file("image2") : null;
        $image3 = ($request->hasFile("image3"))? $request->file("image3") : null;

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($menu_id == '0'){

            if($menu_draft_id !=0){
                MenuDraft::deleteDraftMenu($menu_draft_id);
            }
            $result = Menu::createMenu($vendor_id, $shop_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers);
            return $result;

        }else{

            if($menu_draft_id !=0){
                MenuDraft::deleteDraftMenu($menu_draft_id);
            }
            if(Menu::checkExistMenu($menu_id)){
                $result = Menu::updateMenu($vendor_id, $shop_id, $menu_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers);
                return Message::response($result);
            }else{
                $result = Menu::createMenu($vendor_id, $shop_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers, $menu_id);
                return Message::response($result);
            }

        }

    }

    public function createDraftMenu(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id		= $request->input('vendor_id');
        $shop_id		= $request->input('shop_id');
        $menu_id 		= $request->input('menu_id'); // if menu id 0 create new nemu and other update menu where menu id
        $menu_draft_id	= $request->input('menu_draft_id'); // if menu_draft_id is 0 do not something and other must delete menu_draft by menu_draft_id

        $name			= $request->input('name');
        $main_ingredient= $request->input('main_ingredient');
        $description	= $request->input('description');
        $tiers		    = (array)json_decode($request->input('tiers'), true);

        $cover  = ($request->hasFile("cover"))? $request->file("cover") : null;
        $image1 = ($request->hasFile("image1"))? $request->file("image1") : null;
        $image2 = ($request->hasFile("image2"))? $request->file("image2") : null;
        $image3 = ($request->hasFile("image3"))? $request->file("image3") : null;

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if($menu_draft_id == '0'){
            $result = MenuDraft::createDraft($vendor_id, $shop_id, $menu_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers);
            return $result;

        }else{
            $result = MenuDraft::updateDraft($vendor_id, $shop_id, $menu_id, $menu_draft_id, $cover, $image1, $image2, $image3, $name, $main_ingredient, $description, $tiers);
            return $result;
        }

    }

    public function delete(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');
        $menu_id        = $request->input('menu_id');
        $menu_draft_id  = $request->input('menu_draft_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if($menu_id != '0'){
            //delete menu
            //MenuImage::deleteFolderImageByMenu($vendor_id, $shop_id, $menu_id);
            return Menu::deleteMenu($vendor_id, $shop_id, $menu_id);

        }else{
            //delete draft menu
            MenuImage::deleteFolderImageByMenuDraft($vendor_id, $shop_id, $menu_draft_id);
            return MenuDraft::deleteMenu($vendor_id, $shop_id, $menu_draft_id);
        }

    }

    public function vendorGetMenu(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        if ($auth_key && $vendor_id) {

            $data = Menu::getShopMenu($vendor_id, $shop_id);
            $result = Message::response($data);

        }else {
            $result = Message::wrongParameter();
        }

        return $result;

    }



}


