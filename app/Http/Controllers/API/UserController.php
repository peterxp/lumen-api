<?php
namespace App\Http\Controllers\API;

use DB;
use App\Models\User;
use App\Models\UserForgotPassword;
use App\Models\UserSkip;
use App\Models\Authentication;
use App\Models\UserAuthentication;
use App\Models\PushNotification;
use App\Models\UploadFile;
use App\Models\Message;

use App\Helpers\Calculator;
use App\Helpers\Mail;
use App\Helpers\Utility;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends Controller
{

    /**
     * @param Request $request
     * @return array|static
     */
    public function signUp(Request $request){

        $email          = $request->input('email');
        $password       = $request->input('password');
        $re_password    = $request->input('re_password');
        $system_info    = $request->input('system_info');
        $device_token   = sha1($request->input('system_info'));

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return Message::wrongEmailFormat();
        }

        if(!Mail::is_real_email($email)){
            return Message::signUpEmailWrong();
        }

        if($password != $re_password){
            return Message::passwordNotMatch();
        }

        //Check duplicate user
        if(User::where('email', '=', $email)->exists()){
            return Message::checkDuplicateEmail();
        }

        if ( !empty($email) && !empty($password) && !empty($re_password) ) {

            if($password == $re_password){
                //create users
                $user = new User();
                $user->email = $email;
                $user->password = $password;
                $user->save();
                $user_id = $user->id;


                #TODO get sns_endpoint
                //create sns for customer or user
//                if ($user['device_token'] != 'not allow token id') {
//                    $sns_endpoint = PushNotification::getSNSEndPointForCustomer($device_token, $type . "," . $id);
//                }else{
//                    $sns_endpoint = "user_not_allow_notification";
//                }

                $sns_endpoint = "user_not_allow_notification";

                $auth = new UserAuthentication();
                $auth->user_id      = $user_id;
                $auth->status       = 1;
                $auth->system_info  = $system_info;
                $auth->apns_key     = $sns_endpoint;
                $auth->save();

                $data = User::getProfile($user_id, $request->input('device'));
                $data['auth_key'] = sha1($request->input('device'));

                return Message::response($data);
            }

        }else{
            return Message::wrongParameter();
        }
    }

    public function update(Request $request){

        $user_id    = $request->input('user_id');
        $auth_key   = $request->input('auth_key');

        //check user authenticate
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $user = array(
            'name'	    => $request->input('name'),
            'last_name'	=> $request->input('last_name'),
            'phone'	    => $request->input('phone')
        );

        if($request->hasFile("image")){
            $upload = new UploadFile();
            $image = $request->file('image');
            $ext = $request->file('image')->getClientOriginalExtension();
            $path = "images/users/".$user_id."/profile_picture.".$ext;
            $image_url = $upload->upload_image($image, $path);

            $chk_error = strpos($image_url, "error");
            # if not error
            if($chk_error == false ){
                # append image_url to array
                $user['image_url'] = $image_url;
            }
        }

        User::where('id', '=', $user_id)->update($user);

        return Message::response("Finished");
    }

    public function login(Request $request){

        $system_info    = $request->input('system_info');
        $device_token   = $request->input('device_token');
        $email          = $request->input('email');
        $password       = $request->input('password');

        if(!empty($email) && !empty($password) && !empty($system_info) ) {

            $user = User::where('email', '=', $email)
                ->where('password', '=', sha1($password))
                ->first();

            if(count($user) > 0 ){
                //TODO SNS
                // update user_authentication
                //$apns_key = PushNotification::getSNSEndPoint('customer', $device_token, $id_type.",".$id);

                $apns_key = "3333444";
                $user_data = array(
                    'user_id' => $user->id,
                    'status'  => 1,
                    'system_info' => $system_info,
                    'auth_key'=> sha1($system_info),
                    'apns_key'=> $apns_key
                );

                $arr_condition = array(
                    'user_id'=> $user->id,
                    'system_info'=> $system_info
                );

                //create or update user authentication
                UserAuthentication::updateOrCreate($arr_condition, $user_data);

                //get user profile
                $result = User::getProfile($user->id, $device_token);
                return Message::response($result);

            }else{
                return Message::signInFalseParam();
            }

        }else{
            return Message::signInFalseParam();
        }

    }

    public function facebookLogin(Request $request){

        #user data
        $facebook_id	= $request->input('facebook_id');
        $email 			= $request->input('email');
        $name 			= $request->input('name');
        $last_name 		= $request->input('last_name');
        $image_url 		= $request->input('image_url');

        #user_authentications data
        $system_info	= $request->input('device');
        $device_token 	= $request->input('device_token');

        //check empty password
        if(empty($email) || empty($system_info) || empty($device_token) || empty($facebook_id)){
            return Message::signInFalseParam();
        }

        //check duplicate user
        $user_exist = User::checkDuplicateByEmail($email);
        if($user_exist){
            //update existing user profile with facebook information
            $user_data = array(
                "facebook_id"   => $facebook_id,
                "name"          => $name,
                "last_name"     => $last_name,
                "image_url"     => $image_url
            );
            User::where('email', '=', $email)->update($user_data);
            $user = User::where('email', '=', $email)->select('id')->first();
            $user_id = $user->id;

        }else{
            //create user profile
            $user_data = array(
                "facebook_id"	=> $facebook_id,
                "email" 		=> $email,
                "name"		    => $name,
                "last_name"     => $last_name,
                "image_url"		=> $image_url
            );
            $user = User::create($user_data);
            $user_id = $user->id;
        }

        //TODO create or get apns_key
        //$apns_key = PushNotification::getSNSEndPoint('customer', $device_token, $id_type.",".$id);
        $apns_key = "9999999999";

        $user_authentication_data = array(
            'user_id' => $user_id,
            'status'  => 1,
            'system_info' => $system_info,
            'auth_key'=> sha1($system_info),
            'apns_key'=> $apns_key
        );
        $arr_condition = array(
            'user_id'=> $user_id,
            'system_info'=> $system_info
        );
        //create or update user authentications
        UserAuthentication::updateOrCreate($arr_condition, $user_authentication_data);
        return User::getProfile($user_id, $system_info);

    }


    /**
     * @return array
     */


    public function resetPassword(Request $request){

        $email = $request->input('email');
        $type = "user_reset_password";

        # check email validate
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $info = "Invalid email format";
            Message::invalidEmail();
        }

        if(!Mail::is_real_email($email)){
            Message::invalidEmail();
        }

        if (User::where('email', '=', $email)->exists()) {

            $info = "Please check your mail box";
            // generate & update token key
            $token = Utility::generateToken(80);

            // update SET token='token' user_forgot_password where email = $email
            $user = User::where('email', '=', $email)->first();
            $user_id = $user->id;
            UserForgotPassword::where(['user_id'=>$user_id])->delete();

            //create record
            $user_forgot = new UserForgotPassword();
            $user_forgot->user_id   = $user_id;
            $user_forgot->token     = $token;
            $user_forgot->save();

            // send email
            $resp = Mail::sendMail($email, $vendor_name="", $type, $token);
            if($resp){
                $result = Message::response("Please check your mail box");
            }else{
                $result = Message::response("Please contact administrator");
            }

        }else{

            $info = "Not have email in system";
            $result = Message::responseFalse($info);

        }

        return $result;
    }

    ### Dev from here
    public function renewPassword($token){

        $user    = UserForgotPassword::checkToken($token);

        if(count($user)){
            $data['data'] = $user['0'];
            return view('forgot_password.user_renew_password_form', $data);
        }else{
            echo "NO";
        }

    }

    
    public function updatePassword(Request $request){

        $email       = $request->input('email');
        $password    = $request->input('password');
        $re_password = $request->input('re_password');
        $token       = $request->input('token');


        if( (strlen($password) < 6) OR (strlen($re_password) < 6) ){
            $info = "Minimum password 6 characters";
            return Message::responseFalse($info);
        }

        if( $password != $re_password ){
            $info = "Passwords don't match";
            return Message::passwordNotMatch();
        }

        $row = UserForgotPassword::checkToken($token);

        if(count($row) > 0){
            $info = "Success";
            //update user password
            $chk = User::resetPassword($email, $password);

            //update delete user_forgot_password
            $chk2 = UserForgotPassword::where('token', $token)->delete();

            if( ($chk) && ($chk2)){
                $info = "Please check your mail box";
            }else{
                $info = "False";
            }

            return Message::response($info);
        }else{
            $info = "Your link expired";
            return Message::responseFalse($info);
        }

    }



}


