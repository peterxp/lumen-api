<?php
namespace App\Http\Controllers\API;

use App\Models\BankAccount;
use App\Models\Chat;
use App\Models\DeliveryRule;
use App\Models\Menu;
use App\Models\Order;
use App\Models\ShopLocation;
use App\Models\ShopOpenRule;
use App\Models\Vendor;
use App\Models\VendorAuthentication;
use DB;
use App\Models\User;
use App\Models\Authentication;
use App\Models\UserAuthentication;
use App\Models\PushNotification;
use App\Models\UploadFile;
use App\Models\Message;
use App\Helpers\Calculator;
use App\Helpers\Mail;
use App\Helpers\Utility;
use App\Models\Shop;
use Illuminate\Mail\Mailer;


use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class ShopController extends Controller
{

    /**
     * @param Request $request
     * @return array|static
     */
    public function getByUser(Request $request){

        $version    = $request->input('version');
        $lat	    = $request->input('lat');
        $lon 	    = $request->input('lon');

        $shop = Shop::getShopAllWithMaxDistance();
        return $shop;
    }


    public function getByVendor(Request $request){

        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');

        //check user authenticate
        $chk_auth = VendorAuthentication::authenticate($auth_key, $vendor_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $shop = Shop::getByVendor($vendor_id);
        return Message::response($shop);

    }


    //Vendor update their shop
    public function updateShop(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');
        $name           = $request->input('name');
        $description    = $request->input('description');
        $story          = $request->input('story');
        $category_id    = $request->input('category_id');
        $phone          = $request->input('phone');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticate($auth_key, $vendor_id);

        if(!$chk_auth){
            return Message::authenticateFail();
        }

        //Check record exist
        if(!Shop::where('id', '=', $shop_id)->where('vendor_id', '=', $vendor_id)->exists()){
            return Message::wrongParameter();
        }

        //Shop Data
        $shop_data = [
            'name' => $name,
            'story'=> $story,
            'description' => $description,
            'category_id'=> $category_id,
            'shop_request_status' => 1
        ];

        //Upload image
        if($request->hasFile("logo")){
            $upload     = new UploadFile();
            $image      = $request->file('logo');
            $ext        = $request->file('logo')->getClientOriginalExtension();
            $path       = "images/vendors/".$vendor_id."/shops/".$shop_id."/logo.".$ext;
            $logo_url   = $upload->upload_image($image, $path);
            $chk_error = strpos($logo_url, "error");
            if($chk_error === false ){
                $shop_data['logo'] = $logo_url;
            }
        }

        if($request->hasFile("cover")){
            $upload     = new UploadFile();
            $image      = $request->file('cover');
            $ext        = $request->file('cover')->getClientOriginalExtension();
            $path       = "images/vendors/".$vendor_id."/shops/".$shop_id."/cover.".$ext;
            $cover_url  = $upload->upload_image($image, $path);
            $chk_error = strpos($cover_url, "error");
            if($chk_error === false ){
                $shop_data['cover'] = $cover_url;
            }
        }

        //Update shop data
        $shop = Shop::where('id', '=', $shop_id)
                    ->where('vendor_id', '=', $vendor_id)
                    ->update($shop_data);

        //update vendor status
        //this maybe discuss again
        $chk = Vendor::where('id', '=', $vendor_id)->update(['phone'=> $phone,'status' => 1]);

        if($chk){
            $result =  Message::response($shop);
        } else {
            $result = Message::reponseFalse("Update Fail");
        }
        return $result;

    }

    public function createOpenRule(Request $request){

        /* example array */
        /*
        $json_string = array(
            array(
                "shop_id" => 547,
                "start_day" => 1,
                "start_time" => "08:00:00",
                "close_day" => 2,
                "close_time" => "12:00:00"
            ),
            array(
                "shop_id" => 547,
                "start_day" => 3,
                "start_time" => "12:00:00",
                "close_day" => 4,
                "close_time" => "24:00:00"
            )
        );
        */

        $auth_key       = $request->input('auth_key');
        $shop_id        = $request->input('shop_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $openRuleArr    = (array)json_decode($request->input('open_rules'), true);
        $result = ShopOpenRule::createBatch($openRuleArr, $shop_id);

        return Message::response($result);
    }


    /*
     *
     public function requestopen_post(){
        $auth_key  = $this->post('auth_key');
        $vendor_id = $this->post('vendor_id');
        $shop_id   = $this->post('shop_id');
        if ($auth_key && $vendor_id) {
            if ($this->Authentication->auth_check($auth_key,'vauth')) {
                $info = $this->Vendor_model->requestApproveVendor($vendor_id, $shop_id);

                #hookSlack
               // $this->Curl_helper->hookSlack("shop_request", $shop_id);

                $this->response($info,200);
            }else{
                $this->response($this->Authentication->authError(),200);
            }
        }else{
            $this->response($this->Resmsg->parameterFalse(),200);
        }
    }
     */

    public function RequestOpenShop(Request $request){
        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $msg = Shop::requestOpenShop($vendor_id, $shop_id);

        return Message::response($msg);
    }

    public function OpenOrClose(Request $request){

        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');
        $status     = $request->input('status'); // is a status field of shop table (1 open, 2 close)

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        //check shop is approved before open shop
        // 3 = approved
        $chk_shop_approve = Shop::where('id', '=', $shop_id)
            ->where('vendor_id', '=', $vendor_id)
            ->where('shop_request_status', '=', 3)
            ->exists();
        if(!$chk_shop_approve){
            return Message::responseFalse("You can not open shop this time.");
        }

        if ($auth_key && $vendor_id) {
            $data = Shop::OpenOrClose($vendor_id, $shop_id, $status);
            $result = Message::response($data);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }

    public function updateBankAccount(Request $request){

        //acc_name to account_number
        //acc_number to account_number


        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');
        $account_number = $request->input('account_number');
        $account_name   = $request->input('account_name');
        $bank_id        = $request->input('bank_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $arr = [
            'account' => $account_number,
            'name'    => $account_name,
            'bank_id' => $bank_id
        ];

        $chk = BankAccount::where('vendor_id', '=', $vendor_id)->update($arr);
        if($chk){
            return Message::response("bank_account_updated");
        }else{
            return Message::responseFalse("fail");
        }
    }


    public function createDeliveryRule(Request $request){

        /* example array */
//        $json_string = array(
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 1,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 0
//            ),
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 2,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 0
//            ),
//            array(
//                "shop_id"   => 547,
//                "dayspend"  => 3,
//                "timespend" => "1:00:00",
//                "price"     => 2,
//                "typeinout" => 2,
//                "typeid"    => 1,
//                "is_flat"   => 1
//            )
//        );
//
//        echo json_encode($json_string);
//        exit();



        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');
        $status     = $request->input('status'); // is a status field of shop table (1 open, 2 close)
        $delivery_rules = $request->input('delivery_rules');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $deliveryRulesArr = (array)json_decode($delivery_rules, true);

        $result = DeliveryRule::createBatch($deliveryRulesArr, $shop_id);

        return Message::response($result);

    }

    public function updateLocation(Request $request){
        $auth_key   = $request->input('auth_key');
        $vendor_id  = $request->input('vendor_id');
        $shop_id    = $request->input('shop_id');

        $latitude   = $request->input('latitude');
        $longitude  = $request->input('longitude');
        $place_id   = $request->input('place_id');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $arr = [
            'shop_id'   => $shop_id,
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'place_id'  => $place_id
        ];
        $chk = ShopLocation::where('shop_id', '=', $shop_id)->update($arr);

        if($chk){
            return Message::response("location_updated");
        }else{
            return Message::responseFalse("fail");
        }
    }

    public function getVendorBadge(Request $request){

        $shop_id    = $request->input('shop_id');
        $auth_key   = $request->input('auth_key');

        if (!empty($auth_key)) {
//            $count_order = $this->Order_model->getCountNewOrder($shop_id);
//            $count_message = $this->Chat_model->countMessage($shop_id, 0, 'shop');
//            $sum = (int)$count_order->new_order_count + (int)$count_message->vendor_not_see_count;
//            $returnData = array('badge_home' => (String)$sum,
//                'badge_order'   => $count_order->new_order_count,
//                'badge_message' => $count_message->vendor_not_see_count);
//            $this->response($returnData, 200);

            //===
            $count_order = Order::getCountNewOrder($shop_id);
            $count_message = Chat::countMessage($shop_id, 0, 'shop');
            $sum = (int)$count_order + (int)$count_message;
            $data = [
                'badge_home'    => $sum,
                'badge_order'   => $count_order,
                'badge_message' => $count_message
            ];
            return $data;

        }else{
            //$this->response($this->Authentication->authError(),200);
            return Message::authenticateFail();
        }
    }

}


