<?php
namespace App\Http\Controllers\API;


use App\Models\Chat;
use App\Models\Information;
use App\Models\Message;
use App\Models\Order;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class InformationController extends Controller
{
    public function getVendorInfo(Request $request){
        $result = Information::getVendorInfo();
        return Message::response($result);
    }

    public function getShopCategory(Request $request){
        $result = Information::getAllShopCategory();
        return Message::response($result);
    }


}


