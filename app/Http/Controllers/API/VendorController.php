<?php
namespace App\Http\Controllers\API;


use App\Models\BankAccount;
use DB;
use App\Models\Shop;
use App\Models\Vendor;
use App\Models\VendorForgotPassword;
use App\Models\VendorAuthentication;
use App\Models\ShopLocation;
use App\Models\PushNotification;
use App\Models\UploadFile;
use App\Models\Message;
use App\Helpers\Mail;
use App\Helpers\Utility;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Laravel\Lumen\Routing\Controller as BaseController;

class VendorController extends Controller
{

    /**
     * @param Request $request
     * @return array|static
     */
    public function signUp(Request $request){

        $system_info    = $request->input('system_info');
        $device_token   = sha1($request->input('system_info'));
        $email          = $request->input('email');
        $password       = $request->input('password');
        $re_password    = $request->input('re_password');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return Message::wrongEmailFormat();
        }

        if(!Mail::is_real_email($email)){
            return Message::signUpEmailWrong();
        }

        if($password != $re_password){
            return Message::passwordNotMatch();
        }

        //Check duplicate
        if(Vendor::where('email', '=', $email)->exists()){
            return Message::checkDuplicateEmail();
        }

        if ( !empty($email) && !empty($password) && !empty($re_password) ) {

            if($password == $re_password){

                #1 create vendor
                $vendor = new Vendor();
                $vendor->email = $email;
                $vendor->password = sha1($password);
                $vendor->save();
                $vendor_id = $vendor->id;

                #2 create vendor_authentications
                #TODO get sns_endpoint
                //create sns for customer or vendor
                $sns_endpoint = VendorAuthentication::createAuthentication($vendor_id, $system_info, $device_token);

                #3 create shop
                $shop = new Shop();
                $shop->status           = 2;
                $shop->category_id      = 1;
                $shop->location_shop_id = 0;
                $shop->vendor_id        = $vendor_id;
                $shop->save();

                #4 crate shop location
                $location = new ShopLocation();
                $location->latitude     = 0;
                $location->longitude    = 0;
                $location->address_id   = 0;
                $location->place_id     = 0;
                $location->shop_id      = $vendor_id; //make shop
                $location->save();

                #5 crate bank account
                $bank_account = new BankAccount();
                $bank_account->vendor_id = $vendor_id;
                $bank_account->account = '';
                $bank_account->name = '';
                $bank_account->bank_id = '';
                $bank_account->save();


                $data = Vendor::getProfile($vendor_id, $request->input('system_info'));
                $data['auth_key'] = sha1($request->input('system_info'));

                return Message::response($data);
            }

        }else{
            return Message::wrongParameter();
        }
    }

    public function update(Request $request){

        $vendor_id  = $request->input('vendor_id');
        $auth_key   = $request->input('auth_key');

        //check user authenticate
        $chk_auth = VendorAuthentication::authenticate($auth_key, $vendor_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        $vendor = array(
            'name'	    => $request->input('name'),
            'last_name'	=> $request->input('last_name'),
            'phone'	    => $request->input('phone')
        );

        if($request->hasFile("image")){
            $upload = new UploadFile();
            $image = $request->file('image');
            $ext = $request->file('image')->getClientOriginalExtension();
            $path 	= $path = "images/vendors/".$vendor_id."/profile_picture.".$ext;
            $image_url = $upload->upload_image($image, $path);

            $chk_error = strpos($image_url, "error");
            # if not error
            if($chk_error == false ){
                # append image_url to array
                $vendor['image_url'] = $image_url;
            }
        }

        User::where('id', '=', $vendor_id)->update($vendor);

        return Message::response("Finished");
    }

    public function login(Request $request){

        $system_info    = $request->input('system_info');
        $device_token   = $request->input('device_token');
        $email          = $request->input('email');
        $password       = $request->input('password');

        if(!empty($email) && !empty($password) && !empty($system_info) ) {

            $vendor = Vendor::where('email', '=', $email)
                ->where('password', '=', sha1($password))
                ->first();

            if(count($vendor) > 0 ){
                //TODO SNS
                // update user_authentication
               // $apns_key = PushNotification::getSNSEndPoint('customer', $device_token, $id_type.",".$id);
                $apns_key = "9999999999";

                //create vendor authentication
                $vendor_id = $vendor->id;
                $vendor_auth = VendorAuthentication::createAuthentication($vendor_id, $system_info, $device_token);

                //get user profile
                $result = Vendor::getProfile($vendor->id, $system_info);
                return Message::response($result);

            }else{
                return Message::signInFalseParam();
            }

        }else{
            return Message::signInFalseParam();
        }

    }

    public function facebookLogin(Request $request){

        #user data
        $facebook_id	= $request->input('facebook_id');
        $email 			= $request->input('email');
        $name 			= $request->input('name');
        $last_name 		= $request->input('last_name');
        $image_url 		= $request->input('image_url');

        #user_authentications data
        $system_info	= $request->input('system_info');
        $device_token 	= $request->input('device_token');

        //check empty password
        if(empty($email) || empty($system_info) || empty($device_token) || empty($facebook_id)){
            return Message::signInFalseParam();
        }

        //check duplicate user
        $vendor_exist = Vendor::checkDuplicateByEmail($email);

        if($vendor_exist){
            //update existing user profile with facebook information
            $vendor_data = array(
                "facebook_id"   => $facebook_id,
                "name"          => $name,
                "last_name"     => $last_name
            );
            Vendor::where('email', '=', $email)->update($vendor_data);
            $vendor = Vendor::where('email', '=', $email)->select('id')->first();

        }else{

            #1 create vendor profile
            $vendor = new Vendor();
            $vendor->facebook_id = $facebook_id;
            $vendor->email  = $email;
            $vendor->name   = $name;
            $vendor->last_name = $last_name;
            $vendor->save();
            $vendor_id = $vendor->id;

            #2 create vendor_authentications
            #TODO get sns_endpoint
            //create sns for customer or vendor
            $sns_endpoint = VendorAuthentication::createAuthentication($vendor_id, $system_info, $device_token);


            #3 create shop
            $shop = new Shop();
            $shop->status           = 2;
            $shop->category_id      = 1;
            $shop->location_shop_id = 0;
            $shop->vendor_id        = $vendor_id;
            $shop->save();

            #4 crate shop location
            $location = new ShopLocation();
            $location->latitude     = 0;
            $location->longitude    = 0;
            $location->address_id   = 0;
            $location->place_id     = 0;
            $location->shop_id      = $vendor_id; //make shop
            $location->save();

            #5 crate bank account
            $bank_account = new BankAccount();
            $bank_account->vendor_id = $vendor_id;
            $bank_account->account = '';
            $bank_account->name = '';
            $bank_account->bank_id = '';
            $bank_account->save();

        }

        $vendor_id = $vendor->id;

        //TODO create or get apns_key
        //$apns_key = PushNotification::getSNSEndPoint('customer', $device_token, $id_type.",".$id);
        $apns_key = "9999999999";

        $vendor_auth = new VendorAuthentication();
        $vendor_auth->vendor_id     = $vendor_id;
        $vendor_auth->status        = 1;
        $vendor_auth->system_info   = $system_info;
        $vendor_auth->auth_key      = sha1($system_info);
        $vendor_auth->apns_key      = $apns_key;
        $vendor_auth->save();

        $result = Vendor::getProfile($vendor_id, $system_info);

        return Message::response($result);

    }


    public function resetPassword(Request $request){

        $email = $request->input('email');
        $type = "vendor_reset_password";

        # check email validate
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $info = "Invalid email format";
            Message::invalidEmail();
        }

        if(!Mail::is_real_email($email)){
            Message::invalidEmail();
        }

        if (Vendor::where('email', '=', $email)->exists()) {

            $info = "Please check your mail box";
            // generate & update token key
            $token = Utility::generateToken(80);

            // update SET token='token' user_forgot_password where email = $email
            $vendor = Vendor::where('email', '=', $email)->first();

            $vendor_id = $vendor->id;
            VendorForgotPassword::where(['vendor_id'=>$vendor_id])->delete();

            $forgot = new VendorForgotPassword();
            $forgot->vendor_id = $vendor_id;
            $forgot->token = $token;
            $forgot->save();

            // send email
            $resp = Mail::sendMail($email, $vendor_name="", $type, $token);
            if($resp){
                $result = Message::response("Please check your mail box");
            }else{
                $result = Message::response("Please contact administrator");
            }

        }else{

            $info = "Not have email in system";
            $result = Message::responseFalse($info);

        }

        return $result;
    }

    ### Dev from here
    public function renewPassword($token){

        $vendor    = VendorForgotPassword::checkToken($token);

        if(count($vendor)){
            $data['data'] = $vendor['0'];
            return view('forgot_password.vendor_renew_password_form', $data);
        }else{
            echo "NO";
        }

    }

    
    public function update_password(Request $request){

        $email       = $request->input('email');
        $password    = $request->input('password');
        $re_password = $request->input('re_password');
        $token       = $request->input('token');


        if( (strlen($password) < 6) OR (strlen($re_password) < 6) ){
            $info = "Minimum password 6 characters";
            return Message::responseFalse($info);
        }

        if( $password != $re_password ){
            $info = "Passwords don't match";
            return Message::passwordNotMatch();
        }

        $row = VendorForgotPassword::checkToken($token);

        if(count($row) > 0){
            $info = "Success";
            //update user password
            $chk = Vendor::resetPassword($email, $password);

            //update delete user_forgot_password
            $chk2 = VendorForgotPassword::where('token', $token)->delete();

            if( ($chk) && ($chk2)){
                $info = "Please check your mail box";
            }else{
                $info = "False";
            }

            return Message::response($info);
        }else{
            $info = "Your link expired";
            return Message::responseFalse($info);
        }

    }



}


