<?php
namespace App\Http\Controllers\API;

use App\Models\UserAuthentication;
use DB;
use App\Models\Basket;
use App\Models\LocationBasket;
use App\Models\Message;
use App\Models\User;
use App\Models\Address;
use App\Helpers\Calculator as Culculator;
use App\Helpers\Authentication as Authentication;
use App\Helpers\UploadFile;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class BasketController extends Controller
{

    #add to basket it means save to database
    public function add(Request $request){

        #General Information
        //address data
        $soi        = $request->input('soi');
        $street     = $request->input('street');
        $subdistrict= $request->input('subdis');
        $district   = $request->input('dist');
        $province   = $request->input('province');
        $country    = $request->input('country');
        $postcode   = $request->input('postcode');

        //location_basket data
        $latitude    = $request->input('lat');
        $longitude   = $request->input('long');

        //basket data
        $auth_key           = $request->input('auth_key');
        $user_id            = $request->input('user_id');
        $shop_id            = $request->input('shop_id');
        $menu_id            = $request->input('menu_id');
        $tier_id            = $request->input('tier_id');
        $qty                = $request->input('qty');
        $delivery_charge    = $request->input('delivery_charge') ? $request->input('delivery_charge') : 0;
        $estimate_time      = $request->input('estimate_time');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }
        ### check user authenticate ###

        #Step 1 add address
        $address = new Address();
        $address->soi       = $soi;
        $address->street    = $street;
        $address->subdistrict = $subdistrict;
        $address->district  = $district;
        $address->province  = $province;
        $address->country   = $country;
        $address->postcode  = $postcode;
        $address->save();

        #Step 2 add location_basket (need step 1 address_id)
        $locationBasket = new LocationBasket();
        $locationBasket->latitude   = $latitude;
        $locationBasket->longitude  = $longitude;
        $locationBasket->address_id = $address->id;
        $locationBasket->status     = 1;
        $locationBasket->save();

        #Step 3 add basket (need step 2 location_basket_id)
        $basket = new Basket();
        $basket->menu_id    = $menu_id;
        $basket->qty        = $qty;
        $basket->tier_id    = $tier_id;
        $basket->delivery_charge = $delivery_charge;
        $basket->estimate_time   = $estimate_time;
        $basket->shop_id         = $shop_id;
        $basket->user_id         = $user_id;
        $basket->location_basket_id = $locationBasket->id;
        $basket->status         = 1;
        $basket->save();

        #Step 4 return count menu by user
        $basket_count = Basket::countMenuByUser($user_id);
        return Message::response($basket_count);
    }

    public function deleteByShop(Request $request){
        $auth_key   = $request->input('auth_key');
        $shop_id  = $request->input('shop_id');
        $user_id  = $request->input('user_id');
        return Basket::deleteByShop($shop_id, $user_id);
    }

    public function userDeleteBasket(Request $request){
        $auth_key   = $request->input('auth_key');
        $basket_id  = $request->input('basket_id');
        $user_id    = $request->input('user_id');

        $query_del = Basket::userDeleteBasket($basket_id, $user_id);
        if($query_del){
            //return basket by user id
            $basket = Basket::getBasket($user_id);
            $result = Message::response($basket);
        }else{
            $result = Message::responseFalse("false");
        }
        return $result;
    }

    public function leaveNote(Request $request){

        $auth_key   = $request->input('auth_key');
        $basket_id  = $request->input('basket_id');
        $note       = $request->input('note');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }
        ### check user authenticate ###

        $chk = Basket::leaveNote($basket_id, $note);
        if($chk){
            $result =  Message::response("Completed");
        }else{
            $result = Message::responseFalse("Incomplete update");
        }
        return $result;
    }

    public function getBasket(Request $request){
        $auth_key           = $request->input('auth_key');
        $user_id            = $request->input('user_id');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }
        ### check user authenticate ###

        $basket = Basket::getBasket($user_id);
        $result = Message::response($basket);
        return $result;
    }



    public function getBasketCount(){
        $auth_key           = $request->input('auth_key');
        $user_id            = $request->input('user_id');
        $result = Basket::getBasketCount($user_id);
        return Message::response($result);

    }

    public function countMenuByUser(Request $request){
        $auth_key   = $request->input('auth_key');
        $user_id    = $request->input('user_id');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);

        if(!$chk_auth){
            return Message::authenticateFail();
        }
        ### check user authenticate ###

        return Basket::countMenuByUser($user_id);
    }

}


