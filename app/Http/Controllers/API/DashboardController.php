<?php
namespace App\Http\Controllers\API;

use App\Models\BankAccount;
use App\Models\Dashboard;
use App\Models\DeliveryRule;
use App\Models\Order;
use App\Models\RequestTransfer;
use App\Models\ShopLocation;
use App\Models\ShopOpenRule;
use App\Models\Vendor;
use App\Models\VendorAuthentication;
use DB;
use App\Models\UploadFile;
use App\Models\Message;
use App\Models\Shop;

use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class DashboardController extends Controller
{

    public function main(Request $request){

        $auth_key    	= $request->input('auth_key');
        $vendor_id   	= $request->input('vendor_id');
        $shop_id   	 	= $request->input('shop_id');
        $type        	= $request->input('type');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }
        if ($auth_key && $vendor_id && $shop_id) {
            $info   = Dashboard::getAll($vendor_id, $shop_id, $type);
            $result = Message::response($info);
        }else{
            $result = Message::wrongParameter();
        }

        return $result;

    }


    public function requestTransfer(Request $request){

        $auth_key       = $request->input('auth_key');
        $vendor_id      = $request->input('vendor_id');
        $shop_id        = $request->input('shop_id');
        $tab_value      = $request->input('tab_value');
        $bat_value      = $request->input('bat_value');

        $start_date     = $request->input('start_date');
        $end_date       = $request->input('end_date');

        //check vendor authenticate
        $chk_auth = VendorAuthentication::authenticateOnlyAuthKey($auth_key);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        //check BAT more than 30
        $bat_value_from_database = Dashboard::queryBAT($shop_id);
        if($bat_value_from_database < 30){
            return Message::responseFalse("Money not enough to request");
        }

        //check existing record today
        $chk = RequestTransfer::checkExisting($vendor_id, $shop_id);
        if($chk > 0){
            return Message::responseFalse("Duplicate request");
        }

        if ($auth_key && $vendor_id && $shop_id) {
            $data = Dashboard::requestTransfer($vendor_id, $shop_id, $tab_value, $bat_value, $start_date, $end_date);
            $status = 2; // request status
            Order::updateOrderIsTransfer($shop_id, $status);
            $result = Message::response($data);

        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }


}


