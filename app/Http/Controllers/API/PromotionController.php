<?php
namespace App\Http\Controllers\API;

use App\Models\Promotion;
use App\Models\UserAuthentication;
use DB;
use App\Models\Message;

use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class PromotionController extends Controller
{
    public function validatePromotionCode(Request $request){

        $auth_key   = $request->input('auth_key');
        $shop_id    = $request->input('shop_id');
        $user_id    = $request->input('user_id');
        $code       = $request->input('code');
        $amount     = $request->input('amount');
        $total_amount = $request->input('total_amount');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }

        //check wrong parameter
        if ( empty($auth_key) || empty($user_id) || empty($code) ) {
            return Message::wrongParameter();
        }

        //check promotion available
        if(!Promotion::getPromotion($code)){
            return Message::checkPromotionFalse();
        }

        if ($shop_id) {
            $result = Promotion::validateCode($shop_id, $user_id, $code, $amount, $total_amount);
        }else{
            $result = Promotion::validateCode(0, $user_id, $code, $amount, 0);
        }
        return Message::response($result);
    }

    public function updateVIP(Request $request){

        $auth_key       = $request->input('auth_key');
        $user_id        = $request->input('user_id');
        $user_vip_id    = $request->input('user_vip_id');
        $balance        = $request->input('balance');

        ### check user authenticate ###
        $chk_auth = UserAuthentication::authenticate($auth_key, $user_id);
        if(!$chk_auth){
            return Message::authenticateFail();
        }
        if ( !empty($auth_key) || !empty($user_id) || !empty($user_vip_id) || !empty($balance) ) {
            $result = Promotion::updateVIPBalance($user_id, $user_vip_id, $balance);
        }else{
            $result = Message::wrongParameter();
        }
        return $result;
    }


}


