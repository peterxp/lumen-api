<?php
namespace App\Http\Controllers\Admin;

use App\Models\Omise;
use App\Models\OmisePayment;
use App\Models\PaymentOmise;
use DB;
use App\Helpers\Slack;
use App\Models\BanWord;
use App\Models\User;
use App\Models\ShopCategory;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

use App\Helpers\Authentication as Authentication;
use App\Helpers\UploadFile;
use App\Models\Message;


use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Monolog\Logger;

class ShopCategoryController extends Controller
{

    public function index(Request $request){
        $shopCategory = ShopCategory::select('id', 'name')->get();
        return Message::response($shopCategory);
    }

    public function userExist(Request $request)
    {
        if (User::where('id', '=', "99999999")->exists()) {
            $result = TRUE;
        }else{
            $result = FALSE;
        }
        return ['result'=>$result];
    }

    public function carbon(Request $request){

        $now = Carbon::now();
        $yesterday = Carbon::yesterday();

        $arr = [
            "yesterday" => $yesterday->toDateTimeString(),
            "today" => $now->toDateTimeString(),
            "tomorrow" =>  $now->addDay()->toDateTimeString(),
            "human_read" => Carbon::now()->diffForHumans()

        ];

        return $arr;

    }

    public function rollBar(Request $request){

//        $config = array(
//            // required
//            'access_token' => '4a2f65c609e6447d9a95993739b5601f',
//            // optional - environment name. any string will do.
//            'environment' => 'production',
//            // optional - path to directory your code is in. used for linking stack traces.
//            'root' => '/Users/brian/www/myapp'
//        );
//
//        \Rollbar::init($config);

        $id = 388;
        $x = Log::ERROR('Showing user profile for user: '.$id);
        if($x){
            return "Y";
        }else{
            return "N";
        }

//        User::all
    }


    public function hook(Request $request)
    {

        $target = $request->input("target");

        if($target == "new_order"){

            //new order
            $order_id = 339;
            $shop_name = "Cafe Irene";
            $customer_name = "Chai Pradabsri";
            $created_at = "2016-03-15 14:30:18";
            $grand_total = 1500;
            return Slack::hookNewOrder($order_id, $shop_name, $customer_name, $created_at, $grand_total);

        }else if($target == "request_transfer"){

            //transfer
            $transfer_id = 339;
            $shop_name = "Cafe Irene";
            $tab_value = 3228.32;
            $bat_value = 28000;
            $start  = "2016-03-11 00:00:00";
            $end    = "2016-03-11 00:00:00";
            $created_at = "2016-03-15 14:30:18";

            return Slack::hookRequestTransfer($transfer_id, $shop_name, $tab_value, $bat_value, $start, $end, $created_at);

        }
    }

    public function banWord(Request $request){
        //call magic function
        return BanWord::all();
    }

    public function pagination(Request $request){
        return DB::table('ban_word')->paginate(3);
    }

    public function getPromotion(Request $request){
        return DB::table('pcode')->paginate(10);
    }


    public function omise(Request $request){
        $omise = new PaymentOmise();
        return $omise->init();
    }

    public function transfer(Request $request){
        $omise = new PaymentOmise();
        $recipient_id = "recp_test_525faev5s3r4e0e5ppa";
        $amount = 12000;
        return $omise->transfer($recipient_id, $amount);
    }



}


