<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Message;
use App\Models\UserAuthentication;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * We will check only false value
     */
    public function handle($request, Closure $next)
    {

        $system_info    = $request->input('system_info');
        $auth_key       = sha1($system_info);

        if(!empty($system_info)) {

            $chk_auth = true;
            UserAuthentication::check_auth($auth_key);
            if (!$chk_auth) {
                return Message::authenticateFail();
            }

        }else {

            return Message::authenticateFail();

        }

        return $next($request);
    }


}
