<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Message;
use App\Models\UserAuthentication;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('system_infox') == '') {
            return  Message::authenticateFail();
        }

        return $next($request);
    }


}
