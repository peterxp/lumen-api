This directory is ignored by Git intentionally because keeping copies of the
source code of all of our library dependencies in our Git repo is messy and
encourages people to edit library code directly in our repo, which is a bad
practice.

The `vendor/laravel/homestead` directory is one exception, it is explicitly
tracked in Git because it allows running `vagrant up` when setting up a new
development machine, without needing to install PHP and Composer first. The
library is not loaded into memory when the app runs (it is set as `require-dev`
in `composer.json`).
