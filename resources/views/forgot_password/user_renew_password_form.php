<html>
  <head>
    <title>Forgot Password - Onner Thailand</title>
    <meta charset="utf-8">
    <link href="https://www.onner.com/theme/styles/icons/favicon.ico" rel="shortcut icon">
    <link href="https://www.onner.com/theme/styles/icons/touch.png" rel="apple-touch-icon-precomposed">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="robots" content="noindex,nofollow" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://file.myfontastic.com/JEDwiCeQWc895PiEd2rFq7/icons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
    <style type="text/css">
      body { font-family: 'Fira Sans', sans-serif; font-size: 100%; line-height: 1.5em; font-weight: 300; }
      .loginbox {float: left;width: 320px;padding: 10px;overflow: hidden;position: absolute;height: auto;left: 50%;margin-left: -160px;top: 50%; margin-top: -150px; text-align: center;}
      .loginbox span {float: left;width: 300px;font-size: 3em;margin: 0 0 30px 0;color: #00BEC8;}
      .loginbox input {float: left;width: 300px;padding:10px;border:1px solid #ccc;margin-bottom:10px;color: #333; font-size: 0.7em;}
      .loginbox input:focus {border:1px solid #00BEC8;color: #00BEC8;}
      .loginbox button {float: left;width: 300px;padding:10px; cursor: pointer; background-color:#fff; color: #00BEC8;border:1px solid #00BEC8;margin-bottom:30px;}
      .loginbox button:hover {background-color:#00BEC8; color: #fff;}
      .loginbox button.disable { color: #ccc !important;border-color: #ccc !important; background-color: #fff !important; }
      .loginbox small {float: left;width: 300px;color: #999;font-size: 0.6em;margin: 1em 0;}
      .loginbox .topic { font-size: 100%;}
      @media all and (min-width:768px) {
        body { zoom:1.2; }
      }
    </style>
    <script>

      $(document).ready(function(){
        $('#submit_password').click( function(event) {

          var token = $("#token").val();
          var email = $("#email").val();
          var password = $("#password").val();
          var re_password = $("#re_password").val();

          event.preventDefault();

          $.ajax( {
              type:'POST',
              url:'http://local-api-naja.onner.local/v2/users/update_password',
              data: {'email': email,'password': password, 're_password': re_password, 'token': token},
              success: function(result) {
                console.log(result);

                if(result.status == "true") {
                  swal({ title: result.data, text: 'Done! You can now sign in with your new password.', timer: 2000 });
                } else {
                  swal("Error", result.data, "error");
                }
              },
              error: function() {
                console.log("error");
              }
            });


        });
      });

    </script>
  </head>
  <body>
    <form class="loginbox">

      <span><i class="onner-logo"></i></span>
      <span class="topic">Renew Password</span>

      <input type="hidden" name="token" id="token" value="<?php echo $data->token;?>">
      <input type="hidden" name="email" id="email" value="<?php echo $data->email;?>">

      <input autocomplete="false" placeholder="Password" type="password" name="password" id="password" maxlength="75" required />
      <input autocomplete="false" placeholder="Re-Password" type="password" name="re_password" id="re_password" maxlength="16" required />
      <button type="button" id="submit_password">Submit</button>
      <small>Copyright&copy2015 Onner Thailand. All right reserved.</small>

    </form>
  </body>
</html>