Elastic Beanstalk Environment and Deployment Customization
----------------------------------------------------------

The Onner API is deployed on [AWS Elastic Beanstalk] to staging and production
environments. This directory is where custom options are kept for setting up
the environments and deployment steps. See the main project README for basic
day-to-day deployment usage steps using the `eb` command line tool.

In general, we prefer to set options here rather than the AWS web console, so
that it is easy to repeat environment setups in the future. However, a few
settings, such as environment variables that hold secrets like passwords and
API keys, are best set per-environment in the web interface rather than kept in
source control.

See [the full configuration reference][1] for details on available options.

### Using Elastic Beanstalk with RDS Databases ###

We do not let Elastic Beanstalk set up RDS MySQL databases for our environments
automatically, because it then terminates a database instance if the Beanstalk
environment is terminated. This is too risky for a relatively easy mistake. See
[this document][2] for details on how to use RDS databases set up independently
with Elastic Beanstalk environments (in short, we simply permit the Beanstalk
environment's security group to access the RDS database's group, and configure
the app to use the database host address).

### SSL ###

Our staging environment currently does not have SSL configured, because for a
single-instance Elastic Beanstalk environment AWS [currently requires pasting a
private SSL key in a config file][3] rather than using [IAM uploaded
certificates][4]. I'm not going to put our real private SSL key in a file that
needs to be tracked in source control, that's a lousy practice and Amazon needs
to update their service to have a better way of doing this.

As an alternative, we may soon generate a self-signed certificate for staging
use.

[AWS Elastic Beanstalk]: http://aws.amazon.com/elasticbeanstalk/
[1]: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/customize-containers.html
[2]: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_PHP.rds.html
[3]: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/SSLPHP.SingleInstance.html
[4]: http://docs.aws.amazon.com/cli/latest/reference/iam/upload-server-certificate.html

<!-- vim:set expandtab textwidth=79: -->
