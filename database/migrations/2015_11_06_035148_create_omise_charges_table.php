<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmiseChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('omise_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('omise_charge_id', 255);
            $table->string('omise_transaction_id', 255);
            $table->string('omise_reference_id', 255);
            $table->string('omise_customer_id', 255);
            $table->string('omise_presponse_id', 255);
            $table->text('omise_presponse');
            $table->integer('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('omise_charges');
    }
}
