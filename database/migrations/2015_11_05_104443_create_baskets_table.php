<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id');
            $table->integer('shop_id');
            $table->integer('user_id');
            $table->integer('qty');
            $table->integer('tier_id');
            $table->string('note', 255);
            $table->integer('estimate_time')->comment ="Select from last menu add";
            $table->integer('delivery_charge')->comment ="Select from last menu add charge";
            $table->integer('promotion_code_id')->comment = "Product name column";
            $table->integer('location_basket_id');
            $table->integer('status')->comment = "1 is a not payment, 2 is a paid and move to order table";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baskets');
    }
}
