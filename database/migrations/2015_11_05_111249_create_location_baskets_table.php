<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_baskets', function (Blueprint $table) {
            $table->increments('id');
            $table->float('latitude', 10, 6);
            $table->float('longitude', 10, 6);
            $table->integer('address_id');
            $table->text('delivery_address', 500);
            $table->integer('order_id');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('location_baskets');
    }
}
