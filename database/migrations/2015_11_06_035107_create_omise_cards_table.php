<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmiseCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('omise_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('omise_card_id', 200)->comment = "omise card id from omise API";
            $table->string('omise_cus_id', 200)->comment = "omise customer id from omise API";
            $table->string('omise_expiration_month', 200)->comment = "omise card last digits from omise API";
            $table->string('omise_expiration_year', 200)->comment = "omise card exp mount from omise API";
            $table->string('omise_customer_id', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('omise_cards');
    }
}
