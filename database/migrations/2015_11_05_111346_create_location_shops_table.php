<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_shops', function (Blueprint $table) {
            $table->increments('id');
            $table->float('latitude', 10, 6);
            $table->float('longitude', 10, 6);
            $table->integer('address_id');
            $table->integer('place_id');
            $table->integer('shop_id');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('location_shops');
    }
}
