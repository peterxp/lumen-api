<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->integer('day_spend');
            $table->time('time_spend');
            $table->integer('price');
            $table->integer('type_in_out')->comment = "Delivery inside vendor Location or Outside";
            $table->integer('type_id')->comment = "distancetype or placetype";
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_rules');
    }
}
