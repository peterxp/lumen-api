<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOmiseCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('omise_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('omise_cus_id', 255);
            $table->string('omise_cus_email', 255);
            $table->string('omise_default_card', 255);
            $table->text('omise_response');
            $table->string('onner_key', 255);
            $table->integer('user_id');
            $table->char('iskeepcard', 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('omise_customers');
    }
}
