<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('description', 300);
            $table->text('cover', 500);
            $table->text('ingredient', 120);
            $table->integer('menus_status_id');
            $table->integer('shop_id');
            $table->integer('menu_ref_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_drafts');
    }
}
