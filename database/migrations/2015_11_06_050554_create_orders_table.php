<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basket_id');
            $table->integer('status_for_usr');
            $table->integer('status_for_vendor');
            $table->integer('delivery_charges');
            $table->integer('delivery_address');
            $table->string('customer_phone', 15);
            $table->integer('price');
            $table->integer('food_price');
            $table->integer('total_price');
            $table->string('estimate_time', 100);
            $table->integer('is_use_promotion_code');
            $table->integer('promotion_code_id');
            $table->integer('user_id');
            $table->integer('vendor_id');
            $table->integer('shop_id');
            $table->text('basket_log');
            $table->text('omise_transaction_log');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
