<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOpenRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_open_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->integer('start_day');
            $table->integer('start_time');
            $table->integer('close_day');
            $table->integer('close_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_open_rules');
    }
}
