<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('house', 10);
            $table->string('floor', 5)->nullable();
            $table->string('place_name', 50)->nullable();
            $table->string('soi', 50)->nullable();
            $table->string('street', 50)->nullable();
            $table->string('subdistrict', 40)->nullable();
            $table->string('district', 30);
            $table->string('province', 50);
            $table->string('country', 50);
            $table->integer('postcode');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address');
    }
}
