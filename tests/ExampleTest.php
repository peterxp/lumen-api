<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        return "x";
    }

    public function visit(){
        $this->visit('/')
            ->see('Visit');
    }

    public function testEmpty()
    {
        $stack = array('1','2');
        $this->assertEmpty($stack);
        return $stack;
    }


}
