# API v.2 Developer Manual

This API use Laravel Lumen, Migrate from the best practice of Codeigniter API

## What was changed in API 2.0 (Happy Coding Version)
URL:

        https://abc.com 
        to 
        https://xyz.com
    
PHP Language Version: 

        PHP 5.6 => PHP7
 
Framework:

        Codeigniter 3.1.0 => Laravel Lumen 5.2
        
Webserver Software: 

        Apache2 => NGINX 1.8.x
    
SSL:

        GoDaddy(GRADE A)  => Let’s Encrypt (GRADE B, Free)

AWS Instance Type:
        Medium => t1.micro (must to extend to bigger package)


### Database
When we will integrate system MUST to change table name, field name and merge table with care.
https://github.com/xyz/blob/master/docs/what-is-change-v2.0


### How to setup and Install
First of all download api sourcecode

```sh
$ git clone https://github.com/xyz/xyz.com.git 
$ cd folder
```
If you don't have composer please download  Composer https://getcomposer.org/
```sh
$ composer install
```

composer.json is a config file of composer package manager, if developer want to add any third party source code please config it and run 
```sh
        $ composer install
        $ composer update
```
If you want to install third party library please go to https://packagist.org/ and add the package name and version to root_folder/composer.json then run below command
```sh
$ composer self-update
```
Note: on real server must to with power user command
```sh
$ sudo composer self-update
```


### Structure and Folder
- .env is config file (database config, cache, etc), You can get config value by
```php
        echo env('AWS_VERSION');
        //it will show value in AWS_VERSION
```



- app/Http/routes.php  is a route url of api service, how many main api service we can count at here, Importance!
- Routes separate in two parts

        -API for mobile (Source code store in controllers/API/)
        -Admin for administrator backend/Marketing backend (Source code store in Controllers/Admin)

- app/Http/Middleware/    all middleware that you can apply filter or validate user or other criteria

- docs/ is contain POSTMAN collection (url & variable of api service, example of using service)

- public/ is the main route of this system if you initial application please point to this folder

- storage/ developer should change mode of folder to 0777 to enable app to write error log & cache, Keep in your mind if you can not see error please see storage/logs/lumen.log

- tests is use for write your test case



### Coding Style
- API v.2 use camel case style
```sh
    # Naming variable style
    $totalPrice = 0;
    $vendorInformation = "Coffee Shop";
    $vendorBankAccountj = "1234567890";
    
    # Naming function style
    function getAddress($userId){}
    function calculateOmise($money){}
    
    # Naming table style
    # try to name table as a group of actor 
    # use the same word at the start of table name
    shop
    shop_category
    shop_location
    shop_open_rule
    
    user
    user_location
    user_skip
```

### Examples & Usages

- routes.php
```sh
$app->group(['prefix'=>'v2','namespace' => 'App\Http\Controllers'], function($app)
{
    #user
    $app->post('user/signUp', 'UserController@signUp');
    $app->post('user/skip', 'UserSkipController@skip');
    
});
```
- How to call above route
- https://url/v2/user/signUp 
    explain: call api via POST Method, The route will call "UserController.php" at the "function signUp"

- Please see app/Http/{Admin|API}/TestController.php




